myEnv <- new.env()
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz01.r", 
       local = myEnv)
p1 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz02.r", 
       local = myEnv)
p2 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz03.r", 
       local = myEnv)
p3 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz04.r", 
       local = myEnv)
p4 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz05.r", 
       local = myEnv)
p5 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz06.r", 
       local = myEnv)
p6 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz07.r", 
       local = myEnv)
p7 <- file.path(myEnv$outdir, "out")

p1 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA1_20240626-2248/out/"
p2 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA2_20240626-2248/out/"
p3 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA3_20240626-2248/out/"
p4 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA4_20240626-2248/out/"
p5 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA5_20240626-2248/out/"
p6 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA6_20240626-2248/out/"
p7 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-UA7_20240626-2248/out/"

path <- c(p1, p2, p3, p4, p5, p6, p7)
rslt <- rEFISCEN::loadEFISCEN(path, selection=NULL, run=NULL)
str(rslt, max.level = 1)
class(rslt)
p <- dirname(path[length(path)])
d <- paste0(basename(p), "-all")
dir.create(p <- file.path(dirname(p), d))

for(s in c("0_0_0_0", "1_0_0_0", "2_0_0_0", "3_0_0_0")){
  # s <- "0_0_0_0"
  f <- paste0("UA_all-Sz_Stratum=", s, ".pdf")
  pdf(file=file.path(p, f), paper="a4r", width=20, height=10)
  plot(rslt, vars = "volume-rel", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  ## add values from publication Lavnyy et al. in prep, Figure 4 (average growing stock)
  # points(x=c(2011, 2018), y=c(338, 364), pch=3, cex=2, col="red") # not the same area and, hence, growing stock wrong
  tmp <- subset(rslt$UA1$volume, Step==2012)
  gs <- weighted.mean(tmp$GrStockAv, tmp$Area)
  points(x=c(2012, 2018), y=c(gs, gs * 364/338), pch=3, cex=2, col="red") 
  
  plot(rslt, vars = "volume-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  ## add values from publication Lavnyy et al. in prep, Figure 3 (average growing stock)
  # points(x=c(2011, 2018), y=c(213, 194), pch=3, cex=2, col="red") # not the same area and, hence, growing stock wrong
  tmp <- subset(rslt$UA1$volume, Step==2012)
  gs <- sum(tmp$GrStock) / 1000
  points(x=c(2012, 2018), y=c(gs, gs * 194/213), pch=3, cex=2, col="red") 
  
  plot(rslt, vars = "increment-rel", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  points(x=2012+5, y=11.76*0.77, pch=3, cex=2, col="red")
  plot(rslt, vars = "increment-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "increment-vba", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  ## add values from publication Lavnyy et al. in prep, see text: does it refer to mature stands or all stands?
  rect(xleft = 2012, xright = 2018, ybottom = 5.7, ytop = 8.1, col=rgb(1, 0, 0, 0.2),
       border = NA) # not the same area and, hence, growing stock wrong
  
  plot(rslt, vars = "harvest-rel", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "harvest-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE)) 
  plot(rslt, vars = "harvest-ratio", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "age", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  ageEFI <- 70.3 # from invdata (see UA1-scenario file)
  age2011 <- weighted.mean(c(74, 66, 56, 58), w = c(155.569, 284.390, 87.958, 73.619)) # 2011
  age2018 <- weighted.mean(c(75, 72, 52, 60), w = c(142.971, 258.011, 69.329, 62.273)) # 2018
  points(x=c(2012, 2018), y=c(ageEFI, ageEFI * age2018/age2011), pch=3, cex=2, col="red")
  
  ## loss in area only in UA7
  tmp <- rEFISCEN::loadEFISCEN(p7)
  plot(tmp, var="area-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  tmp <- subset(tmp$UA7$volume, Step==2012)
  ar <- sum(tmp$Area)
  points(x=c(0.75, 2), y=c(ar, ar * 532.6/629.1), pch=3, col="blue")
  
  dev.off()
}

