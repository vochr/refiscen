#' Author: christian.vonderach@forst.bwl.de
#' run EFISCEN using Ukraine data

rm(list=ls())
gc()
require(rEFISCEN)
require(readxl)
require(dplyr)

## Ukraine Scenario 1 ####
SimName <- "UA1" 

# EFIdir <- "J:/FVA-Projekte/P01770_SURGE_Pro/Daten/Ergebnisse/EFISCEN/"
EFIdir <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/"
time <- format(Sys.time(), "%Y%m%d-%H%M")
dir.create(outdir <- paste0(EFIdir, "EFI-", SimName, "_", time))
# copy this r-script to the outdir
file.copy(from = "P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/UA_Sz01.r",
          to = file.path(outdir, "UA_Sz01.r"))
dir.create(file.path(outdir, "out"))
pdf(file.path(outdir, "out/input.pdf"), paper="a4r", width = 20, height = 10)

# latest data: from 28.02.2024
# https://bwsyncandshare.kit.edu/f/3488746144
# data reduced and stored locally / on P:/SurgePro
# forest management ('region'): 1 - protected forest; 2 - protective forest; 3 - production forest
# origin ('origin'): "1 - planted / secondary; 2 - natural / primary; 3 - current year clearcuts and dendrological park"
p <- "P:/SurgePro/Daten/Urdaten/wp5/ua/"
ua <- as.data.frame(readxl::read_xlsx(file.path(p, "DATA_UA_split.xlsx"), sheet = "finalUA_MG"))
ua$siteclass <- NULL # remove bonitaet information, use dGZ100 as siteclass
ua$endage <- ua$endage - 1
colnames(ua) <- c("region", "owner", "siteclass", "species", "startage", 
                  "endage", "area", "volume", "increment")
weighted.mean(ua$increment, ua$area) # Haubarkeitsdurchschnittszuwachs (vol / age)
weighted.mean(ua$volume, ua$area) 

## aggregate ageclasses > 180 into class 160-180
ua <- ua %>% 
  # filter(siteclass==13) %>% 
  group_by(siteclass, species, owner, region) %>% 
  mutate(agg=1:n(),
         agg=ifelse(endage>=160, max(agg), agg)) %>% 
  group_by(siteclass, species, owner, region, agg) %>% 
  mutate(volume = sum(area * volume) / sum(area),
         volume = ifelse(is.na(volume), 0, volume),
         area = sum(area),
         increment = mean(increment[increment > 0])) %>% 
  ungroup() %>% 
  mutate(agg=NULL) %>% 
  filter(startage <= 160) %>% 
  as.data.frame(ua)

weighted.mean(ua$increment, ua$area) # still Haubarkeitszuwachs (vol / age)

## add extended Yield table increment ####
# make sure all classes are available
tmpltROSS <- unique(ua[, c("region", "owner", "siteclass", "species")])
tmpltAGE <- unique(ua[, c("species", "startage", "endage")])
tmplt <- merge(tmpltROSS, tmpltAGE)
ua <- merge(tmplt, ua, all.x = TRUE)
head(ua)

YTinc <- read.csv2("P:/SURGEPro/Daten/aufbereiteteDaten/wp5/ua/cr-total-vol-growth-model.csv")

tmpltROS <- unique(ua[, c("region", "owner", "species")])
tmpltROSS <- merge(tmpltROS, data.frame(species=1, siteclass=sort(unique(YTinc$dGZ100))))
tmpltAGE <- unique(ua[, c("species", "startage", "endage")])
tmplt <- merge(tmpltROSS, tmpltAGE)
incrGPE <- merge(tmplt, YTinc, all.x = TRUE,
                 by.x = c("siteclass", "endage"), by.y = c("dGZ100", "age"))
incrGPE <- incrGPE[order(incrGPE$region, incrGPE$owner, incrGPE$siteclass, 
                         incrGPE$species, incrGPE$startage), 
                   c("region", "owner", "siteclass", "species", "startage",
                     "endage", "totalVol", "stock_wb", "pai")]
colnames(incrGPE)[7:9] <- c("volumeGWL", "volume", "increment") ## using stock_wb as volume
head(incrGPE)

usecols <- colnames(ua)
ua <- merge(ua, YTinc, by.x = c("siteclass", "endage"), by.y = c("dGZ100", "age"))
ua$volumeYT <- ua$stock_wb # volume stock from YT at certain age
ua$incrementYT <- ua$pai # periodic (20yr slice) annual increment
ua <- ua[, c(usecols, "incrementYT", "volumeYT")]
ua <- ua[order(ua$region, ua$owner, ua$siteclass),]
ua <- ua[order(ua$region, ua$owner, ua$siteclass, ua$species, ua$startage),]
head(ua)
weighted.mean(ua$increment, ua$area, na.rm = TRUE) # Haubarkeitszuwachs (vol / age)
weighted.mean(ua[!is.na(ua$area),]$incrementYT, ua[!is.na(ua$area),]$area) # PAVI

# calculate stocking degree to check expected Period mean annual volume increment
# i.e. increment from yield table corrected for area-weighted stocking degree
agg <- ua[complete.cases(ua),] %>% 
  mutate(volRatio = volume / volumeYT) %>% 
  summarise(sdegree = weighted.mean(volRatio, area),
            incr = weighted.mean(increment, area),
            incrYT = weighted.mean(incrementYT, area),
            area = sum(area)) %>% 
  as.data.frame()
agg
agg$incrYT * agg$sdegree # 8 cbm per (ha*a) for all data of UA
agg$incr # Haubarkeitsdurchschnittszuwachs (MAI_UA)

## aggregate UA data to dGz100-classes #### 
# 1-2-3, 4-5-6, 7-8-9, 10-11-12, 13-14-15, 16-17-18
unique(ua$siteclass) # hence: combine 13+15, 4+6
si <- data.frame(old = 1:18, new=rep(c(2, 5, 8, 11, 14, 17), each=3))

ua <- ua %>% 
  mutate(area = ifelse(is.na(area), 0, area),
         areaYT = 1) %>% 
  left_join(si, by = c("siteclass" = "old")) %>% 
  # group_by(region, owner, new, species, startage, endage) %>% 
  group_by(new, startage, endage, species, owner, region) %>% 
  summarise(m3ha = sum(area * volume) / sum(area), # area weighted vol m3/ha
            m3haYT = sum(areaYT * volumeYT) / sum(areaYT), # yield table just averaged
            m3ha = ifelse(is.na(m3ha), 0, m3ha),
            m3haYT = ifelse(is.na(m3haYT), 0, m3haYT),
            increment = weighted.mean(increment, area),
            incrementYT = weighted.mean(incrementYT, areaYT),
            # increment = weighted.mean(increment, ifelse(volume==0, 1, volume)),
            # incrementYT = weighted.mean(incrementYT, ifelse(volumeYT==0, 1, volumeYT)),
            area = sum(area),
            volume = m3ha,
            volumeYT = m3haYT,
            areaYT = NULL,
            m3ha = NULL,
            m3haYT = NULL) %>% 
  ungroup() %>% 
  mutate(siteclass = new,
         new = NULL,
         incr5yrs = 5 * increment / volume * 100,
         incr5yrsYT = 5 * incrementYT / volumeYT * 100,
         volume = ifelse(volume <= 0 | is.na(volume), 0, volume),
         area = ifelse(area <= 0 | is.na(area), 0, area),
         increment = ifelse(increment <= 0 | is.na(increment), 0, increment)) %>% 
  # filter(area > 0) %>% 
  select(siteclass, startage, endage, area, volume, increment, 
         volumeYT, incrementYT, incr5yrs, incr5yrsYT,
         species, owner, region) %>% 
  arrange(region, owner, siteclass, species) %>% 
  as.data.frame()
head(ua)
weighted.mean(ua$incrementYT, ua$area)
weighted.mean(ua$increment, ua$area)

agg <- ua[complete.cases(ua),] %>% 
  mutate(volRatio = volume / volumeYT) %>% 
  summarise(sdegree = weighted.mean(volRatio, area),
            incr = weighted.mean(increment, area),
            incrYT = weighted.mean(incrementYT, area),
            area = sum(area)) %>% 
  as.data.frame()
agg
agg$incrYT * agg$sdegree # 8 cbm per (ha*a) for all data of UA
agg$incr

invdata <- ua
# View(ua)
# any(invdata[, c("area", "volume")]==0)
head(invdata)
ua$vol <- ua$volume*ua$area
ua$class <- paste(ua$region, ua$owner, ua$siteclass, ua$species, sep = "_")
barplot(volume ~ class + endage, data=ua, beside=TRUE,
        ylab="volume [cbm/ha]", las=1,
        legend.text=c(unique(ua$siteclass)), 
        args.legend=list(x="topleft", title="siteclass"))
vol <- ifelse(is.na(ua$volume), 0, ua$volume)
area <- ifelse(is.na(ua$area), 0, ua$area)
inc <- ifelse(is.na(ua$increment), 0, ua$increment)
(wm <- weighted.mean(vol, area))
abline(h=wm)
(MAItotal <- weighted.mean(inc, area))
regs <- c("Protected Forest", "Protective Forest", "Production Forest")
owns <- c("planted / secondary", "natural / primary")
## area distribution
for(cl in sort(unique(substr(ua$class, 0, 3)))){
  # cl <- sort(unique(substr(ua$class, 0, 3)))[2]
  print(cl)
  tmp <- ua[substr(ua$class, 0, 3) == cl,]
  reg <- strsplit(cl, split = "_")[[1]] [1]
  own <- strsplit(cl, split = "_")[[1]] [2]
  area <- round(sum(tmp$area, na.rm = TRUE), 0)
  print(area <- format(area, nsmall=1, big.mark=","))
  barplot(I(area/1000) ~ endage + siteclass, data=tmp, beside=TRUE,
          ylab="area [1000 ha]", las=1, ylim=c(0, max(ua$area)/1000),
          legend.text=c(unique(ua$endage)), 
          args.legend=list(x="topleft", title="age-class"))
  txt <- paste0("Forest Type: ", regs[as.integer(reg)], "\nStand origin: ", 
                owns[as.integer(own)], "\narea: ", area, "ha (", cl, "_0_0)")
  mtext(side=3, line=1, adj=0, cex=1, text = txt)
  area <- ifelse(is.na(tmp$area), 0, tmp$area)
}
## volume distribution
for(cl in sort(unique(substr(ua$class, 0, 3)))){
  # cl <- sort(unique(substr(ua$class, 0, 3)))[2]
  print(cl)
  tmp <- ua[substr(ua$class, 0, 3) == cl,]
  reg <- strsplit(cl, split = "_")[[1]] [1]
  own <- strsplit(cl, split = "_")[[1]] [2]
  area <- round(sum(tmp$area, na.rm = TRUE), 0)
  print(area <- format(area, nsmall=1, big.mark=","))
  barplot(volume ~ endage + siteclass, data=tmp, beside=TRUE,
          ylab="volume [cbm/ha]", las=1, ylim=c(0, max(ua$volume)),
          legend.text=c(unique(ua$endage)), 
          args.legend=list(x="topleft", title="age-class"))
  txt <- paste0("Forest Type: ", regs[as.integer(reg)], "\nStand origin: ", 
                owns[as.integer(own)], "\narea: ", area, "ha (", cl, "_0_0)")
  mtext(side=3, line=1, adj=0, cex=1, text = txt)
  area <- ifelse(is.na(tmp$area), 0, tmp$area)
  wm <- weighted.mean(tmp$volume, area, na.rm = TRUE)
  abline(h=wm)
}

## check given increment ####
for(cl in sort(unique(substr(ua$class, 0, 3)))){
  # cl <- sort(unique(substr(ua$class, 0, 3)))[2]
  print(cl)
  tmp <- ua[substr(ua$class, 0, 3) == cl,]
  tmp$age <- rowMeans(tmp[, c("startage", "endage")])
  reg <- strsplit(cl, split = "_")[[1]] [1]
  own <- strsplit(cl, split = "_")[[1]] [2]
  area <- round(sum(tmp$area, na.rm = TRUE), 0)
  print(area <- format(area, nsmall=1, big.mark=","))
  plot(increment ~ age, data=tmp, type="n", las=1, ylim=range(ua$increment, na.rm = TRUE),
       ylab="mean annual increment")
  print(area <- format(round(sum(tmp$area, na.rm = TRUE), 0), nsmall=1, big.mark=","))
  txt <- paste0("Forest Type: ", regs[as.integer(reg)], "\nStand origin: ", 
                owns[as.integer(own)], "\narea: ", area, "ha")
  mtext(side=3, line=1, adj=0, cex=1, text = txt)
  cols <- rainbow(n=max(unique(tmp$siteclass)))
  for(si in sort(unique(tmp$siteclass))){
    # si <- 17
    tmpp <- subset(tmp, siteclass==si)
    points(x=tmpp$age, y=tmpp$increment, type="b", col=cols[si])
  }
  legend("topright", legend=sort(unique(tmp$siteclass)), title = "siteclass",
         pch=1, lty=1, col=cols[sort(unique(tmp$siteclass))])
}

## check yield table increment ####
for(cl in sort(unique(substr(ua$class, 0, 3)))){
  # cl <- sort(unique(substr(ua$class, 0, 3)))[2]
  print(cl)
  tmp <- ua[substr(ua$class, 0, 3) == cl,]
  tmp$age <- rowMeans(tmp[, c("startage", "endage")])
  reg <- strsplit(cl, split = "_")[[1]] [1]
  own <- strsplit(cl, split = "_")[[1]] [2]
  area <- round(sum(tmp$area, na.rm = TRUE), 0)
  print(area <- format(area, nsmall=1, big.mark=","))
  plot(increment ~ age, data=tmp, type="n", las=1, ylim=range(ua$incrementYT, na.rm = TRUE),
       ylab="periodic annual increment  (20yrs)")
  print(area <- format(round(sum(tmp$area, na.rm = TRUE), 0), nsmall=1, big.mark=","))
  txt <- paste0("Forest Type: ", regs[as.integer(reg)], "\nStand origin: ", 
                owns[as.integer(own)], "\narea: ", area, "ha")
  mtext(side=3, line=1, adj=0, cex=1, text = txt)
  cols <- rainbow(n=max(unique(tmp$siteclass)))
  for(si in sort(unique(tmp$siteclass))){
    # si <- 17
    tmpp <- subset(tmp, siteclass==si)
    points(x=tmpp$age, y=tmpp$incrementYT, type="b", col=cols[si])
  }
  legend("topright", legend=sort(unique(tmp$siteclass)), title = "siteclass",
         pch=1, lty=1, col=cols[sort(unique(tmp$siteclass))])
}

dev.off()

## remove sparsely filled matrices!
agg <- aggregate(area ~ siteclass, data=invdata, FUN=sum)
agg$pct <- agg$area / sum(agg$area)
agg
#> siteclass 2 and 17: each hold < 1% of total considered area
invdata <- subset(invdata, siteclass %in% c(5, 8, 11, 14))
weighted.mean(invdata[!is.na(invdata$area),]$incrementYT, invdata[!is.na(invdata$area),]$area)
weighted.mean(invdata[!is.na(invdata$area),]$increment, invdata[!is.na(invdata$area),]$area)

agg <- invdata %>% 
  mutate(volRatio = volume / volumeYT) %>% 
  summarise(sdegree = weighted.mean(volRatio, area),
            incr = weighted.mean(increment, area),
            incrYT = weighted.mean(incrementYT, area),
            area = sum(area)) %>% 
  as.data.frame()
agg
agg$incrYT * agg$sdegree # 8 cbm per (ha*a) for all data of UA
agg$incr

pdf(file.path(outdir, "out/base.pdf"), paper="a4r", width = 20, height = 10)
# invdata <- subset(invdata, siteclass > 2 & siteclass < 17)

## initialise volume-age matrices ####
# plist <- supplyP2009(invdata = invdata, ccode = SimName, outdir = outdir)
plist <- supplyP2009(invdata = invdata, ccode = SimName, outdir = outdir, 
                     incr=incrGPE, plotTF = TRUE, 
                     control = list(corr=0.6, cv=0.65, R=1, VCW1=NULL))
aer <- rEFISCEN::runP2009(inv = plist, out = outdir)
# shell.exec(outdir) # show folder

## for running EFISCEN, several files are necessary
## 1 .efs, referencing to (supplyEFS())
## 2 .aer (and .vcl)
## 3 .prs (supplyPRS())
## 4 biomcomp-xxx.txt (supplyBioPar)
## 5 soil-xxx.par (supplySoilPar())


## create soil parameters file ####
# default values, used in EFISCEN for several countries, cf. Workshop Driebergen
# the *_defsoil.csv file holds degree days and drought index, specific for matrices
soilpars <- list('0_0_0_0'=
               list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
                    Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
                             klig=0.22, khum1=0.012, khum2=0.0012),
                    transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
                    litterComp=c(cw2cel=0.69, cw2sol=0.03, 
                                 fw2cel=0.65, fw2sol=0.03,
                                 nw2cel=0.51, nw2sol=0.27),
                    cddp=c(chum1=0.6, chum2=0.36)))
sf <- supplySoilPar(basename = SimName, dir = outdir, pars = soilpars, 
                    overwrite = TRUE)


## create parameters file ####
# 0_0_0_0 = Region_Owner_SiteClass_species = ForestType_ManagementType_SiteClass_Species
# origin ('origin'): "1 - planted / secondary; 2 - natural / primary; 3 - current year clearcuts and dendrological park"
# forest management ('region'): 1 - protected forest; 2 - protective forest; 3 - production forest
prspars <- list(YForest = list('0_0_0_0'=0.6), 
             Gamma = list('0_0_0_0'=0.4), 
             # Harvest = list('1_1_0_0'=c(300), # no harvest at all
             #                #min_age max_age min_prob_tresh max_prob_tresh level_below, percentage decrease of min age
             #                '2_1_0_0'=c(61, 71, 0.4, 0.91, 0, 0.2), #protective, secondary
             #                '3_1_0_0'=c(51, 61, 0.4, 0.93, 0, 0.2), #production, secondary # todo 0.93
             #                '1_2_0_0'=c(300), # no harvest at all
             #                '2_2_0_0'=c(120, 141, 0.4, 0.89, 0, 0.2), #protective, primary, todo: lower this value
             #                '3_2_0_0'=c(81, 91, 0.4, 0.90, 0, 0.2)), #production, primary
             Harvest = list('1_1_0_0'=c(300), # no harvest at all
                            #min_age max_age min_prob_tresh max_prob_tresh level_below, percentage decrease of min age
                            '2_1_0_0'=c(61, 100, 0.2, 0.9, 0, 0.2), #protective, secondary
                            '3_1_0_0'=c(51, 80, 0.2, 0.9, 0, 0.2), #production, secondary # todo 0.93
                            '1_2_0_0'=c(300), # no harvest at all
                            '2_2_0_0'=c(120, 160, 0.2, 0.9, 0, 0.2), #protective, primary, todo: lower this value
                            '3_2_0_0'=c(81, 120, 0.2, 0.9, 0, 0.2)), #production, primary
             Thinrange = list('1_1_0_0'=c(20, 20), # no thinning
                              '2_1_0_0'=c(10, 40), # protective / secondary
                              '3_1_0_0'=c(10, 50), # production / secondary
                              '1_2_0_0'=c(20, 20), # no thinning
                              '2_2_0_0'=c(10, 40), # protective / primary
                              '3_2_0_0'=c(10, 50)), # production / primary
             MortAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 200)),
             # P:\SurgePro\Dokumentation\Literatur\Yield-Tables-UA\UA_growth_tables.xlsx
             # mortality rates for protected forests taken from yield tables from UA_growth_tables, sheet1, column s+t, row 82ff
             # mortality rates for used forests taken from BW inventory
             MortRates = list('1_0_0_0'=c(0.08, 0.0602, 0.0347, 0.0266, 0.0226, 0.0194),
                              '2_0_0_0'=c(0.0189, 0.0189, 0.0123, 0.0067, 0.0057, 0.0132),
                              '3_0_0_0'=c(0.0189, 0.0189, 0.0123, 0.0067, 0.0057, 0.0132)),
             Decay = list('0_0_0_0'=0.4), # fall rate of standing deadwood
             Thhistory = list('0_0_0_0'=0.2))
prs <- supplyPRS(SimName = SimName, dir = outdir, steps = 5, pars = prspars, 
                 overwrite = TRUE)


## create biocomp.txt file ####
biopars <- list(Carbon = list('0_0_0_0'=0.5),
                WoodDens = list('0_0_0_0'=0.37),
                ## bioComps taken from Driebergen workshop (same in GER & HUN)
                BioAgeLims = list('0_0_0_0'=c(20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 1000)),
                BioAllocations = list('0_0_0_0'=list(
                  "stem"=c(0.3495, 0.4244, 0.5419, 0.6060, 0.6373, 0.6488, 0.6489, 0.6478, 0.6460, 0.6416, 0.6306),
                  "branches"=c(0.3689, 0.2936, 0.1840, 0.1346, 0.1129, 0.1052, 0.1045, 0.1055, 0.1075, 0.1111, 0.1189),
                  "coarse roots"=c(0.0525, 0.0882, 0.1376, 0.1563, 0.1641, 0.1699, 0.1761, 0.1805, 0.1837, 0.1871, 0.1932),
                  "fine roots"=c(0.0529, 0.0447, 0.0315, 0.0238, 0.0198, 0.0176, 0.0163, 0.0153, 0.0145, 0.0139, 0.0132),
                  "foliage"=c(0.1762, 0.1491, 0.1050, 0.0793, 0.0659, 0.0585, 0.0542, 0.0509, 0.0483, 0.0463, 0.0441))),
                # origin of litter parameters unknown
                LitterAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                LitterProduction = list('0_0_0_0'= list(
                  "Stem"=c(0.0043, 0.0043, 0.0043, 0.0043, 0.0043, 0.0043, 0.0043),
                  "branches"=c(0.0614, 0.0463, 0.0266, 0.0142, 0.0091, 0.0076, 0.0071),
                  "coarse roots"=c(0.0614, 0.0463, 0.0266, 0.0142, 0.0091, 0.0076, 0.0071),
                  "fine roots"=c(0.4906, 0.4906, 0.4906, 0.4906, 0.4906, 0.4906, 0.4906),
                  "foliage"=c(0.21, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21))),
                WLCoarseRootsShare = list('0_0_0_0'=0.0))

biocomp <- supplyBioPar(SimName = SimName, biopars = biopars, dir = outdir, 
                        overwrite = TRUE)


## create efs file ####
usc <- unique(invdata$siteclass) # unique site class
sc <- paste0("dgz", usc, "=", usc, collapse=", ")
sc <- eval(parse(text=paste0("list(", sc, ")")))

supplyEFS(title = "Base scenario Ukraine", abbr = SimName, start = 2012,
          naming = list(region=c(natural=1, planted=2), 
                        owner=c(protected=1, protective=2, production=3),
                        siteclass=sc,
                        species=c(spruce=1)), 
          files = list(prs=basename(prs), biomass=basename(biocomp),
                       aer=aer$outfiles[1], soil=basename(sf)),
          dir = outdir, overwrite = TRUE)

## now EFISCEN can be run (without detailed scenarios)
opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=16, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))

# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))


#plots
plot(rslt)
dev.off()
utils::browseURL(outdir)

################################################################################
## defining scenario files
################################################################################

## create defgrow file ####
# general increase of given increment due to low stocking (B°~0.7) by 10%
# growth change file
# according to intermediate results from LPJ-guess on NPP on 0.5grid (Isimip2b data)?
grthScnName <- "no Climate Change"
growPars <- list(agelimits = list('0_0_0_0' = c(20, 40, 60, 80, 100, 160, 300)),
                 growth = list("20" = list('0_0_0_0' = rep(1.1, 7)))) #
                               
dgf <- supplyDefGrow(SimName, growPars = growPars, grthScnName = grthScnName, 
                     dir = outdir, overwrite = TRUE)


## create defrems file ####
# Removals scenario file
DefRemsPars <- list('0_0_0_0' = list(steps = c(100),
                                     Fel_stem = c(0.9),
                                     Fel_tops = c(0.0),
                                     Fel_branches = c(0),
                                     Fel_leaves = c(0),
                                     Fel_croots = c(0),
                                     Fel_dwood = c(0),
                                     Thin_stem = c(0.9),
                                     Thin_tops = c(0),
                                     Thin_branches = c(0),
                                     Thin_leaves = c(0),
                                     Thin_croots = c(0),
                                     Thin_dwood = c(0)))
DefRemsScen <- "based on guess"

dfr <- supplyDefRems(SimName = SimName, ScenName = DefRemsScen, dir = outdir, 
                     DefRemsPars = DefRemsPars, overwrite = TRUE)


## create sp_change file ####
# species changes file
SpChParams <- list('100' = list(matrices = c('0_0_0_0'),
                                dest_species = c(8),
                                fract_change = c(0.0)))
SpChScen <- "base"

spch <- supplySpChange(SimName = SimName, ScenName = SpChScen, 
                       SpChParams = SpChParams, dir = outdir, overwrite = TRUE)


## create affor file ####
# afforestation scenario file
AffParams <- list('0_0_0_0' = list(steps = c(100),
                                   affor_area = c(0),
                                   affor_carbon = c(0)))
AffScen <- "base"

aff <- supplyAffor(SimName = SimName, ScenName = AffScen, AffParams = AffParams, 
                   dir = outdir, overwrite = TRUE)


## create defor file ####
# afforestation scenario file
# no deforestation of pure forests in UA?
DeforParams <- list('0_0_0_0' = list(steps = c(100),
                                     defor_area = c(0)))
DeforScen <- "base"

def <- supplyDefor(SimName = SimName, ScenName = DeforScen, 
                   DeforParams = DeforParams, dir = outdir, overwrite = TRUE)


## create defcut file ####
# Forest harvest scenario file; the given values refer to 1 simulation step
tmp <- subset(rslt$UA1$volume, Step==2012, "Area")
harv2013scaled <- 798500 / 601537 * sum(tmp$Area*1000)
harv <- rslt$UA1$volume %>% 
  filter(Step == 2012) %>% 
  group_by(REG) %>% 
  summarise(area = sum(Area)*1000) %>% #in [ha]
  as.data.frame() %>% 
  filter(REG > 1) %>% # no harvest in protected areas
  arrange(REG) %>% 
  mutate(area = c(0.5, 1.0) * area, # 50% harvest from protective forest, 100% from productions forests
         share = area / sum (area)) %>% 
  # unit of 798500 (harvest 2013): thousand m3 roundwood TODO: check with/out bark?
  # 0.9 = proportion of roundwood removed from the forest from the amount that is felled
  mutate(harvest2013 = share * harv2013scaled * 5 / 1000 / 0.9) %>% 
  print()

defCutPars <- list('1_0_0_0' = list(step = c(16),
                                   felling = c(0), 
                                   thinning = c(0)),
                   '2_0_0_0' = list(step = c(16),
                                    # split of 80:20 fell:thin by previous runs
                                    # (see git from 24.06.2024, pre UTC 20:15)
                                    felling = harv[harv$REG==2,]$harvest2013 * 0.8, 
                                    thinning = harv[harv$REG==2,]$harvest2013 * 0.2),
                   '3_0_0_0' = list(step = c(16),
                                    felling = harv[harv$REG==3,]$harvest2013 * 0.67, 
                                    thinning = harv[harv$REG==3,]$harvest2013 * 0.33))

DefCutScen <- "base"

dfc <- supplyDefCut(SimName = SimName, ScenName = DefCutScen, 
                    DefCutPars = defCutPars, dir = outdir, overwrite = TRUE)


## create defsoil file ####
### adjust code, because only one of two parameters is needed! ### 
defsoilPars <- list('0_0_0_0' = list(step = 100,
                                  DD = 2067.585333, 
                                  DI = -59.56649925))

# reference: temperature sum (not average annual temperature)
defsoilPars2 <- list(a1 = 0.000387,# temperature sensitivity [°C/day]
                  a2 = 0.00325,# drought sensitivity [1/mm]
                  Tref = 1903,# Reference temperature sum [°C*day]
                  Dref = -32)# Reference drought index [mm]


dfs <- supplyDefSoil(SimName = SimName, 
                     soilPars = defsoilPars, soilPars2 = defsoilPars2, 
                     dir = outdir, overwrite = TRUE)


## create scaling file ####
scalParams <- list('0_0_0_0' = 1)
ScalScen <- "base" 

sca <- supplyMatScal(SimName = SimName, ScenName = ScalScen, 
                     scalParams = scalParams, dir = outdir, overwrite = TRUE)


## create thin change file ##
# thinning parameter file (how thinning changes over time, i.e. simulation steps)
thinParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    thin_change_lower = c(20,20,20),
                                    thin_change_upper = c(75,50,70)))
ThinScen <- "base"

thc <- supplyThinChange(SimName = SimName, ScenName = ThinScen, 
                        thinParams = thinParams, dir = outdir, overwrite = TRUE)


## create ffell change file ####
# final felling change scenario file
fellParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    ffelling = c(75,80,75)))
FellScen <- "base"

fel <- supplyFFellChange(SimName = SimName, ScenName = FellScen, 
                         fellParams = fellParams, dir = outdir, overwrite = TRUE)


## create scenario file ##
# note: look up if efs and scn file automatically searched for by runEFISCEN function
# missing: scaling, thin and fel change, defsoil, defcut, defrems, affor, defor, sp change

supplySCN(SimName = SimName, scnName = "base_scenario", dir = outdir,
          def_scn = def, # deforestation
          rem_def = dfr, # removals
          aff_scn = aff, # afforestation
          grow_ch = dgf, # growth change
          cut_reg = dfc, # define cutting / demand
          soil_cli = dfs, # soil change
          # thinn_cha = thc, # thinning change
          # fell_cha = fel,  # felling change
          overwrite = TRUE)

#both of the following parameters cannot be used, runEFISCEN will crash
#mat_sca = sca
#spec_cha = spch

## run main EFISCEN function ####
# either this 

opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=16, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), 
                    out = file.path(outdir, "out"))


# use output file path from runEFISCEN function as input
rsltD <- loadEFISCEN(path = file.path(outdir, "out"))


#plots
dir.create(file.path(outdir, "outD"))
pdf(file.path(outdir, "outD/res.pdf"), paper="a4r", width=20, height=10)
plot(rsltD)
dev.off()

pdf(file.path(outdir, "outD/res-1_0_0_0.pdf"), paper="a4r", width=20, height=10)
plot(rsltD, stratum = "1_0_0_0")
dev.off()

pdf(file.path(outdir, "outD/res-2_0_0_0.pdf"), paper="a4r", width=20, height=10)
plot(rsltD, stratum = "2_0_0_0")
dev.off()

pdf(file.path(outdir, "outD/res-3_0_0_0.pdf"), paper="a4r", width=20, height=10)
plot(rsltD, stratum = "3_0_0_0")
dev.off()


utils::browseURL(outdir)

