#' Author: christian.vonderach@forst.bwl.de
#' run EFISCEN using artificial data

rm(list=ls())
gc()
require(rEFISCEN)
require(readxl)
require(dplyr)

## Normalwald Scenario 1: no harvest ####
SimName <- "NW1" 

# EFIdir <- "J:/FVA-Projekte/P01770_SURGE_Pro/Daten/Ergebnisse/EFISCEN/"
EFIdir <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/"
time <- format(Sys.time(), "%Y%m%d-%H%M")
dir.create(outdir <- paste0(EFIdir, "EFI-", SimName, "_", time))
dir.create(file.path(outdir, "out"))
pdf(file.path(outdir, "out/input.pdf"), paper="a4r", width = 20, height = 10)

p <- "P:/SurgePro/Daten/Urdaten/wp5/ua/"
ua <- as.data.frame(readxl::read_xlsx(file.path(p, "DATA_UA_split.xlsx"), 
                                      sheet = "finalUA_MG"))
ua$siteclass <- NULL # remove bonitaet information, use dGZ100 as siteclass
ua$endage <- ua$endage - 1
colnames(ua) <- c("region", "owner", "siteclass", "species", "startage", 
                  "endage", "area", "volume", "increment")
weighted.mean(ua$increment, ua$area)

ua <- subset(ua, region==1 & owner==1 & siteclass==8 & species==1 & startage < 160)

YTinc <- read.csv2("P:/SURGEPro/Daten/aufbereiteteDaten/wp5/ua/cr-total-vol-growth-model.csv")
tmpltROS <- unique(ua[, c("region", "owner", "species")])
tmpltROSS <- merge(tmpltROS, data.frame(species=1, siteclass=sort(unique(YTinc$dGZ100))))
tmpltAGE <- unique(ua[, c("species", "startage", "endage")])
tmplt <- merge(tmpltROSS, tmpltAGE)
incrGPE <- merge(tmplt, YTinc, all.x = TRUE,
                 by.x = c("siteclass", "endage"), by.y = c("dGZ100", "age"))
incrGPE <- incrGPE[order(incrGPE$region, incrGPE$owner, incrGPE$siteclass, 
                         incrGPE$species, incrGPE$startage), 
                   c("region", "owner", "siteclass", "species", "startage",
                     "endage", "totalVol", "stock_wb", "pai")]
colnames(incrGPE)[7:9] <- c("volumeGWL", "volume", "increment") ## volume as stock_wb !!!
incrGPE <- subset(incrGPE, siteclass==8)
incrGPE$area <- 100
head(incrGPE)



invdata <- ua <- incrGPE
# View(ua)
# any(invdata[, c("area", "volume")]==0)
head(invdata)
ua$vol <- ua$volume*ua$area 
ua$class <- paste(ua$region, ua$owner, ua$siteclass, ua$species, sep = "_")
barplot(volume ~ class + endage, data=ua, beside=TRUE,
        ylab="volume [cbm/ha]", las=1,
        legend.text=c(unique(ua$siteclass)), 
        args.legend=list(x="topleft", title="siteclass"))
(wm <- weighted.mean(ua$volume, ua$area))
abline(h=wm)
(MAItotal <- weighted.mean(ua$inc, ua$area)) # periodic mean annual increment


## check increment ####
cl <- "1_1"
print(cl)
tmp <- ua[substr(ua$class, 0, 3) == cl,]
tmp$age <- rowMeans(tmp[, c("startage", "endage")])
reg <- strsplit(cl, split = "_")[[1]] [1]
own <- strsplit(cl, split = "_")[[1]] [2]
area <- round(sum(tmp$area, na.rm = TRUE), 0)
print(area <- format(area, nsmall=1, big.mark=","))
plot(increment ~ age, data=tmp, type="n", las=1, ylim=range(ua$increment, na.rm = TRUE),
     ylab="mean periodic annual increment")
print(area <- format(round(sum(tmp$area, na.rm = TRUE), 0), nsmall=1, big.mark=","))

cols <- rainbow(n=max(unique(tmp$siteclass)))
for(si in sort(unique(tmp$siteclass))){
  # si <- 17
  tmpp <- subset(tmp, siteclass==si)
  points(x=tmpp$age, y=tmpp$increment, type="b", col=cols[si])
}
(mincr <- mean(tmp$increment)) # 6.26
(mincr2 <- weighted.mean(tmp$increment, c(0.3, rep(1,7)))) # 6.71
abline(h=mincr, lty=2) # no weighted mean necessary: all age classes same area
text(x = 140, y=mincr, pos=3, labels="mean  increment")
(mincr100 <- mean(tmp[tmp$endage<=100,]$increment))
abline(h=mincr100) # no weighted mean necessary: all age classes same area
text(x = 140, y=mincr100, pos=3, labels="MAVI100/dGZ100")

legend("topright", legend=sort(unique(tmp$siteclass)), title = "siteclass",
       pch=1, lty=1, col=cols[sort(unique(tmp$siteclass))])
dev.off()

pdf(file.path(outdir, "out/base.pdf"), paper="a4r", width = 20, height = 10)
## initialise volume-age matrices ####
# with VWC1 being 75, results look quite good! 
# esp. bare forest land class is only 0.04 (without it is 0.07)
# and increment is below 7 as expected.
plist <- supplyP2009(invdata = invdata, ccode = SimName, outdir = outdir, 
                     plotTF = FALSE, 
                     control = list(corr=0.7, cv=0.65, R=1, VCW1=75)) 
aer <- rEFISCEN::runP2009(inv = plist, out = outdir)
# utils::browseURL(outdir) # show folder

## for running EFISCEN, several files are necessary
## 1 .efs, referencing to (supplyEFS())
## 2 .aer (and .vcl)
## 3 .prs (supplyPRS())
## 4 biomcomp-xxx.txt (supplyBioPar)
## 5 soil-xxx.par (supplySoilPar())


## create soil parameters file ####
# default values, used in EFISCEN for several countries, cf. Workshop Driebergen
# the *_defsoil.csv file holds degree days and drought index, specific for matrices
soilpars <- list('0_0_0_0'=
               list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
                    Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
                             klig=0.22, khum1=0.012, khum2=0.0012),
                    transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
                    litterComp=c(cw2cel=0.69, cw2sol=0.03, 
                                 fw2cel=0.65, fw2sol=0.03,
                                 nw2cel=0.51, nw2sol=0.27),
                    cddp=c(chum1=0.6, chum2=0.36)))
sf <- supplySoilPar(basename = SimName, dir = outdir, pars = soilpars, 
                    overwrite = TRUE)


## create parameters file ####
# 0_0_0_0 = Region_Owner_SiteClass_species = ForestType_ManagementType_SiteClass_Species
# origin ('origin'): "1 - planted / secondary; 2 - natural / primary; 3 - current year clearcuts and dendrological park"
# forest management ('region'): 1 - protected forest; 2 - protective forest; 3 - production forest
prspars <- list(YForest = list('0_0_0_0'=0.3), 
                Gamma = list('0_0_0_0'=0.4), 
                Harvest = list('0_0_0_0'=c(300)), # no final harvest
                Thinrange = list('0_0_0_0'=c(20, 20)), # no thinning
                MortAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 200)),
                # mortality rates for protected forests taken from yield table thinning amount (quite independent on SI)
                # adding a age-class > 120 and doubling the last mortality rate
                MortRates = list('0_0_0_0'=c(0.08, 0.0602, 0.0347, 0.0266, 0.0226, 0.0194, 2*0.0194)),
                # # mortality rates for used forests taken from BW inventory
                # MortRates = list('0_0_0_0'=c(0.0189, 0.0189, 0.0123, 0.0067, 0.0057, 0.0132, 0.0132)),
                Decay = list('0_0_0_0'=0.4), # fall rate of standing deadwood
                Thhistory = list('0_0_0_0'=0.2))
prs <- supplyPRS(SimName = SimName, dir = outdir, steps = 5, pars = prspars, 
                 overwrite = TRUE)


## create biocomp.txt file ####
biopars <- list(Carbon = list('0_0_0_0'=0.5),
                WoodDens = list('0_0_0_0'=0.37),
                ## bioComps taken from Driebergen workshop (same in GER & HUN)
                BioAgeLims = list('0_0_0_0'=c(20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 1000)),
                BioAllocations = list('0_0_0_0'=list(
                  "stem"=c(0.3495, 0.4244, 0.5419, 0.6060, 0.6373, 0.6488, 0.6489, 0.6478, 0.6460, 0.6416, 0.6306),
                  "branches"=c(0.3689, 0.2936, 0.1840, 0.1346, 0.1129, 0.1052, 0.1045, 0.1055, 0.1075, 0.1111, 0.1189),
                  "coarse roots"=c(0.0525, 0.0882, 0.1376, 0.1563, 0.1641, 0.1699, 0.1761, 0.1805, 0.1837, 0.1871, 0.1932),
                  "fine roots"=c(0.0529, 0.0447, 0.0315, 0.0238, 0.0198, 0.0176, 0.0163, 0.0153, 0.0145, 0.0139, 0.0132),
                  "foliage"=c(0.1762, 0.1491, 0.1050, 0.0793, 0.0659, 0.0585, 0.0542, 0.0509, 0.0483, 0.0463, 0.0441))),
                # origin of litter parameters unknown
                LitterAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                LitterProduction = list('0_0_0_0'= list(
                  "Stem"=c(0.0043, 0.0043, 0.0043, 0.0043, 0.0043, 0.0043, 0.0043),
                  "branches"=c(0.0614, 0.0463, 0.0266, 0.0142, 0.0091, 0.0076, 0.0071),
                  "coarse roots"=c(0.0614, 0.0463, 0.0266, 0.0142, 0.0091, 0.0076, 0.0071),
                  "fine roots"=c(0.4906, 0.4906, 0.4906, 0.4906, 0.4906, 0.4906, 0.4906),
                  "foliage"=c(0.21, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21))),
                WLCoarseRootsShare = list('0_0_0_0'=0.0))

biocomp <- supplyBioPar(SimName = SimName, biopars = biopars, dir = outdir, 
                        overwrite = TRUE)


## create efs file ####
usc <- unique(invdata$siteclass) # unique site class
sc <- paste0("dgz", usc, "=", usc, collapse=", ")
sc <- eval(parse(text=paste0("list(", sc, ")")))

supplyEFS(title = "Base scenario Normalwald", abbr = SimName, start = 2012,
          naming = list(region=c(natural=1), 
                        owner=c(production=1),
                        siteclass=sc,
                        species=c(spruce=1)), 
          files = list(prs=basename(prs), biomass=basename(biocomp),
                       aer=aer$outfiles[1], soil=basename(sf)),
          dir = outdir, overwrite = TRUE)

## now EFISCEN can be run (without detailed scenarios)
opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=16, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))

# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))


#plots
plot(rslt, each = FALSE)
dev.off()
utils::browseURL(outdir)


