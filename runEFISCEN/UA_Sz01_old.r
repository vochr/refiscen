#' Author: christian.vonderach@forst.bwl.de
#' run EFISCEN using Ukraine data

rm(list=ls())
gc()
require(rEFISCEN)
require(readxl)
require(dplyr)

# edit for own file path!
# p <- "J:/FVA-Projekte/P01770_SURGE_Pro/Daten/aufbereiteteDaten/UA"
p <- "P:/SurgePro/Daten/aufbereiteteDaten/"
ua <- readxl::read_xlsx(file.path(p, "DATA_UA_modCV.xlsx"), sheet = "finalUA")
ua$siteclass <- NULL # remove bonitaet information, use dGZ100 as siteclass
colnames(ua) <- c("siteclass", "startage", "endage", "area", "volume", 
                  "increment", "species", "owner", "region")
ua <- ua %>% 
  # filter(siteclass==13) %>% 
  group_by(siteclass) %>% 
  mutate(agg=1:n(),
         agg=ifelse(endage>=160, max(agg), agg)) %>% 
  group_by(siteclass, agg) %>% 
  mutate(volume = sum(area * volume) / sum(area),
         volume = ifelse(is.na(volume), 0, volume),
         area = sum(area),
         increment = mean(increment[increment > 0])) %>% 
  ungroup() %>% 
  mutate(agg=NULL) %>% 
  filter(startage <= 160)

## aggregate UA data to dGz100-classes
# 1-2-3, 4-5-6, 7-8-9, 10-11-12, 13-14-15, 16-17-18
unique(ua$siteclass) # hence: combine 13+15, 4+6
si <- data.frame(old = 1:18, new=rep(c(2, 5, 8, 11, 14, 17), each=3))

ua <- ua %>% 
  left_join(si, by = c("siteclass" = "old")) %>% 
  group_by(new, startage, endage, species, owner, region) %>% 
  summarise(m3ha = sum(area * volume) / sum(area),
            m3ha = ifelse(is.na(m3ha), 0, m3ha),
            area = sum(area),
            increment = weighted.mean(increment, ifelse(volume==0, 1, volume)),
            volume = m3ha,
            m3ha = NULL) %>% 
  ungroup() %>% 
  mutate(siteclass = new,
         new = NULL) %>% 
  select(siteclass, startage, endage, area, volume, increment, 
         species, owner, region)

# ua <- ua[ua$siteclass %in% c(18, 15, 13, 10),]
# ua <- ua[ua$siteclass != c(10),]

invdata <- ua
View(ua)
# any(invdata[, c("area", "volume")]==0)
head(invdata)
ua$vol <- ua$volume*ua$area
barplot(volume ~ endage + siteclass, data=ua, beside=TRUE,
        ylab="volume [cbm/ha]", las=1,
        legend.text=c(unique(ua$siteclass)), 
        args.legend=list(x="topleft", title="siteclass"))
wm <- weighted.mean(ua$volume, ua$area)
abline(h=wm)

## Ukraine Scenario 1 ####
SimName <- "UA1" 

# outdir <- tempdir()
EFIdir <- "C:/temp_cv/"
EFIdir <- "J:/FVA-Projekte/P01770_SURGE_Pro/Daten/Ergebnisse/EFISCEN/"
time <- format(Sys.time(), "%Y%m%d")
dir.create(outdir <- paste0(EFIdir, "EFI-", SimName, "_", time))

## initialise volume-age matrices ####
plist <- supplyP2009(invdata, SimName, outdir)
aer <- rEFISCEN::runP2009(inv = plist, out = outdir)
# shell.exec(outdir) # show folder

## for running EFISCEN, several files are necessary
## 1 .efs, referencing to (supplyEFS())
## 2 .aer (and .vcl)
## 3 .prs (supplyPRS())
## 4 biomcomp-xxx.txt (supplyBioPar)
## 5 soil-xxx.par (supplySoilPar())


## create soil parameters file ####
# default values, used in EFISCEN for several countries, cf. Workshop Driebergen
# the *_defsoil.csv file holds degree days and drought index, specific for matrices
soilpars <- list('0_0_0_0'=
               list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
                    Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
                             klig=0.22, khum1=0.012, khum2=0.0012),
                    transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
                    litterComp=c(cw2cel=0.69, cw2sol=0.03, 
                                 fw2cel=0.65, fw2sol=0.03,
                                 nw2cel=0.51, nw2sol=0.27),
                    cddp=c(chum1=0.6, chum2=0.36)))
sf <- supplySoilPar(basename = SimName, dir = outdir, pars = soilpars, 
                    overwrite = TRUE)


## create parameters file ####
# 
prspars <- list(YForest = list('0_0_0_0'=0.7), 
             Gamma = list('0_0_0_0'=0.4), 
             Harvest = list('0_0_0_0'=c(61, 100, 0.1, 1, 0.015, 0.2)),
             Thinrange = list('0_0_0_0'=c(60, 60)),
             MortAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 200)),
             # file 0001_result_Ficht_voll.xlsx from M.Korol: deadvol/totalvol ~ age
             MortRates = list('0_0_0_0'=c(0.078, 0.078, 0.078, 0.078, 0.078, 0.078, 0.078)), 
             Decay = list('0_0_0_0'=0.4), # fall rate of standing deadwood
             Thhistory = list('0_0_0_0'=0.2))
prs <- supplyPRS(SimName = SimName, dir = outdir, steps = 5, pars = prspars, 
                 overwrite = TRUE)


## create biocomp.txt file ####
biopars <- list(Carbon = list('0_0_0_0'=0.5),
                WoodDens = list('0_0_0_0'=0.37),
                ## bioComps taken from Driebergen workshop (same in GER & HUN)
                BioAgeLims = list('0_0_0_0'=c(20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 1000)),
                BioAllocations = list('0_0_0_0'=list(
                  "stem"=c(0.3495, 0.4244, 0.5419, 0.6060, 0.6373, 0.6488, 0.6489, 0.6478, 0.6460, 0.6416, 0.6306),
                  "branches"=c(0.3689, 0.2936, 0.1840, 0.1346, 0.1129, 0.1052, 0.1045, 0.1055, 0.1075, 0.1111, 0.1189),
                  "coarse roots"=c(0.0525, 0.0882, 0.1376, 0.1563, 0.1641, 0.1699, 0.1761, 0.1805, 0.1837, 0.1871, 0.1932),
                  "fine roots"=c(0.0529, 0.0447, 0.0315, 0.0238, 0.0198, 0.0176, 0.0163, 0.0153, 0.0145, 0.0139, 0.0132),
                  "foliage"=c(0.1762, 0.1491, 0.1050, 0.0793, 0.0659, 0.0585, 0.0542, 0.0509, 0.0483, 0.0463, 0.0441))),
                # origin of litter parameters unknown
                LitterAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                LitterProduction = list('0_0_0_0'= list(
                  "Stem"=c(0.0043, 0.0043, 0.0043, 0.0043, 0.0043, 0.0043, 0.0043),
                  "branches"=c(0.0614, 0.0463, 0.0266, 0.0142, 0.0091, 0.0076, 0.0071),
                  "coarse roots"=c(0.0614, 0.0463, 0.0266, 0.0142, 0.0091, 0.0076, 0.0071),
                  "fine roots"=c(0.4906, 0.4906, 0.4906, 0.4906, 0.4906, 0.4906, 0.4906),
                  "foliage"=c(0.21, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21))),
                WLCoarseRootsShare = list('0_0_0_0'=0.0))

biocomp <- supplyBioPar(SimName = SimName, biopars = biopars, dir = outdir, 
                        overwrite = TRUE)


## create efs file ####
usc <- unique(invdata$siteclass) # unique site class
sc <- paste0("dgz", usc, "=", usc, collapse=", ")
sc <- eval(parse(text=paste0("list(", sc, ")")))

supplyEFS(title = "Base scenario Ukraine", abbr = SimName, start = 2012,
          naming = list(region=c(uaf=1), owner=c(state=1),
                        siteclass=sc,
                        species=c(spruce=1)), 
          files = list(prs=basename(prs), biomass=basename(biocomp),
                       aer=aer$outfiles[1], soil=basename(sf)),
          dir = outdir, overwrite = TRUE)

## now EFISCEN can be run (without detailed scenarios)
opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=8, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))


# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))

pdf(file.path(outdir, "out/base.pdf"))
#plots
plot(rslt)
dev.off()

################################################################################
## defining scenario files
################################################################################

## create defgrow file ####
# growth change file
grthScnName <- "Fast Climate Change - positive growth change"
growPars <- list(agelimits = list('0_0_0_0' = c(20, 40, 60, 80, 100, 160, 300)),
                 growth = list("2" = list('0_0_0_0' = c(1,1,1,1,1,1,1)),
                               "6" = list('0_0_0_0' = c(1.1,1.1,1.1,1.1,1.1,1.1,1.1)),
                               "10" = list('0_0_0_0' = c(1.3,1.3,1.3,1.3,1.3,1.3,1.3))))

dgf <- supplyDefGrow(SimName, growPars = growPars, grthScnName = grthScnName, 
                     dir = outdir, overwrite = TRUE)


## create defrems file ####
# Removals scenario file
DefRemsPars <- list('0_0_0_0' = list(steps = c(100),
                                     Fel_stem = c(0.85),
                                     Fel_tops = c(0.0),
                                     Fel_branches = c(0),
                                     Fel_leaves = c(0),
                                     Fel_croots = c(0),
                                     Fel_dwood = c(0),
                                     Thin_stem = c(0.85),
                                     Thin_tops = c(0),
                                     Thin_branches = c(0),
                                     Thin_leaves = c(0),
                                     Thin_croots = c(0),
                                     Thin_dwood = c(0)))
DefRemsScen <- "based on TBFRA"

dfr <- supplyDefRems(SimName = SimName, ScenName = DefRemsScen, dir = outdir, 
                     DefRemsPars = DefRemsPars, overwrite = TRUE)


## create sp_change file ####
# species changes file
SpChParams <- list('100' = list(matrices = c('0_0_0_0'),
                                dest_species = c(8),
                                fract_change = c(0.6)))
SpChScen <- "base"

spch <- supplySpChange(SimName = SimName, ScenName = SpChScen, 
                       SpChParams = SpChParams, dir = outdir, overwrite = TRUE)


## create affor file ####
# afforestation scenario file
AffParams <- list('0_0_0_0' = list(steps = c(100),
                                   affor_area = c(0),
                                   affor_carbon = c(0)))
AffScen <- "base"

aff <- supplyAffor(SimName = SimName, ScenName = AffScen, AffParams = AffParams, 
                   dir = outdir, overwrite = TRUE)


## create defor file ####
# afforestation scenario file
# no deforestation of pure forests in UA?
DeforParams <- list('0_0_0_0' = list(steps = c(100),
                                     defor_area = c(0)))
DeforScen <- "base"

def <- supplyDefor(SimName = SimName, ScenName = DeforScen, 
                   DeforParams = DeforParams, dir = outdir, overwrite = TRUE)


## create defcut file ####
# Forest harvest scenario file
defCutPars <- list('0_0_0_0' = list(step = c(2,4),
                                   felling = c(9000,9100), 
                                   thinning = c(3000,3050)))
DefCutScen <- "base"

dfc <- supplyDefCut(SimName = SimName, ScenName = DefCutScen, 
                    DefCutPars = defCutPars, dir = outdir, overwrite = TRUE)


## create defsoil file ####
### adjust code, because only one of two parameters is needed! ### 
defsoilPars <- list('0_0_0_0' = list(step = 100,
                                  DD = 2067.585333, 
                                  DI = -59.56649925))

# reference: temperature sum (not average annual temperature)
defsoilPars2 <- list(a1 = 0.000387,# temperature sensitivity [°C/day]
                  a2 = 0.00325,# drought sensitivity [1/mm]
                  Tref = 1903,# Reference temperature sum [°C*day]
                  Dref = -32)# Reference drought index [mm]


dfs <- supplyDefSoil(SimName = SimName, 
                     soilPars = defsoilPars, soilPars2 = defsoilPars2, 
                     dir = outdir, overwrite = TRUE)


## create scaling file ####
scalParams <- list('0_0_0_0' = 1)
ScalScen <- "base" 

sca <- supplyMatScal(SimName = SimName, ScenName = ScalScen, 
                     scalParams = scalParams, dir = outdir, overwrite = TRUE)


## create thin change file ##
# thinning parameter file (how thinning changes over time, i.e. simulation steps)
thinParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    thin_change_lower = c(20,20,20),
                                    thin_change_upper = c(75,50,70)))
ThinScen <- "base"

thc <- supplyThinChange(SimName = SimName, ScenName = ThinScen, 
                        thinParams = thinParams, dir = outdir, overwrite = TRUE)


## create ffell change file ####
# final felling change scenario file
fellParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    ffelling = c(75,80,75)))
FellScen <- "base"

fel <- supplyFFellChange(SimName = SimName, ScenName = FellScen, 
                         fellParams = fellParams, dir = outdir, overwrite = TRUE)


## create scenario file ##
# note: look up if efs and scn file automatically searched for by runEFISCEN function
# missing: scaling, thin and fel change, defsoil, defcut, defrems, affor, defor, sp change

supplySCN(SimName = SimName, scnName = "base_scenario", dir = outdir,
          def_scn = def, rem_def = dfr, aff_scn = aff, 
          grow_ch = dgf, cut_reg = dfc, soil_cli = dfs,
          thinn_cha = thc, fell_cha = fel, overwrite = TRUE)

#both of the following parameters cannot be used, runEFISCEN will crash
#mat_sca = sca
#spec_cha = spch

## run main EFISCEN function ####
# either this 

opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=8, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), 
                    out = file.path(outdir, "out"))


# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))


#plots
pdf(file.path(outdir, "out/res.pdf"), paper="a4r", width=20, height=10)
plot(rslt)
dev.off()
