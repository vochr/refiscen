myEnv <- new.env()
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/BW_Sz01.r", 
       local = myEnv)
p1 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/BW_Sz02.r", 
       local = myEnv)
p2 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/BW_Sz03.r", 
       local = myEnv)
p3 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/BW_Sz04.r", 
       local = myEnv)
p4 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/BW_Sz05.r", 
       local = myEnv)
p5 <- file.path(myEnv$outdir, "out")
source("P:/SurgePro/Programme/Eigenentwicklung/rEFISCEN/runEFISCEN/BW_Sz06.r", 
       local = myEnv)
p6 <- file.path(myEnv$outdir, "out")

p1 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW1_20240715-1320/out/"
p2 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW2_20240715-1320/out/"
p3 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW3_20240715-1321/out/"
p4 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW4_20240715-1321/out/"
p5 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW5_20240715-1321/out/"
p6 <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW6_20240715-1321/out/"

path <- c(p6, p5, p4, p3, p2, p1) 
rslt <- rEFISCEN::loadEFISCEN(path, selection=NULL, run=NULL)
str(rslt, max.level = 1)
class(rslt)
p <- dirname(path[length(path)])
d <- paste0(basename(p), "-all")
dir.create(p <- file.path(dirname(p), d))

for(s in c("0_0_0_0")){
  # s <- "0_0_0_0"
  f <- paste0("BW_all-Sz_Stratum=", s, ".pdf")
  pdf(file=file.path(p, f), paper="a4r", width=20, height=10)
  ## no loss in area in BW1
  tmp <- rEFISCEN::loadEFISCEN(p1)
  plot(tmp, var="area-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  ## loss in area only in BW2
  tmp <- rEFISCEN::loadEFISCEN(p6)
  plot(tmp, var="area-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  
  plot(rslt, vars = "volume-rel", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  
  plot(rslt, vars = "volume-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  
  plot(rslt, vars = "increment-rel", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "increment-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "increment-vba", stratum = s, each = FALSE, combine = c(TRUE, FALSE))

  plot(rslt, vars = "harvest-rel", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "harvest-abs", stratum = s, each = FALSE, combine = c(TRUE, FALSE)) 
  plot(rslt, vars = "harvest-ratio", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  plot(rslt, vars = "age", stratum = s, each = FALSE, combine = c(TRUE, FALSE))
  
  dev.off()
}

