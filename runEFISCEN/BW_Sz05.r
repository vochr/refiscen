#' Author: christian.vonderach@forst.bwl.de
#' Date: 21.11.2023
#' run EFISCEN using Baden-Württemberg data
#' alternative scenario

rm(list=ls())
gc()
require(rEFISCEN)

e <- new.env()

# edit for own file path!
# load(system.file("data","bw_spruce.rdata", package = "rEFISCEN"), envir = e)
load("P:/SurgePro/Daten/aufbereiteteDaten/BWI2EFISCEN/bw_spruce.rdata", envir = e)

ls(e)
res <- e[["res"]]

invdata <- res

invdata$VolR_FAO_bw <- NULL

colnames(invdata) <- c("startage", "siteclass", "area", "volume", "increment")
invdata$endage <- invdata$startage + 20
invdata$startage <- invdata$startage + 1
invdata <- invdata[, c("siteclass", "startage", "endage", "area", "volume", "increment")]
invdata$region <- invdata$owner <- invdata$species <- 1
## replace zero values by temporary values to avoid strange estimates in .vcl
invdata[invdata$endage==20 & invdata$siteclass==5, "area"] <- 0
invdata[invdata$endage==20 & invdata$siteclass==5, "volume"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "area"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "volume"] <- 10
agg <- aggregate(area~ endage, data=invdata, FUN="sum")
barplot(area ~ endage, data=agg)
any(invdata[, c("area", "volume")]==0)
head(invdata)
sum(invdata$area) # in 2012 about 270.000 ha
weighted.mean(invdata$increment, invdata$area) # 17.34 m3/(ha*a)
weighted.mean(invdata$volume, invdata$area) # 462 m3/ha
weighted.mean(invdata$endage, invdata$area) - 10 # 65 m3/ha

plot(increment ~ endage, data=invdata, type="n", las=1, 
     main="laufender periodischer Zuwachs (BWI 2002-2012)")
dgz <- c(5, 8, 11, 14, 17)
for(d in dgz){
  # d <- 5
  sdf <- subset(invdata, siteclass==d)
  points(x=sdf$endage, y=sdf$increment, type="o", lty=which(dgz==d))
  print(weighted.mean(sdf$increment, sdf$area))
}
abline(h=weighted.mean(invdata$increment, invdata$area))
legend("topright", legend = dgz, title="dGz100", lty=1:5)

barplot(area ~ endage + siteclass, data=invdata, beside=TRUE,
        main="Area distribution BW\n(Area ~ Age + dGz100)", legend=TRUE)

SimName <- "BW5" # baden württemberg state forest
if(nchar(SimName)!=3){
  stop("SimName must have exactly three characters!")
}else{
  print("abbreviation is good to go!")
}

# EFIdir <- "C:/temp_cv/"
EFIdir <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/"
time <- format(Sys.time(), "%Y%m%d-%H%M")
dir.create(outdir <- paste0(EFIdir, "EFI-", SimName, "_", time))

## initialise volume-age matrices ####
plist <- supplyP2009(invdata, SimName, outdir)
aer <- rEFISCEN::runP2009(inv = plist, out = outdir)
# shell.exec(outdir) # show folder

## for running EFISCEN, several files are necessary
## 1 .efs, referencing to (supplyEFS())
## 2 .aer (and .vcl)
## 3 .prs (supplyPRS())
## 4 biomcomp-xxx.txt (supplyBioPar)
## 5 soil-xxx.par (supplySoilPar())


## create soil parameters file ####
soilpars <- list('0_0_0_0'=
               list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
                    Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
                             klig=0.22, khum1=0.012, khum2=0.0012),
                    transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
                    litterComp=c(cw2cel=0.69, cw2sol=0.03, 
                                 fw2cel=0.65, fw2sol=0.03,
                                 nw2cel=0.51, nw2sol=0.27),
                    cddp=c(chum1=0.6, chum2=0.36)))
sf <- supplySoilPar(basename = SimName, dir = outdir, pars = soilpars, 
                    overwrite = TRUE)



## create parameters file ####
prspars <- list(YForest = list('0_0_0_0'=0.6),
             Gamma = list('0_0_0_0'=0.4),
             #min_age max_age min_prob_tresh max_prob_tresh level_below, percentage decrease of min age
             Harvest = list('0_0_5_0'=c(105, 130, 0.4, 0.95, 0.015, 0.2),
                            '0_0_8_0'=c(95, 120, 0.4, 0.95, 0.015, 0.2),
                            '0_0_11_0'=c(85, 110, 0.4, 0.95, 0.015, 0.2),
                            '0_0_14_0'=c(75, 100, 0.4, 0.95, 0.015, 0.2),
                            '0_0_17_0'=c(65, 90, 0.4, 0.95, 0.015, 0.2)),
             Thinrange = list('0_0_5_0'=c(30, 100),
                              '0_0_8_0'=c(30, 90),
                              '0_0_11_0'=c(25, 80),
                              '0_0_14_0'=c(20, 70),
                              '0_0_17_0'=c(20, 60)),
             MortAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 140, 160, 200)),
             # 5yrs-Mortality rates from Daten/Urdaten/wp5/bw/bwi/
             # 5yrs-interval in BWI3_Mortality.r
             MortRates = list('0_0_0_0'=c(0.0109, 0.0189, 0.0123, 0.0067, 0.0057, 
                                          0.0052, 0.0054, 0.0132, 0.0105)*2),
             Decay = list('0_0_0_0'=0.4), # deadwood fall rate
             Thhistory = list('0_0_0_0'=0.2))
prs <- supplyPRS(SimName = SimName, dir = outdir, steps = 5, pars = prspars, 
                 overwrite = TRUE)


## create biocomp.txt file ####
# see P01770_SURGE_Pro/Daten/aufbereiteteDaten/EFISCEN/wp5/bw/BWI-comp.csv
# to calculate component shares (no roots available, but check Riedel-NFI-documentation)
biopars <- list(Carbon = list('0_0_0_0'=0.51), # IPCC 2019, refinement of guidelines
                WoodDens = list('0_0_0_0'=0.37), # Kollmann 1982
                BioAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                BioAllocations = list('0_0_0_0'=list("stem"=c(0.557,
                                      0.569, 0.581, 0.59, 0.598, 0.605, 0.611),
                                      "branches"=c(0.15, 0.143, 0.138,
                                      0.134, 0.131, 0.128, 0.125),
                                      "coarse roots"=c(0.133, 0.143,
                                      0.151, 0.157, 0.163, 0.167, 0.171),
                                      "fine roots"=c(0.072, 0.067, 0.062,
                                      0.058, 0.054, 0.051, 0.049),
                                      "foliage"=c(0.088, 0.078, 0.068,
                                      0.061, 0.054, 0.049, 0.044))),
                LitterAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                LitterProduction = list('0_0_0_0'= list("Stem"=c(0.0043, 0.0043,
                                       0.0043, 0.0043, 0.0043, 0.0043, 0.0043),
                                       "branches"=c(0.0614, 0.0463, 0.0266,
                                       0.0142, 0.0091, 0.0076, 0.0071),
                                       "coarse roots"=c(0.0614, 0.0463, 0.0266, 
                                       0.0142, 0.0091, 0.0076, 0.0071),
                                       "fine roots"=c(0.4906, 0.4906, 0.4906,
                                       0.4906, 0.4906, 0.4906, 0.4906),
                                       "foliage"=c(0.21, 0.21, 0.21, 0.21, 0.21,
                                       0.21, 0.21))),
                WLCoarseRootsShare = list('0_0_0_0'=0.0))

biocomp <- supplyBioPar(SimName = SimName, biopars = biopars, dir = outdir, 
                        overwrite = TRUE)


## create efs file ####
usc <- unique(invdata$siteclass) # unique site class
sc <- paste0("dgz", usc, "=", usc, collapse=", ")
sc <- eval(parse(text=paste0("list(", sc, ")")))

supplyEFS(title = "Base scenario Baden-Württemberg", abbr = SimName, start = 2012,
          naming = list(region=c(bw=1), owner=c(state=1),
                        siteclass=sc,
                        species=c(spruce=1)), 
          files = list(prs=basename(prs), biomass=basename(biocomp),
                       aer=aer$outfiles[1], soil=basename(sf)),
          dir = outdir, overwrite = TRUE)

## now EFISCEN can be run (without detailed scenarios)
opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=16, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))

# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))


#plots
pdf(file.path(outdir, "out/base.pdf"), paper="a4r", width = 20, height = 10)
plot(rslt)
dev.off()
# utils::browseURL(outdir)


################################################################################
## defining scenario files
################################################################################

## create defgrow file ####
# growth change file
grthScnName <- "growth change according to LPG / Mykola Gusti"
growPars <- list(agelimits = list('0_0_0_0' = c(20, 40, 60, 80, 100, 160, 300)),
                 growth = list("1" = list('0_0_0_0' = rep(0.96, 7)), # 2017
                               "2" = list('0_0_0_0' = c(0.920, 0.930, 0.94, 0.95, 0.950, 0.950, 0.950)), # 2022
                               "3" = list('0_0_0_0' = c(0.905, 0.905, 0.93, 0.94, 0.945, 0.947, 0.947)), # 2027
                               "4" = list('0_0_0_0' = c(0.890, 0.890, 0.92, 0.93, 0.940, 0.943, 0.943)), # 2032
                               "5" = list('0_0_0_0' = c(0.875, 0.875, 0.90, 0.92, 0.930, 0.940, 0.940)), # 2037
                               "6" = list('0_0_0_0' = c(0.860, 0.860, 0.89, 0.91, 0.925, 0.937, 0.937)), # 2042
                               "7" = list('0_0_0_0' = c(0.845, 0.845, 0.88, 0.90, 0.920, 0.933, 0.933)), # 2047
                               "8" = list('0_0_0_0' = c(0.830, 0.830, 0.87, 0.89, 0.915, 0.930, 0.930)), # 2052
                               "9" = list('0_0_0_0' = c(0.815, 0.815, 0.85, 0.88, 0.910, 0.927, 0.927)), # 2057
                               "10" = list('0_0_0_0' = c(0.800, 0.800, 0.84, 0.87, 0.900, 0.923, 0.923)), # 2062
                               "11" = list('0_0_0_0' = c(0.785, 0.785, 0.83, 0.86, 0.895, 0.920, 0.920)), # 2067
                               "12" = list('0_0_0_0' = c(0.770, 0.770, 0.81, 0.85, 0.890, 0.917, 0.917)), # 2072
                               "13" = list('0_0_0_0' = c(0.755, 0.755, 0.80, 0.84, 0.985, 0.913, 0.913)), # 2077
                               "14" = list('0_0_0_0' = c(0.740, 0.740, 0.79, 0.83, 0.980, 0.910, 0.910)), # 2082
                               "15" = list('0_0_0_0' = c(0.720, 0.720, 0.77, 0.82, 0.970, 0.905, 0.905)), # 2087
                               "20" = list('0_0_0_0' = c(0.700, 0.700, 0.75, 0.80, 0.850, 0.900, 0.900)) # 2092+
                 ))

dgf <- supplyDefGrow(SimName, growPars = growPars, grthScnName = grthScnName, 
                     dir = outdir, overwrite = TRUE)


## create defrems file ####
# Removals scenario file
# no use of deadwood
DefRemsPars <- list('0_0_0_0' = list(steps = c(100),
                                     Fel_stem = c(0.9),
                                     Fel_tops = c(0.0),
                                     Fel_branches = c(0),
                                     Fel_leaves = c(0),
                                     Fel_croots = c(0),
                                     Fel_dwood = c(0),
                                     Thin_stem = c(0.9),
                                     Thin_tops = c(0),
                                     Thin_branches = c(0),
                                     Thin_leaves = c(0),
                                     Thin_croots = c(0),
                                     Thin_dwood = c(0)))
DefRemsScen <- "based on TBFRA"

dfr <- supplyDefRems(SimName = SimName, ScenName = DefRemsScen, dir = outdir, 
                     DefRemsPars = DefRemsPars, overwrite = TRUE)


## create sp_change file ####
# species changes file
SpChParams <- list('100' = list(matrices = c('0_0_0_0'),
                                dest_species = c(8),
                                fract_change = c(0.0)))
SpChScen <- "base"

spch <- supplySpChange(SimName = SimName, ScenName = SpChScen, 
                       SpChParams = SpChParams, dir = outdir, overwrite = TRUE)


## create affor file ####
# afforestation scenario file
AffParams <- list('0_0_0_0' = list(steps = c(100),
                                   affor_area = c(0),
                                   affor_carbon = c(0)))
AffScen <- "base"

aff <- supplyAffor(SimName = SimName, ScenName = AffScen, AffParams = AffParams, 
                   dir = outdir, overwrite = TRUE)


## create defor file ####
# no deforestation
DeforParams <- list('0_0_0_0' = list(steps = c(100),
                                     defor_area = c(0)))
DeforScen <- "base"

def <- supplyDefor(SimName = SimName, ScenName = DeforScen, 
                   DeforParams = DeforParams, dir = outdir, overwrite = TRUE)


## create defcut file ####
area <- rslt$BW5$volume %>% 
  filter(Step == 2012) %>% 
  group_by(REG, OWN) %>% 
  summarise(area = sum(Area)*1000) %>% #in [ha]
  as.data.frame()
sum(area$area)

rems <- rslt$BW5$volume %>% 
  mutate(ThinRemsAv = RemsAv * (ThinRems / (ThinRems + FelRems)),
         FelRemsAv = RemsAv * (FelRems / (ThinRems + FelRems))) %>% 
  group_by(REG, OWN) %>% 
  summarise(ThinAv = weighted.mean(ThinRemsAv, Area, na.rm=TRUE),
            FelAv = weighted.mean(FelRemsAv, Area, na.rm=TRUE)) %>% 
  mutate(ratio = ThinAv / (ThinAv + FelAv)) %>% 
  as.data.frame()
rems

inc <- rslt$BW5$volume %>% 
  group_by(REG, OWN) %>% 
  summarise(inc = weighted.mean(IncrAv, Area)) %>%
  inner_join(area) %>% 
  mutate(inc_ha = inc * area, # total increment per year
         inc_ha_5a = inc_ha * 5 / 1000) %>% # per simulation step in Thousand cbm
  as.data.frame()
head(inc)

# in BW ~ 16.7 cbm_inc/ha/a on 270533ha = 4.519619cbm_inc/a => 22.6 Mio m³/5a
# without harvest loss, because defined from increment not from harvest
## inc / harvest = 0.797 (1987-2002)
## inc / harvest = 0.845 (2002-2012)
# P:\SurgePro\Daten\Urdaten\wp1\Table_1_Spruce_BaWue_v1_commented.xlsx
ratio <- 0.845

defCutPars <- list('0_0_0_0' = list(step = c(16),
                                    felling = c(inc$inc_ha_5a*0.9 * ratio * (1-rems$ratio)), 
                                    thinning = c(inc$inc_ha_5a*0.9 * ratio * rems$ratio)))
DefCutScen <- "base"
dfc <- supplyDefCut(SimName = SimName, ScenName = DefCutScen, 
                    DefCutPars = defCutPars, dir = outdir, overwrite = TRUE)


## create defsoil file ####
### adjust code, because only one of two parameters is needed! ### 
defsoilPars <- list('0_0_0_0' = list(step = 100,
                                  DD = 2067.585333, 
                                  DI = -59.56649925))

# reference: temperature sum (not average annual temperature)
defsoilPars2 <- list(a1 = 0.000387,# temperature sensitivity [°C/day]
                  a2 = 0.00325,# drought sensitivity [1/mm]
                  Tref = 1903,# Reference temperature sum [°C*day]
                  Dref = -32)# Reference drought index [mm]


dfs <- supplyDefSoil(SimName = SimName, soilPars = defsoilPars, soilPars2 = defsoilPars2, dir = outdir, 
                     overwrite = TRUE)


## create scaling file ####
scalParams <- list('0_0_0_0' = 1)
ScalScen <- "base" 

sca <- supplyMatScal(SimName = SimName, ScenName = ScalScen, 
                     scalParams = scalParams, dir = outdir, overwrite = TRUE)


## create thin change file ####
# thinning parameter file (how thinning changes over time, i.e. simulation steps)
# in this scenario no change in thinning
thinParams <- list('0_0_5_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(120)),
                   '0_0_8_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(110)),
                   '0_0_11_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(100)),
                   '0_0_14_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(90)),
                   '0_0_17_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(80)))
ThinScen <- "base"


thc <- supplyThinChange(SimName = SimName, ScenName = ThinScen, 
                        thinParams = thinParams, dir = outdir, overwrite = TRUE)


## create ffell change file ####
# final felling change scenario file
fellParams <- list('0_0_5_0' = list(num_Step = c(100),
                                    ffelling = c(125)),
                   '0_0_8_0' = list(num_Step = c(100),
                                    ffelling = c(115)),
                   '0_0_11_0' = list(num_Step = c(100),
                                    ffelling = c(105)),
                   '0_0_14_0' = list(num_Step = c(100),
                                    ffelling = c(95)),
                   '0_0_17_0' = list(num_Step = c(100),
                                    ffelling = c(85)))
FellScen <- "base"

fel <- supplyFFellChange(SimName = SimName, ScenName = FellScen, 
                         fellParams = fellParams, dir = outdir, overwrite = TRUE)


## create scenario file ##
# note: look up if efs and scn file automatically searched for by runEFISCEN function

supplySCN(SimName = SimName, scnName = "no_scenario", dir = outdir,
          def_scn = def, # deforestation
          rem_def = dfr, # removals
          aff_scn = aff, # afforestation
          grow_ch = dgf, # growth change
          cut_reg = dfc, # define cutting / demand
          soil_cli = dfs, # soil change
          # thinn_cha = thc, # thinning change
          # fell_cha = fel,  # felling change
          overwrite = TRUE)

#both of the following parameters cannot be used, runEFISCEN will crash
#mat_sca = sca
#spec_cha = spch

## run main EFISCEN function ####

opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=16, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))
rsltD <- loadEFISCEN(path = file.path(outdir, "out"))

dir.create(file.path(outdir, "outD"))
pdf(file.path(outdir, "outD", "BW_Sz02.pdf"), paper = "a4r", width = 20, height = 10)

plot(rsltD) # plots from Package based on EFISCEN-object

dev.off()

shell.exec(file.path(outdir, "outD")) # show folder
