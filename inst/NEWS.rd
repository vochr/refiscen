\name{NEWS}
\title{NEWS}

\section{Changes in version 0.3.0}{
\itemize{
\item added function to create scenario files
\item bug fixes
}
}

\section{Changes in version 0.2.0}{
\itemize{
\item first version with correct(?) contributions acknowledgement
\item adding functions to provide modelling data directly from within R
}
}

\section{Changes in version 0.1.0}{
\itemize{
\item first version with core functionality working (calling EFISCEN from within R)
}
}

