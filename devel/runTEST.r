#' Author: christian.vonderach@forst.bwl.de
#' for testing purpose
#' especially for implementing thinning and felling changes

rm(list=ls())
gc()
require(rEFISCEN)

e <- new.env()

# edit for own file path!
load(system.file("data","bw_spruce.rdata", package = "rEFISCEN"), envir = e)
load(system.file("data","ytBW.rdata", package = "rEFISCEN"), envir = e)
ls(e)
res <- e[["res"]]
invdata <- res

invdata$VolR_FAO_bw <- NULL

colnames(invdata) <- c("startage", "siteclass", "area", "volume", "increment")
invdata$endage <- invdata$startage + 20
invdata$startage <- invdata$startage + 1
invdata <- invdata[, c("siteclass", "startage", "endage", "area", "volume", "increment")]
invdata$region <- invdata$owner <- invdata$species <- 1

## aggregate yield table values
ytBW <- e[["ytBW"]]
ytBW[which(ytBW$increment==0), "increment"] <- NA
ytBW$endage2 <- ((ytBW$endage-1) %/% 20 + 1) * 20
# extent for siteclass 5 and 17
plot(increment ~ endage, data=ytBW, type="n", las=1, main="comparison of YT vs. NFI")
for(i in 5:17){
  points(x=ytBW[ytBW$siteclass==i, "endage"], y=ytBW[ytBW$siteclass==i, "increment"], 
         type="o")
  points(x=invdata[invdata$siteclass==i, "endage"], y=invdata[invdata$siteclass==i, "increment"], 
         type="o", lty=2, col=2)
}
legend("topright", legend=c("YT", "NFI"), lty=c(1, 2), col=c(1,2))

ytBW <- aggregate(x=list(incrementYT=ytBW[, c("increment")]), 
                  by=list(endage=ytBW$endage2, siteclass=ytBW$siteclass),
                  FUN="mean", na.rm=TRUE)
ytBW[which(ytBW$siteclass==6),"siteclass"] <- 5
ytBW[which(ytBW$siteclass==16),"siteclass"] <- 17
invdata2 <- merge(invdata, ytBW, by=c("siteclass", "endage"), all.x = TRUE)
invdata2 <- invdata2[order(invdata2$siteclass, invdata2$endage),]
# View(invdata2)

invdata <- invdata2
invdata$increment <- invdata$incrementYT
invdata$incrementYT <- NULL
invdata[is.na(invdata$increment), "increment"] <- 0
plot(area ~ endage, data=invdata, subset=(siteclass==8),
     main="Area vs. ageclass for siteclass 8")
any(invdata[, c("area", "volume")]==0)
head(invdata)

## compare volume against yield table
ytBW <- e$ytBW
plot(volume ~ endage, data=invdata, xlim=c(0, 200), type="n", las=1)
usi <- unique(invdata$siteclass)
cols <- rainbow(n=length(usi))
for(i in seq(along=usi)){
  # i <- 2
  si <- usi[i]
  df <- subset(invdata, siteclass==si)
  points(x=df$endage, y=df$volume, type="l", col=cols[i])
  yt <- subset(ytBW, siteclass==si)
  if(!nrow(yt)==0){
    points(x=yt$endage, y=yt$volume, type="l", col=cols[i], lty=2)
  }
}
legend("bottomright", legend = c("yield table", "inventory"), lty=c(1,2))
legend("topleft", legend = paste0("si: ", usi), col=cols, lty=1)

SimName <- "tst" # baden württemberg state forest
if(nchar(SimName)!=3){
  stop("SimName must have exactly three characters!")
} else {
  print("abbreviation is good to go!")
}

# outdir <- tempdir()
temp_cv <- "C:/temp_cv/"
time <- format(Sys.time(), "%Y%m%d-%H%M")
dir.create(outdir <- paste0(temp_cv, "EFI-", SimName, "_", time))
dir.create(paste0(temp_cv,"EFI-", SimName, "_", time, "/out"))
# pdf(file.path(outdir, "out", paste0(SimName, ".pdf")))
# dev.off()

## initialise volume-age matrices ####
plist <- supplyP2009(invdata, SimName, outdir)
aer <- rEFISCEN::runP2009(inv = plist, out = outdir)
# shell.exec(outdir) # show folder

## for running EFISCEN, several files are necessary
## 1 .efs, referencing to (supplyEFS())
## 2 .aer (and .vcl)
## 3 .prs (supplyPRS())
## 4 biomcomp-xxx.txt (supplyBioPar)
## 5 soil-xxx.par (supplySoilPar())


## create soil parameters file ####
soilpars <- list('0_0_0_0'=
               list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
                    Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
                             klig=0.22, khum1=0.012, khum2=0.0012),
                    transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
                    litterComp=c(cw2cel=0.69, cw2sol=0.03, 
                                 fw2cel=0.65, fw2sol=0.03,
                                 nw2cel=0.51, nw2sol=0.27),
                    cddp=c(chum1=0.6, chum2=0.36)))
sf <- supplySoilPar(basename = SimName, dir = outdir, pars = soilpars, 
                    overwrite = TRUE)


## create parameters file ####
prspars <- list(YForest = list('0_0_0_0'=0.6),
             Gamma = list('0_0_0_0'=0.4),
             Harvest = list('0_0_5_0'=c(81, 160, 0.1, 1, 0.015, 0.2),
                            '0_0_8_0'=c(81, 160, 0.1, 1, 0.015, 0.2),
                            '0_0_11_0'=c(81, 160, 0.1, 1, 0.015, 0.2),
                            '0_0_14_0'=c(81, 160, 0.1, 1, 0.015, 0.2),
                            '0_0_17_0'=c(81, 160, 0.1, 1, 0.015, 0.2)),
             Thinrange = list('0_0_5_0'=c(20, 80),
                              '0_0_8_0'=c(20, 80),
                              '0_0_11_0'=c(20, 80),
                              '0_0_14_0'=c(20, 80),
                              '0_0_17_0'=c(20, 80)),
             MortAgeLims = list('0_0_0_0'=c(80, 100, 120, 200)),
             MortRates = list('0_0_0_0'=c(0.02, 0.04, 0.1, 0.25)),
             Decay = list('0_0_0_0'=0.4),
             Thhistory = list('0_0_0_0'=0.2))
prs <- supplyPRS(SimName = SimName, dir = outdir, steps = 5, pars = prspars, 
                 overwrite = TRUE)


## create biocomp.txt file ####
biopars <- list(Carbon = list('0_0_0_0'=0.5),
                WoodDens = list('0_0_0_0'=0.4),
                BioAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                BioAllocations = list('0_0_0_0'=list("stem"=c(0.557,
                                      0.569, 0.581, 0.59, 0.598, 0.605, 0.611),
                                      "branches"=c(0.15, 0.143, 0.138,
                                      0.134, 0.131, 0.128, 0.125),
                                      "coarse roots"=c(0.133, 0.143,
                                      0.151, 0.157, 0.163, 0.167, 0.171),
                                      "fine roots"=c(0.072, 0.067, 0.062,
                                      0.058, 0.054, 0.051, 0.049),
                                      "foliage"=c(0.088, 0.078, 0.068,
                                      0.061, 0.054, 0.049, 0.044))),
                LitterAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                LitterProduction = list('0_0_0_0'= list("Stem"=c(0.0043, 0.0043,
                                       0.0043, 0.0043, 0.0043, 0.0043, 0.0043),
                                       "branches"=c(0.0614, 0.0463, 0.0266,
                                       0.0142, 0.0091, 0.0076, 0.0071),
                                       "coarse roots"=c(0.0614, 0.0463, 0.0266, 
                                       0.0142, 0.0091, 0.0076, 0.0071),
                                       "fine roots"=c(0.4906, 0.4906, 0.4906,
                                       0.4906, 0.4906, 0.4906, 0.4906),
                                       "foliage"=c(0.21, 0.21, 0.21, 0.21, 0.21,
                                       0.21, 0.21))),
                WLCoarseRootsShare = list('0_0_0_0'=0.0))

biocomp <- supplyBioPar(SimName = SimName, biopars = biopars, dir = outdir, 
                        overwrite = TRUE)


## create efs file ####
usc <- unique(invdata$siteclass) # unique site class
sc <- paste0("dgz", usc, "=", usc, collapse=", ")
sc <- eval(parse(text=paste0("list(", sc, ")")))

supplyEFS(title = "Test scenario", abbr = SimName, start = 2012,
          naming = list(region=c(bw=1), owner=c(state=1),
                        siteclass=sc,
                        species=c(spruce=1)), 
          files = list(prs=basename(prs), biomass=basename(biocomp),
                       aer=aer$outfiles[1], soil=basename(sf)),
          dir = outdir, overwrite = TRUE)

## now EFISCEN can be run (without detailed scenarios)
opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=8, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))


# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))

#plots
plot(rslt, combine = c(TRUE, FALSE))
dev.off()


################################################################################
## defining scenario files
################################################################################

## create defgrow file ####
# growth change file
grthScnName <- "Fast Climate Change - positive growth change"
growPars <- list(agelimits = list('0_0_0_0' = c(20, 40, 60, 80, 100, 160, 300)),
                 growth = list("2" = list('0_0_0_0' = c(1,1,1,1,1,1,1)),
                               "8" = list('0_0_0_0' = c(1.1,1.1,1.1,1.1,1.1,1.1,1.1)),
                               "10" = list('0_0_0_0' = c(1.3,1.3,1.3,1.3,1.3,1.3,1.3))))

dgf <- supplyDefGrow(SimName, growPars = growPars, grthScnName = grthScnName, 
                     dir = outdir, overwrite = TRUE)


## create defrems file ####
# Removals scenario file
DefRemsPars <- list('0_0_0_0' = list(steps = c(100),
                                     Fel_stem = c(0.85),
                                     Fel_tops = c(0.0),
                                     Fel_branches = c(0),
                                     Fel_leaves = c(0),
                                     Fel_croots = c(0),
                                     Fel_dwood = c(0),
                                     Thin_stem = c(0.85),
                                     Thin_tops = c(0),
                                     Thin_branches = c(0),
                                     Thin_leaves = c(0),
                                     Thin_croots = c(0),
                                     Thin_dwood = c(0)))
DefRemsScen <- "based on TBFRA"

dfr <- supplyDefRems(SimName = SimName, ScenName = DefRemsScen, dir = outdir, 
                     DefRemsPars = DefRemsPars, overwrite = TRUE)


## create sp_change file ####
# species changes file
SpChParams <- list('100' = list(matrices = c('0_0_0_0'),
                                dest_species = c(8),
                                fract_change = c(0.6)))
SpChScen <- "base"

spch <- supplySpChange(SimName = SimName, ScenName = SpChScen, 
                       SpChParams = SpChParams, dir = outdir, overwrite = TRUE)


## create affor file ####
# afforestation scenario file
AffParams <- list('0_0_0_0' = list(steps = c(100),
                                   affor_area = c(0),
                                   affor_carbon = c(0)))
AffScen <- "base"

aff <- supplyAffor(SimName = SimName, ScenName = AffScen, AffParams = AffParams, 
                   dir = outdir, overwrite = TRUE)


## create defor file ####
#
DeforParams <- list('0_0_0_0' = list(steps = c(100),
                                     defor_area = c(0)))
DeforScen <- "base"

def <- supplyDefor(SimName = SimName, ScenName = DeforScen, 
                   DeforParams = DeforParams, dir = outdir, overwrite = TRUE)


## create defcut file ####
# Forest harvest scenario file
defCutPars <- list('0_0_0_0' = list(step = c(2,4),
                                   felling = c(9000,11000), 
                                   thinning = c(3000,5000)))
DefCutScen <- "base"

dfc <- supplyDefCut(SimName = SimName, ScenName = DefCutScen, 
                    DefCutPars = defCutPars, dir = outdir, overwrite = TRUE)


## create defsoil file ####
### adjust code, because only one of two parameters is needed! ### 
defsoilPars <- list('0_0_0_0' = list(step = 100,
                                  DD = 2067.585333, 
                                  DI = -59.56649925))

# reference: temperature sum (not average annual temperature)
defsoilPars2 <- list(a1 = 0.000387,# temperature sensitivity [°C/day]
                  a2 = 0.00325,# drought sensitivity [1/mm]
                  Tref = 1903,# Reference temperature sum [°C*day]
                  Dref = -32)# Reference drought index [mm]


dfs <- supplyDefSoil(SimName = SimName, soilPars = defsoilPars, soilPars2 = defsoilPars2, dir = outdir, 
                     overwrite = TRUE)


## create scaling file ####
scalParams <- list('0_0_0_0' = 1)
ScalScen <- "base" 

sca <- supplyMatScal(SimName = SimName, ScenName = ScalScen, 
                     scalParams = scalParams, dir = outdir, overwrite = TRUE)


## create thin change file ##
# thinning parameter file (how thinning changes over time, i.e. simulation steps)
thinParams <- list('0_0_5_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(120)),
                   '0_0_8_0' = list(num_Step = c(100),
                                    thin_change_lower = c(20),
                                    thin_change_upper = c(110)),
                   '0_0_11_0' = list(num_Step = c(100),
                                     thin_change_lower = c(20),
                                     thin_change_upper = c(100)),
                   '0_0_14_0' = list(num_Step = c(100),
                                     thin_change_lower = c(20),
                                     thin_change_upper = c(90)),
                   '0_0_17_0' = list(num_Step = c(100),
                                     thin_change_lower = c(20),
                                     thin_change_upper = c(80)))
# thinParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
#                                     thin_change_lower = c(20,20,20),
#                                     thin_change_upper = c(75,50,70)))
# thinParams <- list('0_0_5_0' = list(num_Step = c(100),
#                                     thin_change_lower = c(20),
#                                     thin_change_upper = c(120)))
ThinScen <- "base"

thc <- supplyThinChange(SimName = SimName, ScenName = ThinScen, 
                        thinParams = thinParams, dir = outdir, overwrite = TRUE)


## create ffell change file ####
# final felling change scenario file
fellParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    ffelling = c(75,80,75)))
FellScen <- "base"

fel <- supplyFFellChange(SimName = SimName, ScenName = FellScen, 
                         fellParams = fellParams, dir = outdir, overwrite = TRUE)


## create scenario file ##
# note: look up if efs and scn file automatically searched for by runEFISCEN function

supplySCN(SimName = SimName, 
          scnName = "no_scenario", 
          dir = outdir,
          def_scn = def, 
          rem_def = dfr, 
          aff_scn = aff, 
          grow_ch = dgf, 
          cut_reg = dfc, 
          soil_cli = dfs,
          thinn_cha = thc,
          fell_cha = fel, 
          overwrite = TRUE)

## run main EFISCEN function ####
opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=8, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))


# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))


#plots
pdf(file.path(outdir, "out/res.pdf"), paper="a4r", width=20, height=10)
plot(rslt)
dev.off()

