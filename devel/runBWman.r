#' Author: christian.vonderach@forst.bwl.de
#' Date: 03.11.2021
#' run EFISCEN using Baden-Württemberg data

require(rEFISCEN)

e <-new.env()

# edit for own file path!
load(system.file("data","bw_spruce.rdata", package = "rEFISCEN"), envir = e)

ls(e)
res <- e[["res"]]

invdata <- res
colnames(invdata) <- c("startage", "siteclass", "area", "volume", "increment")
invdata$endage <- invdata$startage + 20
invdata$startage <- invdata$startage + 1
invdata <- invdata[, c("siteclass", "startage", "endage", "area", "volume", "increment")]
invdata$region <- invdata$owner <- invdata$species <- 1
## replace zero values by temporary values to avoid strange estimates in .vcl
invdata[invdata$endage==20 & invdata$siteclass==5, "area"] <- 10
invdata[invdata$endage==40 & invdata$siteclass==5, "area"] <- 15
invdata[invdata$endage==20 & invdata$siteclass==5, "volume"] <- 5
invdata[invdata$endage==40 & invdata$siteclass==5, "volume"] <- 15
invdata[invdata$endage==20 & invdata$siteclass==8, "area"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "volume"] <- 10
plot(area ~ endage, data=invdata, subset=(siteclass==8))

SimName <- "bwsman" # baden württemberg state forest
outdir <- "C:/temp_cv/bwsman"

## initialise volume-age matrices ####
plist <- supplyP2009(invdata, SimName, outdir)
runP2009(inv = plist, out = outdir)
# shell.exec(outdir) # show folder

## provide EFS-file (and dependent files) ####

# supplyPRS()
# supplyPRS(basename = SimName, steps = 5, dir = outdir, overwrite = TRUE,
#           pars = list(AgeClassNum = list('0_0_0_0'=8),
#                       VolClassNum = list('0_0_0_0'=10),
#                       GrFunction = list('0_0_0_0'=c(-2.0384, 1604.33, -10256.0)),
#                       YForest = list('0_0_0_0'=0.6),
#                       Gamma = list('0_0_0_0'=0.4),
#                       Harvest = list('0_0_0_0'=c(80, 160, 10, 100, 10, 50)),
#                       Thinrange = list('0_0_0_0'=c(20, 75)),
#                       AgeLims = list('0_0_0_0'=),
#                       Volsers = list('0_0_0_0'=),
#                       MortAgeLims = list('0_0_0_0'=),
#                       MortRates = list('0_0_0_0'=),
#                       Decay = list('0_0_0_0'=),
#                       Thhistory = list('0_0_0_0'=)))
# supplySoilPar()

supplyEFS(title = "Base scenario Baden-Württemberg", abbr = "bwsman", start = 2012,
          naming = list(region=c(bw=1), owner=c(state=1),
                        siteclass=c(dgz5=5, dgz8=8, dgz11=11, dgz14=14, dgz17=17),
                        species=c(spruce=1)), 
          files = list(prs="bwsman.prs", biomass="biocomp-bwsman.txt",
                       aer="bwsman.aer", soil="soil-bwsman.par"),
          dir = outdir, overwrite = TRUE)

# some input files are still missing in the temporary directory for EFISCEN to run
# the three missing files are: the parameters for increment, management and natural mortality (***.prs-file), the biomass distribution factors, litter and turnover parameters (biocomp-***.txt-file) and the soil internal parameters (soil-***.par-file)
# until further implementation, those files have to be manually written or edited and copied to the temporary directory, in which the other input files can be found

opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=10, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL))


# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = opath, basenames = "bwsman")
colnames(rslt$bws$volume)

#plots
str(rslt$bws$volume)
plot(rslt$bws$volume)

