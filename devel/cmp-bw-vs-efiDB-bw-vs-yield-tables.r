#' compare BW inventory data from EFISCEN-DB against self-calculated values
rm(list=ls())

## EFISCEN DB ##################################################################
e <- new.env()
# edit for own file path!
invdata <- read.csv(system.file("data","efiscenDB_GE_BW_spruce.csv", package = "rEFISCEN"), 
                    skip = 4)
colnames(invdata) <- c("startage", "endage", "area", "volume", "increment")
invdata$species <- invdata$siteclass <- invdata$owner <- invdata$region <- 1
head(invdata)

bwe <- invdata

## self calculated values ######################################################
e <- new.env()

# edit for own file path!
load(system.file("data","bw_spruce.rdata", package = "rEFISCEN"), envir = e)

ls(e)
res <- e[["res"]]

invdata <- res

invdata$VolR_FAO_bw <- NULL

colnames(invdata) <- c("startage", "siteclass", "area", "volume", "increment")
invdata$endage <- invdata$startage + 20
invdata$startage <- invdata$startage + 1
invdata <- invdata[, c("siteclass", "startage", "endage", "area", "volume", "increment")]
invdata$region <- invdata$owner <- invdata$species <- 1
## replace zero values by temporary values to avoid strange estimates in .vcl
invdata[invdata$endage==20 & invdata$siteclass==5, "area"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==5, "volume"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "area"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "volume"] <- 10
plot(area ~ endage, data=invdata, subset=(siteclass==8))
any(invdata[, c("area", "volume")]==0)
head(invdata)

## yield tables BW #############################################################
require(RODBC)
con <- odbcConnectAccess2007("H:/FVA-Projekte/P01770_SURGE_Pro/Daten/Urdaten/wp2/de/hitab_fe2000.accdb")
yt <- sqlFetch(con, sqtable = "HITAB_FE")
yt <- subset(yt, BArt == 1)
yt2 <- data.frame(siteclass=100+yt$dGz100, startage=yt$Alter, endage=yt$Alter + 5,
                  area = 0, volume=yt$VDerb, increment=yt$lGzDerb, species=1,
                  owner=1, region=1)
odbcClose(con)

## bind data and  calculate 5-yr-rel-increment #################################
invdata <- rbind(invdata, bwe, yt2)
View(invdata)
invdata <- invdata[invdata$volume>0,]
invdata[invdata$increment<0, "increment"] <- NA
invdata$relInc5yr <- 5 * invdata$increment / invdata$volume * 100
susc <- sort(unique(invdata$siteclass))

plot(volume ~ endage, data= invdata, type="n", las=1, main="volume [m3/ha]",
     xlim=c(min(invdata$endage), max(invdata$endage)+20))
for(i in seq(along=susc)){
  sc <- susc[i]
  points(x=invdata[invdata$siteclass==sc, "endage"], 
         y=invdata[invdata$siteclass==sc, "volume"], type="b", lty=i, col=i)
}
legend("topright", legend=susc, lty = seq(along=susc), col=seq(along=susc))

plot(increment ~ endage, data= invdata, type="n", las=1, main="increment [m3/ha/a]",
     xlim=c(min(invdata$endage), max(invdata$endage)+20))
for(i in seq(along=susc)){
  sc <- susc[i]
  points(x=invdata[invdata$siteclass==sc, "endage"], 
         y=invdata[invdata$siteclass==sc, "increment"], type="l", lty=i, col=i)
}
legend("topright", legend=susc, lty = seq(along=susc), col=seq(along=susc))


plot(relInc5yr ~ endage, data= invdata, type="n", las=1, main="5-yr-rel-Increment [%]",
     xlim=c(min(invdata$endage), max(invdata$endage)+20))
for(i in seq(along=susc)){
  sc <- susc[i]
  points(x=invdata[invdata$siteclass==sc, "endage"], 
         y=invdata[invdata$siteclass==sc, "relInc5yr"], type="l", lty=i, col=i)
}
legend("topright", legend=susc, lty = seq(along=susc), col=seq(along=susc))
