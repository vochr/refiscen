#' Author: christian.vonderach@forst.bwl.de
#' Date: 03.11.2021
#' run EFISCEN using Baden-Württemberg data

rm(list=ls())
gc()
require(rEFISCEN)

e <- new.env()

# edit for own file path!
load(system.file("data","bw_spruce.rdata", package = "rEFISCEN"), envir = e)

ls(e)
res <- e[["res"]]

invdata <- res

invdata$VolR_FAO_bw <- NULL

colnames(invdata) <- c("startage", "siteclass", "area", "volume", "increment")
invdata$endage <- invdata$startage + 20
invdata$startage <- invdata$startage + 1
invdata <- invdata[, c("siteclass", "startage", "endage", "area", "volume", "increment")]
invdata$region <- invdata$owner <- invdata$species <- 1
## replace zero values by temporary values to avoid strange estimates in .vcl
invdata[invdata$endage==20 & invdata$siteclass==5, "area"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==5, "volume"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "area"] <- 10
invdata[invdata$endage==20 & invdata$siteclass==8, "volume"] <- 10
plot(area ~ endage, data=invdata, subset=(siteclass==8))
any(invdata[, c("area", "volume")]==0)
head(invdata)


SimName <- "bw2" # baden württemberg state forest
if(nchar(SimName)!=3){
  stop("SimName must have exactly three characters!")
}else{
  print("abbreviation is good to go!")
}

# outdir <- tempdir()
temp_cv <- "C:/temp_cv/"
dir.create(outdir <- paste0(temp_cv,"EFI-", SimName))

## initialise volume-age matrices ####
plist <- supplyP2009(invdata, SimName, outdir)
aer <- rEFISCEN::runP2009(inv = plist, out = outdir)
# shell.exec(outdir) # show folder

## for running EFISCEN, several files are necessary
## 1 .efs, referencing to (supplyEFS())
## 2 .aer (and .vcl)
## 3 .prs (supplyPRS())
## 4 biomcomp-xxx.txt (supplyBioPar)
## 5 soil-xxx.par (supplySoilPar())


## create soil parameters file ####
soilpars <- list('0_0_0_0'=
               list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
                    Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
                             klig=0.22, khum1=0.012, khum2=0.0012),
                    transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
                    litterComp=c(cw2cel=0.69, cw2sol=0.03, 
                                 fw2cel=0.65, fw2sol=0.03,
                                 nw2cel=0.51, nw2sol=0.27),
                    cddp=c(chum1=0.6, chum2=0.36)))
sf <- supplySoilPar(basename = SimName, dir = outdir, pars = soilpars, 
                    overwrite = TRUE)


## create parameters file ####
prspars <- list(YForest = list('0_0_0_0'=0.6),
             Gamma = list('0_0_0_0'=0.4),
             Harvest = list('0_0_0_0'=c(81, 160, 0.1, 1, 0.015, 0.2)),
             Thinrange = list('0_0_0_0'=c(20, 80)),
             MortAgeLims = list('0_0_0_0'=c(80, 100, 120, 200)),
             MortRates = list('0_0_0_0'=c(0.02, 0.04, 0.1, 0.25)),
             Decay = list('0_0_0_0'=0.4),
             Thhistory = list('0_0_0_0'=0.2))
prs <- supplyPRS(SimName = SimName, dir = outdir, steps = 5, pars = prspars, 
                 overwrite = TRUE)


## create biocomp.txt file ####
biopars <- list(Carbon = list('0_0_0_0'=0.5),
                WoodDens = list('0_0_0_0'=0.4),
                BioAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                BioAllocations = list('0_0_0_0'=list("stem"=c(0.557,
                                      0.569, 0.581, 0.59, 0.598, 0.605, 0.611),
                                      "branches"=c(0.15, 0.143, 0.138,
                                      0.134, 0.131, 0.128, 0.125),
                                      "coarse roots"=c(0.133, 0.143,
                                      0.151, 0.157, 0.163, 0.167, 0.171),
                                      "fine roots"=c(0.072, 0.067, 0.062,
                                      0.058, 0.054, 0.051, 0.049),
                                      "foliage"=c(0.088, 0.078, 0.068,
                                      0.061, 0.054, 0.049, 0.044))),
                LitterAgeLims = list('0_0_0_0'=c(20, 40, 60, 80, 100, 120, 1000)),
                LitterProduction = list('0_0_0_0'= list("Stem"=c(0.0043, 0.0043,
                                       0.0043, 0.0043, 0.0043, 0.0043, 0.0043),
                                       "branches"=c(0.0614, 0.0463, 0.0266,
                                       0.0142, 0.0091, 0.0076, 0.0071),
                                       "coarse roots"=c(0.0614, 0.0463, 0.0266, 
                                       0.0142, 0.0091, 0.0076, 0.0071),
                                       "fine roots"=c(0.4906, 0.4906, 0.4906,
                                       0.4906, 0.4906, 0.4906, 0.4906),
                                       "foliage"=c(0.21, 0.21, 0.21, 0.21, 0.21,
                                       0.21, 0.21))),
                WLCoarseRootsShare = list('0_0_0_0'=0.0))

biocomp <- supplyBioPar(SimName = SimName, biopars = biopars, dir = outdir, 
                        overwrite = TRUE)


## create efs file ####
usc <- unique(invdata$siteclass) # unique site class
sc <- paste0("dgz", usc, "=", usc, collapse=", ")
sc <- eval(parse(text=paste0("list(", sc, ")")))

supplyEFS(title = "Base scenario Baden-Württemberg", abbr = SimName, start = 2012,
          naming = list(region=c(bw=1), owner=c(state=1),
                        siteclass=sc,
                        species=c(spruce=1)), 
          files = list(prs=basename(prs), biomass=basename(biocomp),
                       aer=aer$outfiles[1], soil=basename(sf)),
          dir = outdir, overwrite = TRUE)

################################################################################
## defining scenario files
################################################################################

## create defgrow file ####
# growth change file
grthScnName <- "no growth change"
growPars <- list(agelimits = list('0_0_0_0' = c(20, 40, 60, 80, 100, 160, 300)),
                 growth = list("10" = list('0_0_0_0' = c(1,1,1,1,1,1,1))))

dgf <- supplyDefGrow(SimName, growPars = growPars, grthScnName = grthScnName, 
                     dir = outdir, overwrite = TRUE)


## create defrems file ####
# Removals scenario file
DefRemsPars <- list('0_0_0_0' = list(steps = c(100),
                                     Fel_stem = c(0.9),
                                     Fel_tops = c(0.0),
                                     Fel_branches = c(0),
                                     Fel_leaves = c(0),
                                     Fel_croots = c(0),
                                     Fel_dwood = c(0),
                                     Thin_stem = c(0.9),
                                     Thin_tops = c(0),
                                     Thin_branches = c(0),
                                     Thin_leaves = c(0),
                                     Thin_croots = c(0),
                                     Thin_dwood = c(0)))
DefRemsScen <- "based on TBFRA"

dfr <- supplyDefRems(SimName = SimName, ScenName = DefRemsScen, dir = outdir, 
                     DefRemsPars = DefRemsPars, overwrite = TRUE)


## create sp_change file ####
# species changes file
SpChParams <- list('100' = list(matrices = c('0_0_0_0'),
                                dest_species = c(8),
                                fract_change = c(0.0)))
SpChScen <- "base"

spch <- supplySpChange(SimName = SimName, ScenName = SpChScen, 
                       SpChParams = SpChParams, dir = outdir, overwrite = TRUE)


## create affor file ####
# afforestation scenario file
AffParams <- list('0_0_0_0' = list(steps = c(100),
                                   affor_area = c(0),
                                   affor_carbon = c(0)))
AffScen <- "base"

aff <- supplyAffor(SimName = SimName, ScenName = AffScen, AffParams = AffParams, 
                   dir = outdir, overwrite = TRUE)


## create defor file ####
## according to NFI ~50.000ha loss from 2002 to 2012
arealoss <- 50000 / 10 * 5 # loss per 5 years [ha/5y]
arealoss <- arealoss/1000 # scale to [1000ha/5y]
DeforParams <- list('0_0_0_0' = list(steps = c(100),
                                     defor_area = c(arealoss)))
DeforScen <- "base"

def <- supplyDefor(SimName = SimName, ScenName = DeforScen, 
                   DeforParams = DeforParams, dir = outdir, overwrite = TRUE)


## create defcut file ####
# Forest harvest scenario file; the given values refer to 1 simulation step
# ie. usually 5 years: here 15000+5000 = 20000 thousand m3/5a => 4 Mio m3/a 
# 5% increase after 20 years.
defCutPars <- list('0_0_0_0' = list(step = c(2,4),
                                   felling = c(25000, 25750), 
                                   thinning = c(10000, 10250)))
DefCutScen <- "base"

dfc <- supplyDefCut(SimName = SimName, ScenName = DefCutScen, 
                    DefCutPars = defCutPars, dir = outdir, overwrite = TRUE)


## create defsoil file ####
### adjust code, because only one of two parameters is needed! ### 
defsoilPars <- list('0_0_0_0' = list(step = 100,
                                  DD = 2067.585333, 
                                  DI = -59.56649925))

# reference: temperature sum (not average annual temperature)
defsoilPars2 <- list(a1 = 0.000387,# temperature sensitivity [°C/day]
                  a2 = 0.00325,# drought sensitivity [1/mm]
                  Tref = 1903,# Reference temperature sum [°C*day]
                  Dref = -32)# Reference drought index [mm]


dfs <- supplyDefSoil(SimName = SimName, soilPars = defsoilPars, soilPars2 = defsoilPars2, dir = outdir, 
                     overwrite = TRUE)


## create scaling file ####
scalParams <- list('0_0_0_0' = 1)
ScalScen <- "base" 

sca <- supplyMatScal(SimName = SimName, ScenName = ScalScen, 
                     scalParams = scalParams, dir = outdir, overwrite = TRUE)


## create thin change file ##
# thinning parameter file (how thinning changes over time, i.e. simulation steps)
thinParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    thin_change_lower = c(20,20,20),
                                    thin_change_upper = c(80,85,90)))
ThinScen <- "base"

thc <- supplyThinChange(SimName = SimName, ScenName = ThinScen, 
                        thinParams = thinParams, dir = outdir, overwrite = TRUE)


## create ffell change file ####
# final felling change scenario file
fellParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
                                    ffelling = c(81,86,91)))
FellScen <- "base"

fel <- supplyFFellChange(SimName = SimName, ScenName = FellScen, 
                         fellParams = fellParams, dir = outdir, overwrite = TRUE)


## create scenario file ##
# note: look up if efs and scn file automatically searched for by runEFISCEN function
# missing: scaling, thin and fel change, defsoil, defcut, defrems, affor, defor, sp change

supplySCN(SimName = SimName, scnName = "no_scenario", dir = outdir,
          def_scn = def, rem_def = dfr, aff_scn = aff, 
          grow_ch = dgf, cut_reg = dfc, soil_cli = dfs,
          thinn_cha = thc, fell_cha = fel, overwrite = TRUE)

#both of the following parameters cannot be used, runEFISCEN will crash
#mat_sca = sca
#spec_cha = spch

## run main EFISCEN function ####
# either this 

opath <- runEFISCEN(dir = outdir, 
                    control=list(steps=8, thinning=1, felling=1, scaling=1, 
                                 select="all", prefix=NULL), out = file.path(outdir, "out"))
shell.exec(file.path(outdir, "out")) # show folder

# use output file path from runEFISCEN function as input
rslt <- loadEFISCEN(path = file.path(outdir, "out"))

tmp <- rslt$bw2$volume
(a <- aggregate(tmp$Area, by=list(step=tmp$Step), FUN=sum))
diff(a$x)


#plots
plot(rslt)

pdf(file.path(outdir, "out", "defor.pdf"))
#plots
plot(rslt)
dev.off()

