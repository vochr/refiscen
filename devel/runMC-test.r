#' run monte carlo simulation on bw data
rm(list=ls())
require(rEFISCEN)
p <- "P:/SurgePro/Daten/Ergebnisse/EFISCEN/EFI-BW1_20240213-1304"
rslt <- loadEFISCEN(path = file.path(p, "out"), run = 1)
plot(rslt)

dir <- p
pue <- rEFISCEN::providePUE(dir)
file.show(pue)

mcdir <- rEFISCEN::efiscenMC(dir = dir, mcdir = NULL, iter=10)
res <- loadEFISCENMC(path = mcdir, selection = "volume")
range(res[[1]]$volume$run)
class(res)

plot(res)

