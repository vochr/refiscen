#' @title Function to call and run EFISCEN default
#' @description This function is intended to call and run the forest growth model EFISCEN. No input parameter are needed. The default values come from the Utopia data from EFISCEN. The output directory can be edited if wanted. If the function is run under a certain name (i.e. example_name <- runEFISCEN_default()), the output of the function will be saved under that name as a list.
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' if(identical(Sys.info()[["sysname"]], "Windows")){
#'   runEFISCEN(dir="C:/temp_cv/EFISCEN/utopia", scn="utopia.scn")
#' }
#' }
#'
#'
runEFISCEN_default <- function(){
  
  user_out <- menu(c("yes","no"), title="Do you want to save the data in a specific location? Otherwise a temporary file will be created to store the data.")
  
  
  ##write directory
  dir<-system.file("efiscen/utopia", package = "rEFISCEN")
  ## check input
  years_EFISCEN <- menu(c("five","ten","fifteen","twenty","twentyfive","thirty"), title="How many years should EFISCAN simulate?")
  
  
  control=list(steps=years_EFISCEN, thinning=1, felling=1, scaling=1, select="all", prefix=NULL)
  
  #efs <- checkFileExists(system.file("efiscen/utopia", "utopia.efs", package = "rEFISCEN"))
  #scn <-  checkFileExists(system.file("efiscen/utopia", "utopia.scn", package = "rEFISCEN"))
  
  efs <- checkFileExists(paste0(dir,"/utopia.efs"))
  scn <- checkFileExists(paste0(dir,"/utopia.scn"))
  
  
  ctrl <- checkControl(control, scn)
  
  
  # check if there is a user defined output path or if the default output path should be used
  
  if(user_out == 1){
    yes <-readline(prompt="Please enter your preferred output path: " )
    if(dir.exists(yes)==TRUE){
      output_dir <- normalizePath(yes, winslash = ws)
    }else{
      output_dir <- tempdir()
      print("File does not exist or has written error, default temporary directory was used.")
    }
  }else{
    output_dir <- tempdir()
  }
  
  #ifelse(is.character(output_dir), output_dir <- output_dir, output_dir <- tempdir())
  out=file.path(output_dir, "out")
  
  ## build file for output selection
  if(!dir.exists(out)) dir.create(out, recursive = TRUE)
  fileConn <- file(slctfile <- file.path(out, "selected.txt"))
  writeLines(ctrl$select, fileConn)
  close(fileConn)
  # file.show(slctfile)
  
  ## build parameter string
  ws <- "\\" # winslash
  opt <- paste0(" steps=", formatC(ctrl$steps, format="f", digits=0),
                " thinning=", formatC(ctrl$thinning, format = "f", digits = 2),
                " felling=", formatC(ctrl$felling, format = "f", digits = 2),
                " scaling=", formatC(ctrl$scaling, format = "f", digits = 2),
                " experiment=", normalizePath(efs, winslash = ws),
                " scenario=", normalizePath(scn, winslash = ws),
                " outputfile=", normalizePath(file.path(out, ctrl$prefix),
                                              winslash = ws, mustWork = FALSE),
                " selected=", normalizePath(slctfile, winslash = ws),
                sep=" ")
  
  # find path of efiscen directory
  efi <- system.file("efiscen", "Efiscen_guifx.jar", package = "rEFISCEN")
  
  
  # write batch file to be called into output location
  fileConn <- file(tmpfile <- file.path(out, "runEFISCEN.bat"))
  writeLines(paste0("java -Duser.language=US -Duser.region=US -cp \"", efi, "\" efi.efiscen.cli.EfiscenCLI", opt), fileConn)
  close(fileConn)
  # file.show(tmpfile)
  
  cmd <- system2(command = tmpfile, stderr = TRUE)
  
  # if successful write message
  if(!is.null(attr(cmd, "status"))){
    message("not successful")
  } else {
    message("succesful")
  }
  
  # user gets a list of all data created by this function
  
  print("The following list with output data was created by running the rEFISCEN-function with utopia test-data. If the function is assigned to a name by the user, the values of the list can be found under the chosen name.")
  
  selected <- read.table(paste0(out,"/selected.txt"), quote="\"", comment.char="")
  output_list <- list(path = normalizePath(out), files = c(list.files(out)))
  print(output_list)
  
  
  # user gets asked if they want some example data plotted
  
  cont <- menu(c("yes","no"), title="Do you want to have example plots from the utopia.csv (base) data?")
  
  # utopia data will be loaded into R
  
  utopia_data <- read.csv(paste0(out,"/utopia.csv"))
  
  # six plots at once can be displayed
  
  #par(mfrow=c(3,2))
  
  # if the user answered with yes, then these example plots will be displayed
  if (cont == 1) {
    plot(utopia_data$Step, utopia_data$GrStock, ylab = "Volume of growing stock in 1000 m3", xlab = "Year", main = "Example Plot 1")
    plot(utopia_data$Step, utopia_data$DeadWood, ylab= "Volume of standing dead wood  in 1000 m3", xlab = "Year", main = "Example Plot 2")
    plot(utopia_data$Step, utopia_data$NatMort, ylab = "Volume of natural mortality in 1000 m3 per time step", xlab = "Year", main = "Example Plot 3")
    plot(utopia_data$Step, utopia_data$Area, ylab = "Forest area in 1000 ha", xlab = "Year", main = "Example Plot 4")
    plot(utopia_data$Step, utopia_data$ThinRems, ylab = "Volume of removals from thinnings in 1000 m3 per time step", xlab = "Year", main = "Example Plot 5")
    plot(utopia_data$Step, utopia_data$FelRems,ylab = "Volume of removals from final fellings in 1000 m3 per time step", xlab = "Year", main = "Example Plot 6")
    plot(utopia_data$Step, utopia_data$IncrAv,ylab = "Net annual increment per ha in m3", xlab = "Year", main = "Example Plot 7")
  } else {
    print("I WANT OUT OF HERE!")
  }  
  par(mfrow=c(1,1))
  
   
  
  
  #import yes or no?
  imp_question <- menu(c("yes","no"), title="Do you want the output data to be imported into RStudio?")
  
  
  
  # make them ready to import
  
  if(imp_question==1){
    temp = list.files(normalizePath(out), pattern="*.csv")
    temp = temp[-which(temp == "utopia_gdat.csv")]
    temp = temp[-which(temp == "utopia_gspec.csv")]
    myfiles = lapply(normalizePath(paste0(paste0(out,"/"),temp)), read.csv)
    names(myfiles) = temp
    myfiles <- c("filepath" = normalizePath(out), "filenames" =list(list.files(out)) , myfiles)
  }
  
  
  #print("This is the folder where the output is saved:")
  return(myfiles)
  
}
