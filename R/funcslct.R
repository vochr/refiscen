#' @title making loadEFISCEN output ready for plotting
#' @description takes loadEFISCEN output list and selects all lists of same class to create a new and comparable list
#' @param rslt output list from loadEFISCEN function
#' @param option name of class will be compared
#' @details rslt has to be a list, option has to be a spelled correctly and given as a character
#' @return this function returns a list, that collected all data frames of a certain class from the output list
#' @export 
#' @examples
#' rslt <- rslt (as in loadEFISCEN output)
#' option <- "volume"
#' to check for possible options, do names rslt[[i]]

funcslct <- function(rslt, option){
  if(is.list(rslt)){
  lnew <- list()
  for (i in seq(along=rslt)) {
    if(option %in% names(rslt[[i]])){
    lnew[i] <- rslt[[i]][which(names(rslt[[i]])== option)]
    listname <- paste0(names(rslt[i]),"_",option)
    names(lnew)[i] <- listname}else{
      stop("option is not listed in output! check for spelling errors!")
    }
  }
  return(lnew)}else{
    stop("rslt input is not a list!")
  }
} 




