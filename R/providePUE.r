#' @title provide VCOV for Monte-Carlo-Simulation on EFISCEN
#' @description build a matrix holding the variances or variable range for the 
#' parameters of the EFISCEN model to be varied during a Monte-Carlo-Simulation
#' @param dir path to EFISCEN directory holding all necessary EFISCEN simulation files
#' @param mcdir directory to run the Monte Carlo simulation
#' @param cv vector of coefficients of variation for each parameter, defaults to 1
#' @param control a list with control options to specify the simulation, i.e.
#' \code{keepDir} - whether directory should be kept (default \code{TRUE}); 
#' \code{keepPUE} - whether existing PUE file should be kept (default \code{TRUE}); 
#' \code{vcovGRF} - whether variance information for growth parameters should be
#'  taken from GRF-file (default \code{TRUE}); 
#' @details Reading the parameters from the simulation path and provide a
#'   variance-covariance matrix for these parameters. This file is then used in
#'   the subsequently applid function \code{\link[rEFISCEN]{efiscenMC}}.
#' @return nothing, side effect: provide file with parameter uncertainty estimates (PUE)
#' @export
#' @examples
#' dir <- "C:/temp_cv/EFI-bws/"
#' mcdir <- "C:/temp_cv/EFI-MC4"
#' control <- list(keepDir=TRUE, keepPUE=TRUE, vcovGRF=TRUE)
#' cv <- 1
#' pue <- providePUE(dir=dir,  mcdir=mcdir, cv=cv, control=control)
#' file.show(pue)
#' ## update the file as shown in the message


providePUE <- function(dir, mcdir=NULL, cv = 1,
                       control=list(keepDir=TRUE, keepPUE=TRUE, vcovGRF=TRUE)){
  
  ## read parameter files
  parpath <- dir
  if(!dir.exists(parpath)) stop(paste0("given file path '", parpath, "' does not exist"))
  
  ## get simname
  SimName <- substr(gsub(pattern = "^EFI-", "", basename(dir)), 1, 3)
  
  ## create directory to run MC
  if(is.null(mcdir)){
    mcdir <- file.path(dir, "MC")
  }
  if(dir.exists(mcdir) & isTRUE(control$keepDir)){
    message("directory exists and you want to keep it (control$keepDir=TRUE).")
  } else {
    unlink(mcdir, recursive = TRUE)
    dir.create(mcdir)
    dir.exists(mcdir)
  }
  
  ## get files to base MC-simulation on
  dirfiles <- list.files(parpath, full.names = TRUE)
  
  ## check if the var-covariance file is already there, otherwise 
  ## create the respective matrix, populate it and write it to the directory
  puefile <- grep(paste0("^PUE-", SimName), list.files(mcdir), value=TRUE)
  
  if(identical(length(puefile), 0L) || identical(control$keepPUE, FALSE)){
    
    ## parameter file
    try(par <- readParFiles(parpath, files = c("prs", "bio", "soil"))) # base simulation w/o scenarios defs
    # TODO add file read for scenario files
    
    ## need to produce a matrix and write it out
    len <- rapply(par, function(a) length(a))
    (nms <- rep(names(len), len))
    (val <- rapply(par, function(a) a))
    nmslst <- strsplit(nms, split = ".", fixed = TRUE)
    ## make sure always 4 elements in list
    nmslst <- lapply(nmslst, function(a){
      if(length(a) < 4){
        return(c(a, "non"))
      } else {
        return(a)
      }
    })
    ## switch list elements in soil elements
    nmslst <- lapply(nmslst, function(a){
      if(any(grepl("^soil$", a))){
        return(c(a[1], a[3], a[2], a[4]))
      } else {
        return(a)
      }
    })
    ## list to data.frame
    df <- data.frame(matrix(unlist(nmslst),
                            nrow = sum(len), byrow = TRUE))
    df$var <- paste0("var", 1:sum(len))
    colnames(df) <- c("file", "parameter", "group", "comp", "var")
    df$val <- val
    df$max <- df$min <- NA # in case of uniform error distribution set values accordingly
    if(identical(1L, length(cv)) | !identical(length(cv), nrow(df))){
      cv <- rep(cv[1], nrow(df))
    }
    for(i in 1:sum(len)){
      # i <- 1
      eval(parse(text=paste0("df$var", i, " <- 0")))
      tmp <- eval(parse(text=paste0("val[", i, "]")))
      tmp <- cv[i]^2 * tmp^2
      eval(parse(text=paste0("df[", i, ", 'var", i, "'] <- tmp")))
    }
    ## get vcov matrix of growth function parameters, turn into matrix
    if(isTRUE(control$vcovGRF)){
      gfefile <- grep("Growth_func_EFSOS1_error.csv", dirfiles, value = TRUE)
      gfe <- readGFE(gfefile)
      for(i in seq(along=gfe)){
        # i <- 1
        idy <- df[idx <- which(df$group %in% names(gfe[i]) & df$parameter %in% "GrFunction"), "var"]
        df[idx, idy] <- gfe[[i]]
      }
    }
    ## write out for adjustments and for application in rEFISCEN::efiscenMC()
    write.csv2(df, file = fp <- file.path(mcdir, paste0("PUE-", SimName, ".csv")), row.names = FALSE)
    message(paste0("see file 'PUE-", SimName, ".csv' in ", mcdir, " and adjust variances where necessary.\n ",
                   "values on diagonal are variances given coefficient of variance being given by 'cv' (default=1) \n ",
                   "i.e. CV=STD[X]/E[X] so that E[X] * cv = STD[X] and VAR[X] = E[X]^2 \n ",
                   "use min/max column, where uniform distribution with boundaries should be used."))
    # file.show(file.path(mcdir, "par-sigma.csv"))
  } else {
    message("there is a PUE-file already in the specified directory and you want to keep it (control$keepPUE=TRUE)")
    fp <- file.path(mcdir, paste0("PUE-", SimName, ".csv"))
  }
  return(fp)
}



