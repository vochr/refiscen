#' @title extracting data for P2009
#' @description producing necessary file for matrix initialization using P2009-tool 
#'
#' @param invdata inventory data
#' @param ccode country code
#' @param outdir directory where files should be written 
#' @param incr optional dataset giving e.g. yield table increment
#' @param plotTF should plots be generated? defaults to \code{FALSE}
#' @param control list of control values to either calculate VCW1 or to hand 
#'  over a pre-defined VCW1 value. Required values are \code{corr} (efault is 0.6),
#'  \code{cv} (default is 0.65), \code{R} (default is 1) and \code{VCW1} (NULL).
#'
#' @details providing all files for initializing the age-volume matrices using the
#' P2009-tool based on the inventory data.
#' The parameters to calculate VCW1 are \code{corr}, i.e. the correlation between
#' volume per hectare and log(age), \code{cv} is the coefficient of variation of
#' volume per hectare and \code{R} is a value to determine equality of the volume
#' classes. \code{R=1} results in equal-width volume classes, \code{R>1} higher
#' volume classes will be larger.
#' @return a list with file paths for the generated files
#' @export
#' @examples 
#' require(rEFISCEN)
#' p <- "H:/FVA-Projekte/P01770_SURGE_Pro/Daten/aufbereiteteDaten/BWI2EFISCEN"
#' load(file.path(p, "bw_spruce.rdata"))
#' tmp <- expand.grid(dGZ100=unique(dfa$dGZ100),
#'                    Al20=seq(0, 160, 20))
#' inv <- merge(tmp, dfa, all.x = TRUE)
#' inv <- merge(inv, inca, all.x = TRUE)
#' inv[is.na(inv)] <- 0 
#' invdata <- inv[order(inv$dGZ100, inv$Al20), ]
#' colnames(invdata) <- c("siteclass", "startage", "volume", "area", "increment")
#' invdata$endage <- invdata$startage + 20
#' invdata$startage <- invdata$startage + 1
#' invdata <- invdata[, c("siteclass", "startage", "endage", "area", "volume", "increment")]
#' invdata$region <- invdata$owner <- invdata$species <- 1
#' ccode <- "bwd"
#' outdir <- "H:/FVA-Projekte/P01770_SURGE_Pro/Daten/aufbereiteteDaten/EFISCEN/wp5/bw"

supplyP2009 <- function(invdata, ccode, outdir, incr=NULL, plotTF=FALSE, 
                        control=list(corr=0.6, cv=0.65, R=1, VCW1=NULL)){
  ## need to provide
  ## 1: inventory data
  ## 2: cfg configuration file: 
  ##    number of region, owners, site classes, species, age and volume classes
  ## 3: ectn matrix parameter file: holding cv, r, VCW1, Beta
  ## 4: growth functions
  
  ## check invdata
  reqcols <- c("region", "owner", "siteclass", "species", "startage", "endage",
               "area", "volume", "increment") # required columns
  if(!all(reqcols %in% colnames(invdata))) stop("missing columns in 'invdata'!")
  
  ## build output
  strinv <- character(0) # string holding inventory data
  strgf <- character(0) # growth functions
  strgfe <- character(0) # error (vcov) of growth function
  strecnt <- c("2009 version") # ecnt file
  usecols <- c("startage", "endage", "area", "volume", "increment")
  for(reg in unique(invdata$region)){
    # reg <- 1
    for(own in unique(invdata$owner)){
      # own <- 1
      for(si in unique(invdata$siteclass)){
        # si <- 5
        for(sp in unique(invdata$species)){
          # sp <- 1
          sid <- subset(invdata, region==reg & owner==own & siteclass==si & species==sp)
          print(sid[, c("region", "owner", "siteclass", "species")])
          if(!nrow(sid)<=0){
            if(!is.null(incr)){
              # in case an alternative increment table is given
              sincr <- subset(incr, region==reg & owner==own & siteclass==si & species==sp)
            } else {
              sincr <- NULL
            }
            
            corr <- control$corr
            cv <- control$cv # should be constant
            R <- control$R
            if(is.null(control$VCW1)){
              VCW1 <- ceiling(estVCW1(x = sid, corr = corr, cv = cv, R = R, alt = TRUE))
            } else {
              VCW1 <- control$VCW1
            }
            
            beta <- 0.40 # constant
            (strecnt <- c(strecnt, paste0(c(cv, corr, VCW1, beta, reg, own, si, sp), collapse = " ")))
            plotmain = paste0("Region: ", reg, ", Owner: ", own, ", Siteclass: ", si, ", Species: ", sp)
            
            egf <- estGrowthFun(stock = sid$volume, increment = sid$increment, alt = sincr,
                                age = rowMeans(sid[, c("startage", "endage")]), plot = plotTF, 
                                main=plotmain)
            egf$param <- round(egf$param, 2)
            (gfcode <- paste0(c(1, formatC(c(reg, own, si, sp), width = 2, flag = 0)), collapse = ""))
            (strgf <- c(strgf, paste0(c(gfcode, egf$param[[1]], egf$param[[2]], egf$param[[3]], 
                                        "", ccode,"Somewhere, Someone, Pine"), collapse = ", ")))
            (strgfe <- c(strgfe, paste0(c(gfcode, as.vector(egf$vcov), 
                                          "", ccode,"Somewhere, Someone, Pine"), collapse = ", ")))
            (strinv <- c(strinv, paste0("START, ",reg,", ", own, ", ", si, ", ", sp, ", ", nrow(sid), ", ", gfcode)))
            for(k in seq(along=sid$startage)){
              # k <- 1
              strinv <- c(strinv, paste0(sid[k, c(usecols)], collapse = ", "))
              # print(strinv)
            }
          }
        }
      }
    }
  }
  strinv
  strgf
  strgfe
  strecnt
  
  ## configuration file
  nreg <- length(unique(invdata$region)) # regions
  nkat <- length(unique(invdata$owner)) # owners
  nbon <- length(unique(invdata$siteclass)) # site classes
  nspp <- length(unique(invdata$species)) # species
  nage <- nrow(sid) # depends on last iteration, but should be same length anyway
  nvol <- 10 # number of desired volume classes ?!
  
  strcfg <- c("#P_2009 configuration file", "#LAND: experiment name", 
              "#NREG: number of Regions", "#NKAT: number of owners",
              "#NBON: number of site types", "#NTRSL: number of species",
              "#NAGE: number of age classes", "#NVOL: number of volume classes",
              "# LAND is not case sensitive",
              paste0(c(ccode, nreg, nkat, nbon, nspp, nage, nvol), collapse = " "),
              "# ", "# END")
  
  ## write strings to file
  if(!dir.exists(outdir)) dir.create(outdir, recursive = TRUE)
  # cfg
  fileConn <- file(cfgfile <- file.path(outdir, "p_2009.cfg"))
  writeLines(strcfg, fileConn) 
  close(fileConn)
  # inventory data
  fileConn <- file(invfile <- file.path(outdir, paste0(ccode, "newefsos.csv")))
  writeLines(strinv, fileConn) 
  close(fileConn)
  # growth functions
  fileConn <- file(gffile <- file.path(outdir, "Growth_func_EFSOS1.csv"))
  writeLines(strgf, fileConn) 
  close(fileConn)
  # growth functions vcov matrix
  fileConn <- file(gfefile <- file.path(outdir, "Growth_func_EFSOS1_error.csv"))
  writeLines(strgfe, fileConn) 
  close(fileConn)
  # ecnt
  fileConn <- file(ecntfile <- file.path(outdir, paste0(ccode, "-ecnt.dat")))
  writeLines(strecnt, fileConn) 
  close(fileConn)
  
  return(list(cfg = cfgfile, inv = invfile, gf = gffile, gfe = gfefile, ecnt = ecntfile))
}


