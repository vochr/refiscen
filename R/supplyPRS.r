#' @title supply .prs file
#' @description write experiments parameter file
#' @param SimName short name for title, used as file name and in file header
#' @param overwrite should file be overwritten if already present?
#' @param dir file path of aer, vcl and grf file
#' @param steps number of years included in one simulation step
#' @param list of parameters that are not extracted from a file by a function, need to be written manually
#' @param aer aer-matrix file path, can be given manually or will be searched from dir
#' @param vcl either NULL, then volume classes taken from inventory data (*newefsos.csvfile),
#' or a a file name (either "*.vcl" or "*newefsos.csv"), then the respective file is used
#' or "vcl", then the vcl file is used. Volume classes can also explicitly given via 'pars'.
#' @param grf grf-matrix file path, can be given manually or will be searched from dir
#' @details not yet added
#' @export
#' @examples 
#' SimName <- "bws"
#' dir <- "C:/temp_cv/EFI-QQGFY"
#' pars <- list(YForest = list('0_0_0_0'=0.6),
#'              Gamma = list('0_0_0_0'=0.4),
#'              Harvest = list('0_0_0_0'=c(81, 160, 0.1, 1, 0.015, 0.2)),
#'              Thinrange = list('0_0_0_0'=c(20, 80)),
#'              MortAgeLims = list('0_0_0_0'=c(80, 100, 120, 200)),
#'              MortRates = list('0_0_0_0'=c(0.02, 0.04, 0.1, 0.25)),
#'              Decay = list('0_0_0_0'=0.4),
#'              Thhistory = list('0_0_0_0'=0.2))
#'
supplyPRS <- function(SimName, dir, steps=5, pars=NULL, aer=NULL, vcl=NULL, 
                      grf=NULL, useDefault=FALSE, overwrite=FALSE){
  
  ## get data from other files
  # aer
  if(is.null(aer)){
    aer <- file.path(dir, paste0(SimName, ".aer", collapse = ""))
  } else if(identical(basename(aer), aer)){
    aer <- file.path(dir, aer)
  }
  if(!file.exists(aer)){
    stop("aer-file not found.")
  }
  aerdata <- rEFISCEN:::getAER(aer) # for AgeClassNum, AgeLims & VolClassNum
  
  # vcl
  if(is.null(vcl)){
    ## default take from observation assuming fully stocked forests
    vcl <- file.path(dir, paste0(SimName, "newefsos.csv", collapse = ""))
  } else if(identical(basename(vcl), vcl)){
    ## if file is specified by name only
    vcl <- file.path(dir, vcl)
  } else if(identical(vcl, "vcl")){
    ## take values from .vcl file of the EFISCEN file directory
    vcl <- file.path(dir, paste0(SimName, ".vcl", collapse = ""))
  } 
  if(!file.exists(vcl)){
    stop("vcl-file not found.")
  }
  vcldata <- rEFISCEN:::getVCL(vcl) # for optimal growing stock, Volsers
  
  # grf
  if(is.null(grf)){
    grf <- file.path(dir, "Growth_func_EFSOS1.csv")
  } else if(identical(basename(grf), grf)){
    grf <- file.path(dir, grf)
  }
  if(!file.exists(grf)){
    stop("grf-file not found.")
  }
  grf <- rEFISCEN:::getGFE(grf) # for growth function parameters
  
  requiredNames <- c("YForest", "Gamma", "Harvest", "Thinrange", "MortAgeLims",
                     "MortRates", "Decay", "Thhistory")
  if(!all(requiredNames %in% names(pars))){
    stop("not all parameters given")
  }
  
  ## head of file
  str <- c("#Experiment's parameters file", 
           "#File build using R and rEFISCEN::supplyPRS()",
           paste("#called by:", Sys.info()[["user"]]), 
           paste("#Date:", Sys.time()),
           paste0("#", SimName), 
           "#Step of simulation (Number of years included in one simulation step)",
           steps, "#")
  
  ## Number of age classes (X axis)
  str <- c(str, "#Number of age classes (X axis)", 
           paste0("AgeClassNum ", length(aerdata$AgeClassNum)), 
           "0 0 0 0", paste(length(aerdata$AgeClassNum), aerdata$AgeClassNum))

  ## Size of age classes (X axis)  - not used
  str <- c(str, "#size of age classes (X axis) - not used", 
           "X1 1", "0 0 0 0", "1 20")
  
  ## Number of volume classes (Y axis)
  str <- c(str, "#Number of volume classes (Y axis)", 
           paste0("VolClassNum ", length(aerdata$VolClassNum)),
           "0 0 0 0", paste(length(aerdata$VolClassNum), aerdata$VolClassNum))

  ## width of Volume classes (Y axis) - not used
  str <- c(str, "#width of vol classes (Y axis) - not used", 
           "Y1 1", "0 0 0 0", "1 50.")
  
  ## Number of volume classes (Y axis)
  str <- c(str, "#Growth function coefficients", 
           paste0("GrFunction ", length(grf)))
  for(i in seq(along=grf)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(grf)[i]),
             paste(length(grf[[i]]), 
                   paste0(round(grf[[i]], 2), collapse = " ")))
  }
  ## Young forest coeff
  str <- c(str, "#Young forest coeff", 
           paste0("YForest ", length(pars$YForest)))
  for(i in seq(along=pars$YForest)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$YForest)[i]),
             paste(length(pars$YForest[[i]]), 
                   paste0(round(pars$YForest[[i]], 1), collapse = " ")))
  }
  ## Growth boost
  str <- c(str, "#Growth boost", 
           paste0("Gamma ", length(pars$Gamma)))
  for(i in seq(along=pars$Gamma)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$Gamma)[i]),
             paste(length(pars$Gamma[[i]]), 
                   paste0(round(pars$Gamma[[i]], 1), collapse = " ")))
  }
  ## Final felling regime
  str <- c(str, "#Final felling regime", 
           paste0("Harvest ", length(pars$Harvest)))
  for(i in seq(along=pars$Harvest)){
    # i <- 1
    if(!(length(pars$Harvest[[i]]) %in% c(1, 6))){
      stop(paste0("length of parameter 'Harvest' should be 1 or 6! Check: ", 
                  names(pars$Harvest[i])))
    }
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$Harvest)[i]),
             paste(length(pars$Harvest[[i]]), 
                   paste0(pars$Harvest[[i]], collapse = " ")))
  }
  ## Thinning range
  str <- c(str, "#Thinning range", 
           paste0("Thinrange ", length(pars$Thinrange)))
  for(i in seq(along=pars$Thinrange)){
    # i <- 1
    if(!identical(length(pars$Thinrange[[i]]), 2L)){
      stop(paste0("length of parameter 'Thinrange' should be 2! Check: ", 
                  names(pars$Thinrange[i])))
    }
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$Thinrange)[i]),
             paste(length(pars$Thinrange[[i]]), 
                   paste0(round(pars$Thinrange[[i]], 1), collapse = " ")))
  }
  ## Coefficient to avoid matrix diversion
  ## width of Volume classes (Y axis) - not used
  str <- c(str, "#Coefficient to avoid matrix diversion", 
           "Beta 1", "0 0 0 0", "1 0.4")
  # ## 
  # str <- c(str, "#Coefficient to avoid matrix diversion", 
  #          paste0("Beta ", length(pars$Beta)))
  # for(i in seq(along=pars$Beta)){
  #   # i <- 1
  #   str <- c(str, 
  #            gsub(pattern = "_", replacement = " ", names(pars$Beta)[i]),
  #            paste(length(pars$Beta[[i]]), 
  #                  paste0(round(pars$Beta[[i]], 1), collapse = " ")))
  # }
  
  ## Age classes of volume series
  str <- c(str, "#Age classes of volume series", 
           paste0("AgeLims ", length(aerdata$AgeClassNum)),
           "0 0 0 0", paste0(aerdata$AgeClassNum, " ", paste0(seq(20, 300, 20), collapse = " ")))
 
  ## Optimal growing stock per age class
  if(!is.null(pars$Volsers)){
    ## take values from pars-list
    str <- c(str, "#Optimal growing stock per age class", 
             paste0("Volsers ", length(pars$Volsers)))
    for(i in seq(along=pars$Volsers)){
      # i <- 1
      volser <- approx(x = seq(along=pars$Volsers[[i]]), y = round(pars$Volsers[[i]], 0), 
                       xout = 1:15, rule = 2)$y
      str <- c(str, 
               gsub(pattern = "_", replacement = " ", names(pars$Volsers)[i]),
               paste(length(volser), paste0(volser, ".", collapse = " ")))
    }
  } else {
    ## take values from getVCL()-function
    str <- c(str, "#Optimal growing stock per age class", 
             paste0("Volsers ", length(vcldata)))
    for(i in seq(along=vcldata)){
      # i <- 1
      x <- 1:15
      volser <- approx(x = seq(along=vcldata[[i]]), y = round(vcldata[[i]], 0), 
                       xout = x, rule = 2)$y
      volser2 <- smooth.spline(x=x, y=volser)$y
      # plot(volser ~ I(1:15), type="o", main=i)
      # points(x=1:15, y=volser2, lty=3, type="o")
      for(e in 1:3){
        d <- abs(volser2 - volser)
        x <- 1:15
        if(any(d > 10)){
          idx <- which.max(d)
          x <- x[-idx]
          volser2 <- volser2[-idx]
          volser <- volser[-idx]
          tmp <- smooth.spline(x=x, y=volser)$y
          volser <- approx(x=x, y=volser, xout=1:15)$y
          volser2 <- approx(x=x, y=tmp, xout=1:15)$y
          # points(x=1:15, y=volser2, lty=e, pch=e, type="o", col="red")
        } else {
          break
        }
      }
      str <- c(str,
               gsub(pattern = "_", replacement = " ", names(vcldata)[i]),
               paste(length(volser), paste0(as.integer(volser), ".", collapse = " ")))
    }
  }
  
  ## Age classes for mortality rates
  str <- c(str, "#Age classes for mortality rates", 
           paste0("MortAgeLims ", length(pars$MortAgeLims)))
  for(i in seq(along=pars$MortAgeLims)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$MortAgeLims)[i]),
             paste(length(pars$MortAgeLims[[i]]), 
                   paste0(pars$MortAgeLims[[i]], collapse = " ")))
  }
  ## Proportion of growing stock that dies each time-step
  str <- c(str, "#Proportion of growing stock that dies each time-step", 
           paste0("MortRates ", length(pars$MortRates)))
  for(i in seq(along=pars$MortRates)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$MortRates)[i]),
             paste(length(pars$MortRates[[i]]), 
                   paste0(pars$MortRates[[i]], collapse = " ")))
  }
  ## Proportion of standing deadwood that falls down each time step
  str <- c(str, "#Proportion of standing deadwood that falls down each time step", 
           paste0("Decay ", length(pars$Decay)))
  for(i in seq(along=pars$Decay)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$Decay)[i]),
             paste(length(pars$Decay[[i]]), 
                   paste0(pars$Decay[[i]], collapse = " ")))
  }
  ## Proportion of area not available for thinning due to recent thinning
  str <- c(str, "#Proportion of area not available for thinning due to recent thinning", 
           paste0("Thhistory ", length(pars$Thhistory)))
  for(i in seq(along=pars$Thhistory)){
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = " ", names(pars$Thhistory)[i]),
             paste(length(pars$Thhistory[[i]]), 
                   paste0(pars$Thhistory[[i]], collapse = " ")))
  }
  str <- c(str, "END") 
  
  ## write
  ptf <- file.path(dir, paste0(SimName, ".prs"))
  if(file.exists(ptf) & isFALSE(overwrite)){
    stop("file exists and overwrite==FALSE")
  } else {
    fileConn <- file(ptf)
    writeLines(str, fileConn) 
    close(fileConn)
    return(ptf) # return path to file
  }
}