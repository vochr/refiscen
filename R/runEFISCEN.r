#' @title Function to call and run EFISCEN
#' @description This function is intended to call and run the forest growth
#'   model EFISCEN using its command line interface (CLI).
#'   
#' @param dir directory where input files live (also efs- and scn files, if not
#'   explicitely given)
#' @param efs experiment file to be run, optional; if NULL efs file is taken
#'   from \code{dir}
#' @param scn scenario file to be applied, optional; if NULL scn file is taken
#'   from \code{dir}
#' @param control setting options for command line interface (CLI), see details
#' @details This functions runs EFISCEN via its command line interface. Several
#'   options can be passed to the model. All of them can be specified via
#'   \code{control}: 
#'   \itemize{ 
#'     \item \code{steps} Number of steps to run the simulation
#'     \item \code{thinning} Proportion of available felling potential
#'     that is actually thinned 
#'     \item \code{felling} Proportion of available
#'     felling potential that is actually felled 
#'     \item \code{scaling} Factor with which all area in all matrix cells is 
#'     multiplied when loading the initial input data to modify the forest area
#'     \item \code{outputfile} File path to where the output files will be 
#'     saved. Path must include a file name (for example 'C:\\Folder\\FileName' 
#'     would save the outputs in 'C:\\Folder\\' and the files would have names 
#'     starting with 'FileName'). 
#'     \item \code{selected} which output files should be generated? 'all' 
#'     means all, otherwise give one, many or all of: base, carbon_country, 
#'     carbon_soil, gdat, gspec, deadwood, felling_matrix, felling_residues, 
#'     natmort, thinning_matrix, thinning_residues, treec_matrix
#'   } 
#'   
#'   For details on using the CLI (p. 35ff), see:
#'   \url{https://efi.int/sites/default/files/files/publication-bank/2018/tr_99.pdf}
#'
#'
#' @return
#' @export
#'
#' @examples
#' \dontrun{
#' if(identical(Sys.info()[["sysname"]], "Windows")){
#'   runEFISCEN(dir="C:/temp_cv/EFISCEN/utopia", scn="utopia.scn")
#' }
#' }
#' 
runEFISCEN <- function(dir, efs=NULL, scn=NULL, out=file.path(dir, "out"),
                       control=list(steps=2, thinning=1, felling=1, scaling=1, 
                                    select="all", prefix=NULL)){
  ## check input 
  efs <- checkFileExists(efs, dir, "efs")
  if(isFALSE(efs)) stop("efs-file not found!")
  scn <- checkFileExists(scn, dir, "scn")
  ctrl <- checkControl(control, efs)
  
  ## build file for output selection
  if(!dir.exists(out)) dir.create(out, recursive = TRUE)
  fileConn <- file(slctfile <- file.path(out, "selected.txt"))
  writeLines(ctrl$select, fileConn)
  close(fileConn)
  # file.show(slctfile)
  
  ws <- "\\" # winslash
  
  ## in case of presence of a scn-file
  if(isFALSE(scn)){
    print("no SCN file will be used in this simulation run!")
  } else {
    strscn <- paste0(" scenario=\"", normalizePath(scn, winslash = ws), "\"")
  }
  
  ## build parameter string
  opt <- paste0(" steps=", formatC(ctrl$steps, format="f", digits=0), 
                " thinning=", formatC(ctrl$thinning, format = "f", digits = 2), 
                " felling=", formatC(ctrl$felling, format = "f", digits = 2), 
                " scaling=", formatC(ctrl$scaling, format = "f", digits = 2),
                " experiment=\"", normalizePath(efs, winslash = ws), "\"",
                ifelse(exists("strscn"), strscn, ""),
                " outputfile=\"", paste0(normalizePath(out, winslash = ws), "\\", ctrl$prefix),
                "\" selected=\"", normalizePath(slctfile, winslash = ws), "\"",
             sep=" ")
  
  # find path of efiscen directory
  efi <- system.file("efiscen", "Efiscen_guifx.jar", package = "rEFISCEN")
  # file.exists(efi)
  # efi <- "C:/temp_cv/EFISCEN/Efiscen_guifx.jar"
 
  # write batch file to be called into output location
  fileConn <- file(tmpfile <- file.path(out, "runEFISCEN.bat"))
  writeLines(paste0("java -Duser.language=US -Duser.region=US -cp \"", efi, 
                    "\" efi.efiscen.cli.EfiscenCLI", opt), fileConn)
  close(fileConn)
  # file.show(tmpfile)
  
  cmd <- system2(command = tmpfile, stderr = TRUE)
  
  # if successful write message
  if(!is.null(attr(cmd, "status"))){
    message("not successful")
  } else {
    message("succesful")
  }
  return(out)
}   
 