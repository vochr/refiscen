#' @title supply defgrow change file 
#' @description This function writes the growth change file. It defines the impact of environmental changes on tree growth.
#' @details If no changes added, ratio is set to 1. Ratio of e.g. 1.1 means an increase of 10% in growth.
#' @param dir path to where the project is saved and where the scaling file will be
#' @param SimName name of simulation, same for all files
#' @param grthScnName name of growth scenario
#' @param growPars list of growth parameters, structure in examples, first, age limits for each matrix is defined and then the growth parameters for every matrix and every timestep (here 2, 6 and 10)
#' @export
#' @examples 
#' dir <- "C:/Users/Eisenecker/Desktop/SURGE-Pro/R/refiscen"
#' SimName <- "bws"
#' grthScnName <- "Fast Climate Change - positive growth change"
#' growPars <- list(agelimits = list('0_0_0_0' = c(20, 40, 60, 80, 100, 160, 300)),
#'                 growth = list("2" = list('0_0_0_0' = c(1,1,1,1,1,1,1)),
#'                               "6" = list('0_0_0_0' = c(1.1,1.1,1.1,1.1,1.1,1.1,1.1)),
#'                               "10" = list('0_0_0_0' = c(1.3,1.3,1.3,1.3,1.3,1.3,1.3))))



supplyDefGrow <- function(SimName, grthScnName, growPars=NULL, dir, useDefault=FALSE, overwrite=FALSE){
 
  ## head of file
  str <- c(paste(c("Comment line (do not delete): Forest grow scenario file",
                   "File built using R", paste("called by:", Sys.info()[["user"]]),
                   paste("Date:", Sys.time()), SimName), collapse = ", "),
           "Comment line (do not delete): scenario  name followed by number of matrices for which age classes will be provided",
           grthScnName,
           length(growPars$agelimits), 
           "Comment line (do not delete): listing of matrices; age-class")

  ## age limits
  for (i in seq(along=growPars$agelimits)) {
    # i <- 1
    str <- c(str, 
             gsub(pattern = "_", replacement = ",", names(growPars$agelimits)[i]),
             paste0(length(growPars$agelimits[[i]]),",", 
                   paste0(growPars$agelimits[[i]], collapse = ",")))
  }
  
  
  ## growth ratios and steps
  
  str <- c(str,
           "Comment line (do not delete): steps; listing of matrices; growth ratio")
  
  for (i in seq(along=growPars$growth)) {
    # i <- 1
    str <- c(str, 
             as.numeric(names(growPars$growth[i])),
             length(growPars$growth[[i]]))
    for (y in seq(along=growPars$growth[[i]])) {
      str <- c(str, 
               gsub(pattern = "_", replacement = ",", names(growPars$growth[[i]][y])),
               paste(c(length(unlist(growPars$growth[[i]][y], use.names = FALSE)),
                 unlist(growPars$growth[[i]][y], use.names = FALSE)),collapse = ","))
    }
  }
  
  ## write
  ptf <- file.path(dir, paste0(SimName, "_defgrow.csv"))
  if(file.exists(ptf) & isFALSE(overwrite)){
    stop("file exists and overwrite==FALSE")
  } else {
    fileConn <- file(ptf)
    writeLines(str, fileConn) 
    close(fileConn)
    return(ptf) # return path to file
  }



   
}

