#' @title execute P-2009 tool
#' @description initialising the required EFISCEN matrices using the P-2009 tool
#' via command line
#' @param inv path to inventory data, see details.
#' @param cfg configuration file for P-2009 tool
#' @param ecnt file for parameters to set-up matrix and to distribute the area
#' @param gf path to growth function file
#' @param out director where the resulting files should be copied to
#' 
#' @details see file Matrix-initialisation.pdf
#' To run the P-2009 tool, which is a compiled windows program, three files are
#' required inside the folder, where the tool is executed. (A fourth file is 
#' required, but has no meaning any more, hence, this demand is internally 
#' treated.)
#' 
#' The inventory data is given as a csv file (parameter \code{inv}), holding
#' columns for start age of age class, end age of age class,
#' area (ha), average standing volume (m3/ha) and net annual increment 
#' (m3/ha/yr). The first age class should include the bare forest land.
#' 
#' The configuration file (parameter \code{cfg}) contains information on the 
#' number of regions, owners, site-classes, species, age-classes and volume 
#' classes for the country for which matrices need to be generated. 
#' 
#' The ecnt file (parameter \code{ecnt}) contains the parameters that are needed
#' for the matrix set-up and the distribution of the area with one line per
#' forest type (defined by region, owner, site-class and species) giving four
#' parameters: \code{cv} (the coefficient of variation of the volume per 
#' hectare for the forest type), \code{r} (correlation between volume per 
#' hectare and ln(age)), \code{VCW1} (width of first volume class) 
#' and \code{Beta}. The parameter \code{cv} is 0.65 by default for all forest
#' types, whereas \code{r} ranges from 0.45 to 0.7, depending on tree species,
#' whether the data is separated into site classes, and whether the forests are
#' well stocked. Basically, the larger the the correlation between volume and 
#' ln(age), the smaller is the variance of volume per hectare. The order of
#' forest types should be the same as in the inventory data file.
#' @export
#' @examples 
#' inv <- system.file("p2009/exampleData", "utonewefsos.csv", package="rEFISCEN")
#' cfg <- system.file("p2009/exampleData", "p_2009.cfg", package="rEFISCEN")
#' ecnt <- system.file("p2009/exampleData", "uto-ecnt.dat", package="rEFISCEN")
#' out <- "C:/temp_cv/testEFISCEN"
#' p <- runP2009(inv, cfg=cfg, ecnt=ecnt, out=out)
#' file.exists(file.path(p$outpath, p$outfiles))
#' 
#' ## test BW data, which is available at rEFISCEN/devel/bw
#' inv <- list(cfg = "devel/bw/p_2009.cfg",
#'             ecnt = "devel/bw/bws-ecnt.dat",
#'             gf = "devel/bw/Growth_func_EFSOS1.csv",
#'             inv = "devel/bw/bwsnewefsos.csv")
#' out <- "devel/bw"

runP2009 <- function(inv, cfg=NULL, ecnt=NULL, gf=NULL, out){
  
  ## in case all path are given as list via parameter inv
  if(is.list(inv)){
    cfg <- inv$cfg
    ecnt <- inv$ecnt
    gf <- inv$gf
    inv <- inv$inv
  }
  if(is.null(cfg)) stop("cfg-file needs be given!")
  if(is.null(ecnt)) stop("ecnt-file needs be given!")
  
  
  ## create temporary paths
  tempdir <- file.path(tempdir(), "runP2009")
  dir.create(file.path(tempdir, "ecnt"), recursive = TRUE)
  ## copy input data to temporary locations
  ccode <- strsplit(basename(ecnt), split = "-")[[1]][1] # country code
  file.copy(from = inv, to = tempdir) # copy inventory data
  file.copy(from = cfg, to = tempdir) # copy configuration file
  file.copy(from = ecnt, to = file.path(tempdir, "ecnt")) # copy ecnt file
  ## find tool and required file
  p2009 <- system.file("p2009", "P_2009.exe", package = "rEFISCEN")
  pgrowth <- system.file("p2009", "Growth_func_EFSOS1.csv", package = "rEFISCEN")
  ## copy those files
  file.copy(from = p2009, to = tempdir) # copy tool
  if(is.null(gf)){
    file.copy(from = pgrowth, to = tempdir) # copy configuration file
    warning("its safer to use a specific growth function file! Use function supplyP2009()!")
  } else {
    file.copy(from = gf, to = tempdir) # copy own growth function file
  }
  
  ## create output directory
  dir.create(outdir <- file.path(tempdir, "output"))
  ## write command to batch file
  fileConn <- file(tmpfile <- file.path(tempdir, "runP2009.bat"))
  cmd <- paste0("cd /D ", normalizePath(tempdir))
  cmd <- c(cmd, paste0("(echo 0 && echo 1 && echo 1) | (P_2009.exe ", ccode, ")"))
  writeLines(cmd, fileConn) 
  close(fileConn)
  # file.show(tmpfile)
  cmdreturn <- system2(tmpfile)
  
  # if successful write message
  if(!is.null(attr(cmdreturn, "status")) | cmdreturn != 0){
    message("not successful")
    aer <- vcl <- NULL
  } else {
    message("succesful")
    # check if all volume classes are positive
    ok <- checkP2009output(aer=file.path(outdir, paste0(ccode, ".aer")))
    if(isTRUE(ok)){
      # copy files back to given location
      if(!dir.exists(out)){
        dir.create(out, recursive = TRUE)
      }
      aer <- paste0(ccode, ".aer")
      file.copy(from = file.path(outdir, aer), to = file.path(out, aer))
      vcl <- paste0(ccode, ".vcl")
      file.copy(from = file.path(outdir, vcl), to = file.path(out, vcl))
    }
  }
  unlink(tempdir, recursive = TRUE)
  return(list(outpath=out, outfiles=c(aer, vcl)))
}
