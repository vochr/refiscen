#' @title extract .vcl data
#' @description extracts limits of volume classes for each forest type and returns them in a list
#' @param vcl vcl-file produced by P2009-Tool
#' 
#' @examples 
#' vcl <- "C:/temp_cv/EFIbw/bws.vcl"

getVCL <- function(vcl){
  if(grepl(".vcl$", vcl[1])){
    ## from file *.vcl
    data <- readLines(con = vcl)
    data2 <- data[(max(grep("#", data))+1):length(data)]
    n <- invisible(sapply(seq(along=data2), FUN=function(a){
      tmp <- strsplit(data2[a], split = "\\s+")[[1]]
      tmp <- tmp[tmp!=""]
      length(tmp)
    }))
    idx <- which(n==4)
    if(identical(length(idx), 1L)){
      volclasses <- length(n) - idx[1]
    } else {
      volclasses <- idx[2] - idx[1] - 1
    }
    
    ll <- list()
    for(i in seq(along=idx)){
      # i <- 1
      tmp <- as.numeric(data2[(idx[i]+1):(idx[i]+volclasses)])
      tmp2 <- strsplit(data2[idx[i]], split = "\\s+")[[1]]
      tmp2 <- tmp2[tmp2!=""]
      tmp2 <- paste0(tmp2, collapse = "_")
      ll[[tmp2]] <- tmp
    }
  } else if (grepl("newefsos.csv", vcl[1])){
    ## from file XXXnewefsos.csv
    data <- readLines(con = vcl)
    n <- length(grep("^START", data, value = TRUE))
    idx <- which(grepl("^START", data))
    if(identical(length(idx), 1L)){
      len <- length(data) - 1
    } else {
      len <- idx[2] - idx[1] - 1
    }
    ll <- list()
    for(i in seq(along=idx)){
      # i <- 1
      strat <- paste0(unlist(strsplit(data[idx[i]], split = ", "))[2:5], collapse = "_")
      df <- data[(idx[i]+1):(idx[i]+len)]
      df <- as.data.frame(matrix(as.integer(unlist(strsplit(df, split = ","))), 
                                 ncol = 5, byrow = TRUE))
      endage <- df$V2
      V4 <- ifelse(df$V4 == 0, NA, df$V4)
      if(length(which(!is.na(V4)))>1){
        V4 <- approx(x = endage, y = V4, xout = endage, rule = 2)$y
      } else {
        V4 <- df$V4
      }
      
      ll[[strat]] <- V4
    }
  }
  
  return(ll)
}
