#' @title loading EFISCEN MC - output
#' @description loads output data of EFISCEN MC simulation
#' @param path path to EFISCEN Monte Carlo output
#' @param selection selector for which files to be loaded
#' @details Each EFISCEN run produces one to several csv-files which hold the
#' results of the simulations. Each file starts with the simulation basename.
#' @return This function loads all of the specified files into a list, possibly 
#' a list of lists.
#' @export 
#' @examples 
#' path <- "C:/temp_cv/EFI-MC"
#' selection <- "volume"
#' res <- loadEFISCENMC(path, selection)

loadEFISCENMC <- function(path, selection=NULL){
  
  dirs <- list.dirs(path = path)
  dirs <- dirs[grep("out$", dirs)]
  
  ll <- list()
  i <- 0
  for(d in seq_along(dirs)){
    # d <- 1
    dd <- dirs[d]
    i <- i + 1 # simulation run 
    bsnm <- substr(gsub("EFI-", "", basename(dirname(dd))), 1, 3)
    
    tmp <- loadEFISCEN(path = dd, selection = selection, run = i)
    nm <- paste0(names(tmp), formatC(i, width = nchar(length(dirs)), flag=0))
    ll[[nm]] <- tmp[[1]]
  }
  
  res <- list()
  for(i in seq(ll[[1]])){
    # i <- 1
    nm <- names(ll[[1]][i])
    res[[nm]] <- do.call(rbind, lapply(seq(ll), function(a) ll[[a]][[i]]))
  }
  res <- list(tmp=res)
  names(res) <- bsnm
  class(res) <- "EFISCENMC"
  return(res)
}

