#' @title read EFISCEN simulation files
#' @description especially for \code{efiscenMC}, read the EFISCEN simulation 
#' files and extract the given parameters
#' @param path path to a working EFISCEN directory holding all necessary EFISCEN 
#' files
#' @details read and extract the parameters to run EFISCEN from a directory
#' @return a list with EFISCEN parameters
#' @export 
#' @examples 
#' path <- "C:/temp_cv/EFI-bws/"
#' grep("^biocomp", list.files(path), value=TRUE)
#' readParFiles(path, files = "prs")
#' readParFiles(path, files = "bio")
#' readParFiles(path, files = "soil")
 
readParFiles <- function(path, files=NULL){
  
  if(is.null(files)){
    files <- c("prs", "aer", "bio", "soil")
  } else {
    files <- files[files %in% c("prs", "aer", "bio", "soil")]
  }
  
  if(!dir.exists(path)){
    stop("path does not exist!")
  } else {
        ## read file list
    fls <- list.files(path)
    scn <- grep(".efs$", fls, value = TRUE) # scenario file
    lns <- readLines(file.path(path, scn))
    prspath <- grep(".prs$", fls, value = TRUE) # parameter file
    aerpath <- grep(".aer$", fls, value=TRUE) # aer-file
    biopath <- grep("^biocomp-[a-zA-Z0-9]+.txt$", fls, value = TRUE) # biomass parameter file
    soilpath <- grep("^soil-[a-zA-Z0-9]+.par$", fls, value = TRUE) # soil parameter file
    
    if("prs" %in% files){
      ## define variables
      prsVars <- c("GrFunction", "YForest", "Gamma", "Harvest", "Thinrange", "Beta",
                   "MortAgeLims", "MortRates", "Decay", "Thhistory")
      ## read parameter file
      prs <- readLines(file.path(path, prspath))
      prsll <- list()
      for(var in prsVars){
        print(var)
        line <- grep(paste0("^", var), prs)
        n <- as.integer(strsplit(prs[line], split = " ")[[1]][2])
        ll <- list()
        for(i in 1:n){
          # i <- 1
          strat <- prs[line+(1+(i-1)*2)] # stratification
          strat <- gsub(" ", "_", strat)
          val <- prs[line+(2+(i-1)*2)]
          val <- strsplit(val, " ")[[1]]
          nval <- as.integer(val[1])
          val <- as.numeric(val[2:(1+nval)])
          ll[[strat]] <- val
        }
        prsll[[var]] <- ll
      }
    } else {
      prsll <- NULL
    }
    
    
    if("aer" %in% files){
      ## read aer-file
      aer <- readLines(file.path(path, aerpath))
    } else {
      aerll <- NULL
    }
    
    if("bio" %in% files){
      ## define variables
      bioVars <- c("Carbon", "WoodDens", "BioAgeLims", "BioAllocations", 
                   "LitterAgeLims", "LitterProduction",
                   "WLCoarseRootsShare")
      ## read biomass parameter file
      bio <- readLines(file.path(path, biopath))
      bioll <- list()
      for(var in bioVars){
        # var <- bioVars[4]
        print(var)
        line <- grep(paste0("^", var), bio)
        n <- as.integer(strsplit(bio[line], split = " ")[[1]][2]) # number of stratifications
        ll <- list()
        if(var %in% c("BioAllocations", "LitterProduction")){
          ## read five components (stem, branch, coarse roots, fine roots, foliage) per stratification
          for(i in 1:n){
            # i <- 1
            biotemp <- bio[(line+1):length(bio)]
            idx <- which(!grepl("^#", biotemp))[1:6]
            biotemp <- biotemp[idx]
            line <- line + max(idx) # reset line indicator for next iteration
            idx <- which(unlist(lapply(strsplit(biotemp, " "), "length")) == 4)
            strat <- biotemp[idx] # stratification
            strat <- gsub(" ", "_", strat)
            biotemp <- biotemp[-idx]
            vall <- list()
            comps <- c("stem", "branches", "coarse roots", 
                       "fine roots", "foliage")
            for(j in seq(along=comps)){
              # j <- 1
              comp <- comps[j]
              val <- strsplit(biotemp[j], " ")[[1]]
              nval <- as.integer(val[1])
              val <- as.numeric(val[2:(1+nval)])
              vall[[comp]] <- val
            }
            ll[[strat]] <- vall
          }
        } else {
          for(i in 1:n){
            # i <- 1
            strat <- bio[line+(1+(i-1)*2)] # stratification
            strat <- gsub(" ", "_", strat)
            val <- bio[line+(2+(i-1)*2)]
            val <- strsplit(val, " ")[[1]]
            nval <- as.integer(val[1])
            val <- as.numeric(val[2:(1+nval)])
            ll[[strat]] <- val
          }
        }
        bioll[[var]] <- ll
      }
    } else {
      bioll <- NULL
    }
    
    if("soil" %in% files){
      ## define variables
      soilVars <- c("ini", "Decomp", "transProp", "litterComp", "cddp")
      ## read parameter file
      soil <- readLines(file.path(path, soilpath))
      ## get number of soil definitions
      line <- grep("^soils", soil)
      n <- as.integer(strsplit(soil[line], split = " ")[[1]][2])
      soilll <- list()
      for(i in 1:n){
        # i <- 1
        ## for each soil definition
        soiltemp <- soil[(line+1):length(soil)]
        ## read six lines (without the commented ones)
        idx <- which(!grepl("^#", soiltemp))[1:6]
        soiltemp <- soiltemp[idx]
        line <- line + max(idx)
        strat <- gsub(" ", "_", soiltemp[1]) # stratification
        # ini
        ini <- as.numeric(strsplit(soiltemp[2], " ")[[1]])
        names(ini) <- c("cwl", "fwl", "nwl", "sol", "cel", "lig", "hum1", "hum2")
        # Decomp
        Decomp <- as.numeric(strsplit(soiltemp[3], " ")[[1]])
        names(Decomp) <- c("acwl", "afwl", "anwl", "ksol", "kcel", "klig", "khum1", "khum2")
        # transProp
        transProp <- as.numeric(strsplit(soiltemp[4], " ")[[1]])
        names(transProp) <- c("psol", "pcel", "plig", "phum")
        # litterComp
        litterComp <- as.numeric(strsplit(soiltemp[5], " ")[[1]])
        names(litterComp) <- c("cw2cel", "cw2sol", "fw2cel", "fw2sol", "nw2cel", "nw2sol")
        # cddp
        cddp <- as.numeric(strsplit(soiltemp[6], " ")[[1]])
        names(cddp) <- c("chum1", "chum2")
        ## build list
        soilll[[strat]] <- list(ini=ini, Decomp=Decomp, transProp=transProp, 
                                litterComp=litterComp, cddp=cddp)
      }
    } else {
      soilll <- NULL
    }
  }
  ll <- list(prs=prsll, aer=aerll, bio=bioll, soil=soilll)
  ll <- ll[lapply(ll, length)>0]
  return(ll)
}
