#' @title Check options for EFISCEN
#' @description Check the given command line options to hand over to EFISCEN CLI
#'
#' @param control list of control values, see details
#' @param efs path to chosen experiment file, used to determine a prefix, if not 
#' explicitly given
#' @details Using the EFISCEN command line interface, one can provide several
#' options. Some are mandatory, like \code{steps}, \code{thinning}, 
#' \code{felling}, \code{experiment} (i.e. efs-file), \code{scaling} and 
#' \code{outputfile}. Two others (\code{scenario} and \code{selected}) are
#' optional. s
#' @return
#' @export
#'
#' @examples
#' 1+1 # fix
checkControl <- function(control, efs){
  stps <- as.integer(control$steps)
  if(!is.integer(stps) | is.na(stps) | stps <= 0){
    stop("control$steps must be positive integer!")
  }
  thin <- as.numeric(control$thinning)
  if(!is.numeric(thin) | is.na(thin) | thin <= 0){
    stop("control$thinning must be positive numeric!")
  }
  fell <- as.numeric(control$felling)
  if(!is.numeric(fell) | is.na(fell) | fell <= 0){
    stop("control$felling must be positive numeric!")
  }
  scal <- as.numeric(control$scaling)
  if(!is.numeric(scal) | is.na(scal) | scal <= 0){
    stop("control$scaling must be positive numeric!")
  }
  slct <- control$select
  avail <- c("base", "carbon_country", "carbon_soil", "gdat", "gspec", "deadwood",
             "felling_matrix", "felling_residues", "natmort", "thinning_matrix", 
             "thinning_residues", "treec_matrix")
  if(identical(slct, "all") | is.na(slct)){
    slct <- avail
  } else if(!all(slct %in% avail)){
    slct <- slct[slct %in% avail]
  }
  slct <- paste0(slct, " 1")
  if(length(slct) < 1) stop("selection of output (control$select) failed.")
  prfx <- control$prefix
  if(is.null(prfx)){
    prfx <- sub(".efs", "", basename(efs))
    # todo check prefix more thoroughly
  }
  
  return(list(steps=stps,
              thinning=thin,
              felling=fell,
              scaling=scal,
              select=slct,
              prefix=prfx))
}