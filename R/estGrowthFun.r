#' @title estimate growth function
#' @description function estimates parameters of EFISCEN growth function
#' @param stock volume stock
#' @param increment yearly increment
#' @param age age of stand
#' @param alt alternative data to fit the growth function, e.g. used in cases
#' where the inventory data are too sparse, but their volume data still should
#' be used
#' @param plot logical, should a plot be produced showing data and model 
#' estimate?
#' @param ... parameters to be passed to plot
#' 
#' @details see file Efiscen_description.pdf
#' @return a list with element param holding the coefficients of the model

#' @export
#' @examples 
#' 1+1

estGrowthFun <- function(stock, increment, age, alt, plot=FALSE, ...){
  idx <- which(stock > 0 & increment > 0)
  stock <- stock[idx]
  increment <- increment[idx]
  age <- age[idx]
  
  useALT <- FALSE
  if(!is.null(alt)){
    idx <- which(alt$volume > 0 & alt$increment > 0, arr.ind = TRUE)
    alt <- alt[idx,]
    stock <- alt$volume
    increment <- alt$increment
    age <- rowMeans(alt[, c("startage", "endage")])
    useALT <- TRUE
  }
  
  ## estimate parameters of EFISCEN growth functions
  ## Increment_T = a_0 + a_1 / T + a_2 / T^2 
  ## with T being stand age
  ## and a_0, a_1 and a_2 being parameters
  ri <- 5 * increment / stock * 100 # relative 5-years increment
  startvals <- c(a0=0, a1=500, a2=-1000)
  fn <- function(par, age, obs){
    a0 <- par[[1]]
    a1 <- par[[2]]
    a2 <- par[[3]]
    # sqrt(mean(((a0 + a1 / age + a2 / age^2) - obs)^2))
    weight <- 1 / age^2
    # weight <- age / age
    sqrt(sum(((a0 + a1 / age + a2 / age^2) - obs)^2 * weight) / sum(weight))
  }
  # # fn(startvals, age, ri)
  # n1 <- optim(startvals, fn, age=age, obs=ri, hessian = TRUE)
  # fn2 <- function(par, age, obs){
  #   a0 <- par[[1]]
  #   a1 <- par[[2]]
  #   a2 <- par[[3]]
  #   eps <- exp(par[[4]])
  #   sum(-dnorm(x = ri, mean = a0 + a1 / age + a2 / age^2, sd = eps, log = TRUE))
  # }
  svals <- c(startvals) # including error estimate
  # fn2(svals, age=age, obs = ri)
  n2 <- optim(svals, fn, age=age, obs=ri, hessian = TRUE)
  if(FALSE){
    n1$par
    n2$par
    coef(n)
    sqrt(diag(solve(n1$hessian))) / sqrt(length(age))
    sqrt(diag(solve(n2$hessian[1:3, 1:3]))) / sqrt(length(age))
    sqrt(diag(vcov(n)))
    
    solve(n1$hessian) / (length(age))
    solve(n2$hessian[1:3, 1:3]) / length(age)
    vcov(n)
    
  }
  # opt <- nlminb(startvals, fn, age=age, obs=ri) # no vcov
  # n <- nls(ri ~ a0 + a1 / age + a2 / age^2, start=n1$par, #weights = age^2,
  #          control=nls.control(minFactor=1/4096, maxiter = 500))
  
  if(isTRUE(plot)){
    agenew <- seq(10, 150, 20)
    yhat <- n2$par[[1]] + n2$par[[2]] / agenew + n2$par[[3]] / agenew^2
    
    agenew2 <- seq(10, 150, 1)
    yhat2 <- n2$par[[1]] + n2$par[[2]] / agenew2 + n2$par[[3]] / agenew2^2
    
    plot(ri ~ age, type="b", ylab="relative increment", las=1, 
         xlim=range(agenew), ylim=c(0, max(yhat, yhat2, ri)), ...)
    # yhat <- predict(n, age)
    points(x = agenew2, y = yhat2, type="l", col="grey")
    points(x = agenew, y = yhat, type="b", pch=3, col=2)
    
    usetitle <- ifelse(isFALSE(useALT), "based on Inventory data", "based on YT data") 
    legend("topright", legend=c("observed", "predicted"), col=c(1,2), pch=c(1,3),
           title=usetitle)
  }
  
  # return(list(param=coef(n), vcov=vcov(n)))
  return(list(param=n2$par[1:3], vcov=solve(n2$hessian[1:3, 1:3]) / (length(age))))
}
