#' @title random character string
#' @description creates random character string for uniqueness
#' @param n length of character string
#' @details just a collapsed sample of n letters.
#' @export
#' @examples 
#' n <- 5
#' hex(n)
hex <- function(n){
  paste0(sample(LETTERS, size = floor(abs(n)), replace = TRUE), collapse = "")
}
