#' @title supply Soil.par file
#' @description write Soil parameter file to define parameters for Yasso soil 
#' module
#' @param basename short name for title, used as file name and in file header
#' @param overwrite should file be overwritten if already present?
#' @details not yet added
#' @export
#' @examples 
#' basename <- "bws"
#' dir <- "C:/temp_cv/EFIbw"
#' pars <- list('0_0_0_0'=
#'                list(ini=c(cwl=0, fwl=0, nwl=0, sol=0, cel=0, lig=0, hum1=0, hum2=0),
#'                     Decomp=c(acwl=0.053, afwl=0.54, anwl=1.0, ksol=0.48, kcel=0.3,
#'                              klig=0.22, khum1=0.012, khum2=0.0012),
#'                     transProp=c(psol=0.2, pcel=0.2, plig=0.2, phum=0.2),
#'                     litterComp=c(cw2cel=0.69, cw2sol=0.03, fw2cel=0.65, fw2sol=0.03,
#'                                  nw2cel=0.51, nw2sol=0.27),
#'                     cddp=c(chum1=0.6, chum2=0.36)))
#' p <- supplySoilPar(basename="bws", dir=dir, pars=pars)
#' p

supplySoilPar <- function(basename, dir, pars=NULL, useDefault=TRUE, 
                          overwrite=FALSE){
  
  n <- length(pars) # number of parameter sets to write
  ## head of file
  str <- c("#Parameter file for EFISCEN (soil)", 
           "#File build using R and rEFISCEN::supplySoilPar()",
           paste("#called by:", Sys.info()[["user"]]), 
           paste("#Date:", Sys.time()),
           paste0("#", basename), 
           paste0("soils ", n))
  for(i in seq(along=pars)){
    str <- c(str, "#REG OWN ST SP")
    str <- c(str, gsub(pattern = "_", replacement = " ", names(pars)[i]))
    
    ## initial carbon stock
    str <- c(str, "#Initial carbon stock in soil compartment (Gg C)",
             "#coarse wl, fine wl, non wl, soluble, cellulose, lignin, humus1, humus2",
             paste0(pars[[i]]$ini, collapse = " "))
    
    ## decomposition rates
    str <- c(str, "#Decomposition rates",
             "#acwl afwl anwl ksol kcel klig khum1 khum2",
             paste0(pars[[i]]$Decomp, collapse = " "))
    
    ## transfer proportion from one to another component
    str <- c(str, "#proportion of mass decomposed in a compartment transferred to a subsequent compartment",
             "#psol pcel plig phum",
             paste0(pars[[i]]$transProp, collapse = " "))
    
    ## litter decomposition
    str <- c(str, "#Litter composition",
             "#cw2cel cw2sol fw2cel fw2sol nw2cel nw2sol",
             paste0(pars[[i]]$litterComp, collapse = " "))
    
    ## climate dependent decomposition pars
    str <- c(str, "#Climate dependence decomposition parameters",
             "#chum1 chum2",
             paste0(pars[[i]]$cddp, collapse = " "))
  }
  
  str <- c(str, "#END")
  
  ## write
  ptf <- file.path(dir, paste0("soil-", basename, ".par"))
  if(file.exists(ptf) & isFALSE(overwrite)){
    stop("file exists and overwrite==FALSE")
  } else {
    fileConn <- file(ptf)
    writeLines(str, fileConn) 
    close(fileConn)
    return(ptf) # return path to file
  }
}