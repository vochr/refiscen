#' @title estimating size of first volume class
#' @description define size of first volume class for initialization of the 
#' age-volume matrix
#' @param x inventory data as necessary for \code{supplyP2009}
#' @param corr correlation r (values found Tab. 2, p.7, EFISCEN_description.pdf)
#' @param cv coefficient of variation, default value of 0.65 for all forest types 
#' @param R scaling factor; if close to 1 then equidistant volume classes
#' @details see EFISCEN_description p. 7/8
#' @return width of first volume class
#' @export
#' @examples 
#' 1+1
 
estVCW1 <- function(x, corr, cv, R=1.00001, alt = FALSE){
  
  if(R <= 1 | R > 2){
    R <- 1.00001
  }
  if(isTRUE(alt) & "volumeYT" %in% colnames(x)){
    Vbar <- weighted.mean(x$volumeYT, x$area)
  } else {
    Vbar <- weighted.mean(x$volume, x$area)
  }
  x$age <- rowMeans(x[c("startage", "endage")])
  relArea <- x$area / sum(x$area)
  k <- sqrt((1-corr^2) * (Vbar * cv)^2) / sum(log(x$age)*relArea)
  s2 <- k * log(x$age)
  VCL10 <- max(x$volume) + 3 * max(s2)
  # VCW1 <- VCL10/10
  VCW1 <- VCL10 * (R-1) / (R^10 - 1)
  return(VCW1)
}