#' @title extract growth functions from EFSOS file
#' @description extracts the three growth function parameters for each matrix and returns them in a list
#' @param gfe file holding growth functions (EFSOS file)
#' @details names of objects in list according to matrix "code"
#' @examples 
#' gfe <- "C:/temp_cv/EFIbw/Growth_func_EFSOS1.csv"

getGFE <- function(gfe){
  data <- read.csv(file = gfe, header = FALSE)
  colnames(data) <- c("idx", "a1", "a2", "a3", "non", "REG", "OWN", "ST", "SP")
  ll <- list()
  for(i in 1:nrow(data)){
    ll[[i]] <- unlist(as.vector(data[i, c("a1", "a2", "a3")]))
    nms <- strsplit(as.character(data[i, "idx"]), "")[[1]]
    nms <- nms[-1] # first is just a number to be not zero
    nms <- paste0(paste0(nms[1:2], collapse = ""), "_", 
                  paste0(nms[3:4], collapse = ""), "_",
                  paste0(nms[5:6], collapse = ""), "_", 
                  paste0(nms[7:8], collapse = ""))
    nms <- gsub("0", "", nms)
    names(ll)[i] <- nms
  }
  
  return(ll)
}
