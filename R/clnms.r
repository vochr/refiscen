#' @title colnames for results
#' @description 
#' @param table name of table for which colnames are required
#' @details R sensible names
#' @return character vector with colnames
#' @export
clnms <- function(table){
  
  tblnms <- c("volume", "carbon_country", "carbon_soil", "fell_matr", "fell_res",
              "gdat", "gspec", "natmort", "thin_matr", "thin_res", "treeC_matr")
  
  if(table %in% c(tblnms)){
    if(identical(table, "volume")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "GrStock", "Area",
               "DeadWood", "NatMort", "ThinRems", "FelRems", "RemsAv", 
               "GrStockAv", "IncrAv", paste0("A_ge", seq(0, 150, 10)),
               paste0("V_ge", seq(0, 150, 10))))
      
    } else if(identical(table, "carbon_country")){
      
      return(c("Step", "C_Trees", "C_Stem", "C_Leaves", "C_Branches", "C_CRoots",
               "C_FRoots", "C_Soil", "CWL", "FWL", "NWL", "SOL", "CEL", "LIG",
               "HUM1", "HUM2", "COUT"))
      
    } else if(identical(table, "carbon_soil")){
      
      return(c("S_ID", "REG", "OWN", "ST", "SP", "Step", "C_Trees", "CWL", "FWL",
               "NWL", "SOL", "CEL", "LIG", "HUM1", "HUM2", "C_Soil", "COUT", 
               "LITIN", "CWL_IN", "FWL_IN", "NWL_IN", "C_BAL"))
      
    } else if(identical(table, "fell_matr")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "FelRem", 
               paste0("A_ge", seq(0, 150, 10)),
               paste0("V_ge", seq(0, 150, 10))))
      
    } else if(identical(table, "fell_res")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "C_TopsRes", "C_BrRes",
               "C_LvRes", "C_CrRes", "C_TopsRem", "C_BrRem", "C_LvRem", 
               "C_CrRem", paste0("X_ge", seq(0, 150, 10))))
      
    } else if(identical(table, "gdat")){
      
      return(c("Step", "Area", "GrStock", "ThinRems", "FelRems", "RemsAv", 
               "GrStockAv", "IncrAv", "C_GrStock", "C_DWood", "C_ThRem", 
               "C_FelRem", "Area.1", "GrStock.1", "ThinRems.1", "FelRems.1",
               "RemsAv.1", "GrStockAv.1", "IncrAv.1", "C_GrStock.1",
               "C_DWood.1", "C_ThRem.1", "C_FelRem.1"))
      
    } else if(identical(table, "gspec")){
      
      return(c("Step", "Area", "GrStock", "ThinRems", "FelRems", "RemsAv", 
               "GrStockAv", "IncrAv", "C_GrStock", "C_DWood", "C_ThRem", 
               "C_FelRem", "Area.1", "GrStock.1", "ThinRems.1", "FelRems.1",
               "RemsAv.1", "GrStockAv.1", "IncrAv.1", "C_GrStock.1",
               "C_DWood.1", "C_ThRem.1", "C_FelRem.1"))
      
    } else if(identical(table, "natmort")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "Nmort", "DWood",
               "C_DWood", paste0("DW_ge", seq(0, 150, 10)), 
               paste0("NW_ge", seq(0, 150, 10))))
      
    } else if(identical(table, "thin_matr")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "ThinRem", 
               paste0("A_ge", seq(0, 150, 10)), 
               paste0("V_ge", seq(0, 150, 10))))
      
    } else if(identical(table, "thin_res")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "C_TopsRes", "C_BrRes",
               "C_LvRes", "C_CrRes", "C_TopsRem", "C_BrRem", "C_LvRem", 
               "C_CrRem", paste0("X_ge", seq(0, 150, 10))))
      
    } else if(identical(table, "treeC_matr")){
      
      return(c("M_ID", "REG", "OWN", "ST", "SP", "Step", "C_Trees",
               paste0("C_St_ge", seq(0, 150, 10)),
               paste0("C_Br_ge", seq(0, 150, 10)),
               paste0("C_Lv_ge", seq(0, 150, 10)),
               paste0("C_Cr_ge", seq(0, 150, 10)),
               paste0("C_Fr_ge", seq(0, 150, 10))))
      
    }
    
  } else {
    
    stop(paste0("colnames for ", table, " not available"))
    
  }
}