#' @title check result of P-2009 tool
#' @description checking the resulting .aer file from P-2009 tool for plausible
#' values, especially for the estimated VCW-value (width of first volume class)
#' @param aer path to output file of P-2009 tool
#' 
#' @details see file Matrix-initialisation.pdf
#' checking the output file .aer of a P-2009.exe tool run.
#' All values in .aer file should be positive, share of bare forest land class 
#' should not exceed 8% of VWC. 
#' @export
#' @examples 
#' aer <- "C:/temp_cv/testEFISCEN/uto.aer"
checkP2009output <- function(aer){
  if(!file.exists(aer)){
    stop(paste0("file ", aer, "does not exist!"))
  }
  # read file
  fileConn <- file(aer)
  f <- readLines(con = fileConn, n = -1)
  close(fileConn)
  f <- f[!grepl("^#", f)]
  
  # process file data
  data <- res <- list()
  n <- m <- s <- d <- 0
  l <- 1
  i <- 0
  for(i in seq(along=f)){
    # i <- i+1
    tmp <- f[i]
    if (grepl(".vcl", tmp)){
      vcl <- tmp
    } else {
      tmp <- gsub(pattern = "^[ ]{1,}", replacement = "", x = f[i])
      IsInt <- !grepl("[.]", tmp) # test if data is integer
      tmp <- as.numeric(strsplit(tmp, split = "[ ]{1,}")[[1]])
      if(identical(length(tmp), 1L)){
        # only one element, then its the number of matrices given
        nmatrix <- tmp
      } else if(identical(length(tmp), 4L) & isTRUE(IsInt)){
        # new matrix
        stratum <- tmp
        d <- i+1 # new data starts
        m <- 1 # iterator for data list
      } else {
        ## add data to  matrix (list)
        data[[m]] <- tmp
        if(identical(as.integer(i), length(f))){
          res[[l]] <- do.call(rbind, data)
        } else {
          m <- m+1
          d <- d+1 # reset iterator for comparison
        }
      }
    } 
  }
  all(sapply(seq(along=res), function(a) res[[a]]>=0))
}
  