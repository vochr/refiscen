
#' Read growth function error file
#'
#' @param file path to gfe-file
#'
#' @return
#' @export
#' @examples
#' 1+1

readGFE <- function(file){
  gfe <- readLines(file)
  
  ll <- list()
  for(i in seq(along=gfe)){
    str <- strsplit(gfe[i], split = ",", fixed = TRUE)[[1]]
    nms <- paste0(c(substr(str[1], 2, 3), substr(str[1], 4, 5), 
                    substr(str[1], 6, 7), substr(str[1], 8, 9)), collapse = "_")
    nms <- gsub("0", "", nms)
    vcov <- matrix(as.numeric(str[2:10]), nrow = 3)
    ll[[nms]] <- vcov
  }
  return(ll)
}