#' @title supply defrems file
#' @description This function writes the removal ratio scenario file. The 
#' proportion of stem, topwood, standing deadwood, branches and foliage to be
#'  removed for each thinning and felling is given. Residues that are not 
#'  extracted are left on site and are used in soil module YASSO.
#' @details if no scenario is given, removal rate for final fellings is 0.95,
#'  for thinnings 0.9 and no removal of topwood, deadwood, branches or foliage
#' @param dir path to where the project is saved and where the scaling file will be
#' @param SimName name of simulation, same for all files
#' @param ScenName name of specific defrems scenario
#' @param DefRemsPars list of parameters for extraction proportions for every matrix
#' @export
#' @examples 
#' dir <- "C:/Users/Eisenecker/Desktop/SURGE-Pro/R/refiscen"
#' SimName <- "bws"
#' ScenName <- "based on TBFRA"
#' DefRemsPars <- list('0_0_0_0' = list(steps = c(100),
#'                                     Fel_stem = c(0.85),
#'                                     Fel_tops = c(0.0),
#'                                     Fel_branches = c(0),
#'                                     Fel_leaves = c(0),
#'                                     Fel_croots = c(0),
#'                                     Fel_dwood = c(0),
#'                                     Thin_stem = c(0.85),
#'                                     Thin_tops = c(0),
#'                                     Thin_branches = c(0),
#'                                     Thin_leaves = c(0),
#'                                     Thin_croots = c(0),
#'                                     Thin_dwood = c(0)))
                            
                        
supplyDefRems <- function(SimName, ScenName, dir, DefRemsPars=NULL, useDefault=FALSE, overwrite=FALSE){
  
  ## head of file
  str <- c(paste(c("Comment line (do not delete): Removals scenario file",
                   "File built using R", paste("called by:", Sys.info()[["user"]]),
                   paste("Date:", Sys.time()), SimName), collapse = ", "),
           "Comment line (do not delete): name of scenario; number of matrices for which input is provided",
           ScenName,
           length(DefRemsPars))

  
  ## matrices and parameters
  for (i in seq(along=DefRemsPars)) {
    # i <- 1
    str <- c(str,
             "Comment line (do not delete): listing of matrices for which input are provided",
             gsub(pattern = "_", replacement = ",", names(DefRemsPars[i])))
    str <- c(str, paste(c("Comment line (do not delete): ",paste(names(DefRemsPars[[i]]), collapse = ", ")), collapse = ""))
    for (y in seq(along=DefRemsPars[[i]]$step)) {
      str <- c(str, paste(c(DefRemsPars[[i]]$steps[y], DefRemsPars[[i]]$Fel_stem[y], DefRemsPars[[i]]$Fel_tops[y], DefRemsPars[[i]]$Fel_branches[y], DefRemsPars[[i]]$Fel_leaves[y], DefRemsPars[[i]]$Fel_croots[y], DefRemsPars[[i]]$Fel_dwood[y], DefRemsPars[[i]]$Thin_stem[y], DefRemsPars[[i]]$Thin_tops[y], DefRemsPars[[i]]$Thin_branches[y], DefRemsPars[[i]]$Thin_leaves[y],DefRemsPars[[i]]$Thin_croots[y],DefRemsPars[[i]]$Thin_dwood[y]), collapse = ","))
    }
  }
  
  ## write
  ptf <- file.path(dir, paste0(SimName, "_defrems.csv"))
  if(file.exists(ptf) & isFALSE(overwrite)){
    stop("file exists and overwrite==FALSE")
  } else {
    fileConn <- file(ptf)
    writeLines(str, fileConn) 
    close(fileConn)
    return(ptf) # return path to file
  }
}
