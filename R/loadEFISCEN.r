#' @title loading EFISCEN output
#' @description loads output data of EFISCEN simulation
#' @param path path to EFISCEN output
#' @param basenames character vector of basenames to be loaded
#' @param selection selector for which files to be loaded, currently not used
#' @param run adding a number to specify the simulation run; mainly used for
#' Monte Carlo runs.
#' @details Each EFISCEN run produces one to several csv-files which hold the
#' results of the simulations. Each file starts with the simulation basename.
#' @return This function loads all of the specified files into a list, possibly 
#' a list of lists.
#' @export 
#' @examples 
#' path <- "C:/temp_cv/EFI-bws/out"
#' selection <- NULL
#' run <- 1
#' loadEFISCEN(path, selection=selection, run=run)

loadEFISCEN <- function(path, selection=NULL, run=NULL){
  
  slct <- c("volume","carbon_soil", "fell_matr",
            "fell_res","natmort", "thin_matr","thin_res",
            "treeC_matr","gdat", "gspec","carbon_country")
  if(!is.null(selection)){
    slct <- slct[slct %in% selection]
    if(!identical(length(slct), length(selection))){
      warning("check wording of selection")
    }
  }
  
  #comment: description from EFISCEN manual
  cmnt <- c(volume = "detailed volume output",
            gspec = "volume output by tree species",
            gdat = "volume output by regions",
            natmort = "amount of mortality that occurred
            and on the standing deadwood pool",
            carbon_soil = "detailed soil carbon output",
            treeC_matr = "detailed tree carbon output",
            carbon_country = "aggregated carbon output by country",
            fell_res = "residues from management operations: felling",
            thin_res = "residues from management operations: thinning",
            fell_matr = "felling results by matrix",
            thin_matr = "thinning results by matrix")
  
  ll <- list()
  for(y in seq_along(path)){
    # y <- 1
    # print(y)
    outFiles <- list.files(path[y], pattern = ".csv$")
    basename <- substr(outFiles[1], 1, 3)
    if(!identical(length(outFiles), 0L)){
      ###### load files ######
      l <- list()
      for(i in seq(along=outFiles)){
        # i <- 1
        # print(i)
        nm <- gsub(pattern = ".csv$", replacement = "", x = outFiles[i])
        nm <- gsub(pattern = paste0(basename, "_"), replacement = "", x=nm)
        nm <- ifelse(nm == basename, "volume", nm)
        
        if(nm %in% slct){
          
          skip <- ifelse(nm %in% c("gdat", "gspec"), 4, 0)
          tmp <- read.csv(file.path(path[y], outFiles[i]), skip=skip, header = TRUE)
          # str(tmp, max.level=1)
          colnames(tmp) <- clnms(nm)
          class(tmp) <- c(paste0("EFI_", nm), class(tmp))
          comment(tmp) <- cmnt[[nm]]
          
          if(nm %in% c("volume","carbon_soil","fell_matr","fell_res","natmort",
                       "thin_matr","thin_res","treeC_matr")){
            # extract exponent of number
            w1 <- ifelse(all(tmp$REG == 0), 0, max(floor(log10(tmp$REG)))) + 1 
            w2 <- ifelse(all(tmp$OWN == 0), 0, max(floor(log10(tmp$OWN)))) + 1
            w3 <- ifelse(all(tmp$ST == 0), 0, max(floor(log10(tmp$ST)))) + 1
            w4 <- ifelse(all(tmp$SP == 0), 0, max(floor(log10(tmp$SP)))) + 1
            tmp$code <- paste0(formatC(tmp$REG, width = w1, flag = "0"), "_",
                               formatC(tmp$OWN, width = w2, flag = "0"), "_",
                               formatC(tmp$ST, width = w3, flag = "0"), "_",
                               formatC(tmp$SP, width = w4, flag = "0"))
          }
          if(!is.null(run)){
            tmp$run <- run
          }
          l[[nm]] <- tmp
        }
      }
      class(l) <- "EFISCEN"
      ll[[basename]] <- l
      class(ll) <- "EFISCEN"
    } else {
      print(paste0("No files for abbreviation ", basenames[y], " found!"))
    }
  }
  return(ll)
}
