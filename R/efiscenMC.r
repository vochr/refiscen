#' @title run Monte-Carlo-Simulation on EFISCEN
#' @description runs EFISCEN multiple times, modifying the parameters in each
#' iteration 
#' @param param list of parameters to vary and hand over to EFISCEN or a path to
#' a working EFISCEN directory holding all necessary EFISCEN files
#' @param iter number of iterations to do
#' @param mcdir directory to run the Monte Carlo simulation
#' @param cv vector of coefficients of variation for each parameter
#' @param control a list with control options to specify the simulation
#' @details in case \code{par} is a path, the EFISCEN files of that directory 
#' are read. In each iteration of the Monte-Carlo simulation, each given 
#' parameter is slightly modified by a randomly drawn error from the 
#' distribution specified by \code{cv} and the EFISCEN simulation repeated. All 
#' results are stored in a list and can be analysed later on.
#' @return a list with EFISCEN results
#' @export 
#' @importFrom mvtnorm rmvnorm
#' @importFrom stats runif
#' @examples 
#' dir <- "C:/temp_cv/EFI-bws/"
#' mcdir <- "C:/temp_cv/EFI-MC3"
#'
#' control <- list(files="prs", steps=5, keep=TRUE, verbose=TRUE)
#' iter <- 10
#' pue <- rEFISCEN::providePUE(dir = dir, mcdir = mcdir)
#' 
#' ## update the file as shown in the message
#' efiscenMC(dir, mcdir=mcdir, iter=iter, control=control)
#' #rEFISCEN::loadEFISCEN(path = mcdir)

efiscenMC <- function(dir, mcdir=NULL, iter=1000, 
                      control=list(files="prs", steps=5, 
                                   verbose=TRUE, keep=TRUE)){
 
  ## get simname
  bsnm <- basename(dir)
  SimName <- substr(gsub(pattern = "^EFI-", "", bsnm), 1, 3)
  
  if(is.null(mcdir)){
    # default is MC directory inside dir
    mcdir <- file.path(dir, "MC")
  }
  
  ## read file in and proceed, or stop
  if(file.exists(pue <- file.path(mcdir, paste0("PUE-", SimName, ".csv")))){
    df <- read.csv2(pue)
  } else {
    stop(paste0("no such file: ", pue))
  }
  
  ## draw 'iter' number of random values from distributions
  sigma <- as.matrix(df[, grep("^var[1-9]+", colnames(df))])
  mcpars <- mvtnorm::rmvnorm(n = iter, mean = df$val, sigma = sigma)
  colnames(mcpars) <- paste(df$file, df$parameter, df$group, df$comp, sep=".")
  ## check if uniform interval is given and possibly override
  for(i in 1:nrow(df)){
    if(!is.na(df[i, "min"]) & !is.na(df[i, "max"])){
      tmp <- runif(n = iter, min = df[i, "min"], max = df[i, "max"])
      mcpars[, i] <- tmp # assign to respective variable
    }
  }
  if(isTRUE(control$verbose)){
    write.csv2(mcpars, file=file.path(mcdir, "mcpars.csv"), row.names = FALSE)
    pdf(file.path(mcdir, "mcpars-plot.pdf"))
    ## plot parameters
    for(i in 1:ncol(mcpars)){
      hist(mcpars[, i], main=colnames(mcpars)[i])
    }
    smc <- mcpars[, grep("GrFunction", colnames(mcpars))]
    ucn <- unique(colnames(smc))
    for(u in ucn){
      # u <- "prs.GrFunction.1_1_5_1"
      plot(1, type="n", ylim=c(0, 150), xlim=c(0, 150), las=1, main=u,
           ylab="rel. incr", xlab="age")
      t <- seq(10, 150)
      for(i in 1:nrow(mcpars)){
        # i <- 1
        pars <- as.vector(smc[i, grep(u, colnames(smc))])
        a0 <- pars[1]
        a1 <- pars[2]
        a2 <- pars[3]
        y <- a0 + a1/t + a2/t^2
        points(x=t, y=y, type="l", col="grey")
      }
      box()
    }
    dev.off()
  }
  
  ## get files to base MC-simulation on
  dirfiles <- list.files(dir, full.names = TRUE)
  par <- readParFiles(dir, files = c("prs", "bio", "soil"))
  
  aerfile <- grep(".aer$", dirfiles, value = TRUE)
  vclfile <- grep(".vcl$", dirfiles, value = TRUE)
  grffile <- grep("Growth_func_EFSOS1.csv", dirfiles, value = TRUE)
  #required for PRS file (fully stocked forests):
  efsosfile <- grep("newefsos.csv", dirfiles, value = TRUE) 
  
  niter <- nchar(as.character(iter))
  
  for(i in 1:iter){
    # i <- 1
    
    dir.create(EFIdir <- file.path(mcdir, paste0(bsnm, "-", formatC(i, width=niter, flag="0"))))
    
    ## copy aer and vcl and grf files; input data: no MC required
    file.copy(from = aerfile, to = EFIdir)
    file.copy(from = vclfile, to = EFIdir)
    file.copy(from = grffile, to = EFIdir)
    file.copy(from = efsosfile, to = EFIdir)
    
    ## efs file; reference file: no MC required
    efsfile <- grep(".efs$", dirfiles, value = TRUE)
    file.copy(efsfile, EFIdir)
    
    ## prs file
    if(!is.null(par$prs)){
      for(pn in names(par$prs)){
        # pn <- "YForest"
        # pn <- "GrFunction"
        print(pn)
        for(g in seq(along=par$prs[[pn]])){
          # print(g)
          nm <- names(par$prs[[pn]][g])
          idy <- grep(paste0("prs.", pn, ".", nm), colnames(mcpars))
          par$prs[[pn]][[g]] <- round(as.vector(mcpars[i, idy]), digits = 2)
        }
      }
      prs <- supplyPRS(SimName = SimName, dir = EFIdir, steps = control$steps, 
                       pars = par$prs, overwrite = TRUE)
    } else {
      prsfile <- grep(".prs$", dirfiles, value = TRUE)
      file.copy(prsfile, EFIdir)
    }
    
    ## biocomp file
    if(!is.null(par$bio)){
      for(pn in names(par$bio)){
        # pn <- "Carbon"
        # pn <- "BioAllocations"
        print(pn)
        for(g in seq(along=par$bio[[pn]])){
          # print(g <- 1)
          nm <- names(par$bio[[pn]][g])
          for(cmp in seq(along=par$bio[[pn]][[nm]])){
            # print(cmp <- 1)
            comp <- names(par$bio[[pn]][[nm]][cmp])
            if(is.null(comp)){
              comp <- "non"
              idy <- grep(paste0("bio.", pn, ".", nm, ".", comp), colnames(mcpars))
              par$bio[[pn]][[g]] <- round(as.vector(mcpars[i, idy]), digits = 2)
            } else {
              idy <- grep(paste0("bio.", pn, ".", nm, ".", comp), colnames(mcpars))
              par$bio[[pn]][[g]][[comp]] <- round(as.vector(mcpars[i, idy]), digits = 2)
            }
          }
        }
      }
      bio <- supplyBioPar(SimName = SimName, biopars = par$bio, dir = EFIdir, overwrite = TRUE)
    } else {
      biofile <- grep("biocomp-[a-zA-Z0-9]+.txt", dirfiles, value = TRUE)
      file.copy(biofile, EFIdir)
    }
    
    ## soil file
    if(!is.null(par$soil)){
      for(pn in names(par$soil)){
        # pn <- "YForest"
        # pn <- "GrFunction"
        print(pn)
        for(g in seq(along=par$soil[[pn]])){
          # print(g)
          nm <- names(par$soil[[pn]][g])
          idy <- grep(paste0("soil.", pn, ".", nm), colnames(mcpars))
          par$soil[[pn]][[g]] <- round(as.vector(mcpars[i, idy]), digits = 2)
        }
      }
      soil <- supplySoilPar(basename = SimName, dir = EFIdir, pars = par$soil, overwrite = TRUE)
    } else {
      soilfile <- grep("soil-[a-zA-Z0-9]+.par", dirfiles, value = TRUE)
      file.copy(soilfile, EFIdir)
    }
    
    # EFIdir <- "C:/temp_cv/RtmpcFzSuK/EFI-bws"
    opath <- runEFISCEN(dir = EFIdir, out = file.path(EFIdir, "out"),
                        control=list(steps=8, thinning=1, felling=1, scaling=1, 
                                     select="all", prefix=NULL))
    # print(opath)
  }
  return(mcdir)
}



