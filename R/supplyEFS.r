#' @title supply EFS file
#' @description write EFS file to define basic information for and about the 
#' EFISCEN simulation
#' @param title dicriminating title for specific run of EFISCEN
#' @param abbr short name for title, used as file name
#' @param start starting year of simulation, i.e. of given inventory data
#' @param naming list with entries for four stratifying factor variables, i.e. 
#' for region, owner, siteclass and species. Each list entry should be a named
#' vector of length fitting to the factor levels in inventory data, i.e. 
#' aer-file.
#' @param files a list with file names for referenced files (i.e. prs, biocomp, 
#' aer and soil-file). See details for more information. 
#' @param dir directory where to write the file
#' @param overwrite should file be overwritten if already present?
#' @details As file can be given as NULL, file name or as a full path, it checks 
#' whether a file with \code{pattern} or the given file name exists in 
#' \code{dir} or whether the specified file path exists.
#' @export
#' @examples 
#' title <- "BW State Forest"
#' abbr <- "bws"
#' start <- 2012
#' naming <- list(region=c(bw=1),
#'                owner=c(state=1),
#'                siteclass=c(dgz5=5, dgz8=8, dgz11=11, dgz14=14, dgz17=17),
#'                species=c(spruce=1))
#' files <- list(prs="bws.prs", biomass="biocomp-bws.txt", aer="bws.aer", 
#'               soil="soil-bws.aer")
#' dir <- tempdir()
#' p <- supplyEFS(title, abbr, start, naming, files, dir)
#' p

supplyEFS <- function(title, abbr, start, naming, files, dir, overwrite=FALSE){
  
  ## head of file
  str <- c("EFISCEN experiment file", "#Experiment's initialisation file", 
           "#Format EFISCEN 4.1", "#File build using R and rEFISCEN::supplyEFS()",
           paste("#called by:", Sys.info()[["user"]]), paste("#Date:", Sys.time()))
  ## adding title
  str <- c(str, title)
  ## adding base year for which inventory data is provided
  str <- c(str, paste0(c(tmp <- "#Base year", rep("-", 60-nchar(tmp))), collapse = ""), start)
  ## add namings 
  if(is.null(naming)){
    stop("need names for factor levels (region:owner:siteclass:species)")
  } else {
    for(var in c("region", "owner", "siteclass", "species")){
      # var <- "region"
      varname <- naming[[var]]
      if(is.null(varname) | is.null(names(varname))) stop(paste("no naming for:", var))
      str <- c(str, paste0(c(tmp<-paste0("#", var), rep("-", 60-nchar(tmp))), collapse=""), 
               length(varname), paste(varname, names(varname)))
    }
  }
  ## add file reference
  str <- c(str, paste0(c(tmp<-"#File name for parameters", rep("-", 60-nchar(tmp))), collapse=""), files$prs, "#")
  str <- c(str, paste0(c(tmp<-"#File name for bioparameters", rep("-", 60-nchar(tmp))), collapse=""), files$biomass, "#")
  str <- c(str, paste0(c(tmp<-"#File name for matrices", rep("-", 60-nchar(tmp))), collapse=""), files$aer, "#")
  str <- c(str, paste0(c(tmp<-"#File name for soils", rep("-", 60-nchar(tmp))), collapse=""), files$soil, "#END")
  
  ## write
  ptf <- file.path(dir, paste0(abbr, ".efs"))
  if(file.exists(ptf) & isFALSE(overwrite)){
    stop("file exists and overwrite==FALSE")
  } else {
    fileConn <- file(ptf)
    writeLines(str, fileConn) 
    close(fileConn)
    return(ptf) # return path to file
  }
}