#' @title supply thin change file
#' @description This functionw writes the thinning change file. It holds the minimum and maximum age for thinnings at certain steps of the simulation.
#' @details If no parameters are given, management parameters of parameters file (***.prs) are applied.
#' @param dir path to where the project is saved and where the scaling file will be
#' @param SimName name of simulation
#' @param ScenName name of scenario
#' @param thinParams list of parameters for thinning for each matrix
#' @export
#' @examples 
#' dir <- "C:/temp_cv"
#' SimName <- "bws"
#' ScenName <- "base"
#' thinParams <- list('0_0_0_0' = list(num_Step = c(2,4,100),
#'                                     thin_change_lower = c(20,20,20),
#'                                     thin_change_upper = c(75,50,70)))
#' supplyThinChange(SimName, ScenName, thinParams, dir=dir, overwrite=TRUE)
#' thinParams <- list('0_0_5_0' = list(num_Step = c(2,4,100),
#'                                     thin_change_lower = c(25,25,25),
#'                                     thin_change_upper = c(80,75,70)),
#'                    '0_0_8_0' = list(num_Step = c(2,4,100),
#'                                     thin_change_lower = c(20,20,20),
#'                                     thin_change_upper = c(75,50,70)))
#' supplyThinChange(SimName, ScenName, thinParams, dir=dir)

supplyThinChange <- function(SimName, ScenName, thinParams, dir, useDefault=FALSE, overwrite=FALSE){
  
  ## head of file
  str <- c(paste(c("Comment line (do not delete): Thinning parameter file",
                   "File built using R", paste("called by:", Sys.info()[["user"]]),
                   paste("Date:", Sys.time()), SimName), collapse = ", "),
           "Comment line (do not delete): name of scenario; number of matrices for which input is provided",
           ScenName,
           length(thinParams))

  
  ## matrices and parameters
  str <- c(str,
           "Comment line (do not delete): listing of matrices for which input are provided",
           paste(gsub(pattern = "_", replacement = ",", names(thinParams)), collapse = ","),
           paste(c("Comment line (do not delete): num_Step, thin_change_lower, thin_change_upper"),
                 collapse = ""))
  for(i in seq(along=thinParams[[1]]$num_Step)){
    # i <- 1
    str2 <- thinParams[[1]]$num_Step[i]
    for(y in seq(along=thinParams)){ # number of matrices
      # y <- 1
      # print(y)
      str2 <- c(str2, 
                thinParams[[y]]$thin_change_lower[i], 
                thinParams[[y]]$thin_change_upper[i])
    }
    str <- c(str, paste(str2, collapse = ","))
  }

   
  ## write file
  ptf <- file.path(dir, paste0(SimName, "_thin_change.csv"))
  if(file.exists(ptf) & isFALSE(overwrite)){
    stop("file exists and overwrite==FALSE")
  } else {
    fileConn <- file(ptf)
    writeLines(str, fileConn) 
    close(fileConn)
    return(ptf) # return path to file
  }
  
}
  