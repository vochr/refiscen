#' @title extract .aer data
#' @description extracts number of age classes and volume classes of matrix file and returns them as a list
#' @param aer aer-file produced by P2009-Tool
#' 
#' @examples 
#' aer <- "C:/temp_cv/EFIbw/bws.aer"

getAER <- function(aer){
  data <- readLines(con = aer)
  data2 <- data[(max(grep("#", data))+1):length(data)]
  n <- invisible(sapply(seq(along=data2), FUN=function(a){
    tmp <- strsplit(data2[a], split = "\\s+")[[1]]
    tmp <- tmp[tmp!=""]
    length(tmp)
  }))
  idx <- which(n==4)
  ageclasses <- n[ idx[1] + 1 ]
  if(identical(length(idx), 1L)){
    volclasses <- length(n) - idx[1] - 1
  } else {
    volclasses <- idx[2] - idx[1] - 2
  }
  
  return(list(AgeClassNum=ageclasses, VolClassNum=volclasses))
}