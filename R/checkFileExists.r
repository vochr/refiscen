#' @title Check file existence
#' @description Make sure, given file (as file name in \code{dir} or path) is 
#' available to be used in EFISCEN
#' @param file a file name with or without full path to a file or NULL
#' @param dir a directory where to search for \code{file} in case of NULL or as
#' given as file name
#' @param pattern which file type should be checked (and searched for in case 
#' file is NULL)
#' @details As file can be given as NULL, file name or as a full path, it checks 
#' whether a file with \code{pattern} or the given file name exists in 
#' \code{dir} or whether the specified file path exists.
#' @export

checkFileExists <- function(file, dir, pattern){
  if(is.null(file)){
    file <- list.files(dir, pattern = paste0(".", pattern, "$"))
    if(length(file) > 1){
      stop(paste0("'file' is NULL and more than one *.", pattern, " file in 'dir' (", dir,
                  "); please specify parameter 'file'."))
    } else {
      file <- file.path(dir, file)
    }
  } else {
    ## if file is given
    if(identical(file, basename(file))){
      # file is a file name, not a path
      # hence, make it a path
      file <- file.path(dir, file)
    }
    if(!file.exists(file)){
      stop(paste0("cannot find ", ifelse(pattern=="efs", "experiment", "scenario"),
                  " file (", pattern, "-file)."))
    }
  }
  if(length(file)==0){
    return(FALSE)
  } else {
    return(file)
  }
  
}